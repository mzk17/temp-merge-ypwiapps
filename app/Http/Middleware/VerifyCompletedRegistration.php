<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Redirect;

class VerifyCompletedRegistration
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! $request->user()->hasCompletedRegistration()) {
            return $request->expectsJson()
                ? abort(403, 'Anda belum melengkapi informasi data diri.')
                : Redirect::route('prime.show-fcr');
        }

        return $next($request);
    }
}
