<?php

namespace App\Http\View\Composers\AssetInventory;

use Illuminate\View\View;

class SidebarItemComposer
{
    public function compose(View $view)
    {
        $master_data_children = [
        [
          'link' => route('asset-inventory.barang.index'),
          'name' => 'Barang',
          'icon' => 'nav-icon fas fa-shopping-cart'
        ],
        [
          'link' => route('asset-inventory.barang.index'),
          'name' => 'Tipe / Jenis Tanah',
          'icon' => 'nav-icon fas fa-map-marked-alt'
        ],
        [
          'link' => route('asset-inventory.kategori.index'),
          'name' => 'Kategori',
          'icon' => 'nav-icon fas fa-store-alt'
        ],
        [
          'link' => route('asset-inventory.ruangan.index'),
          'name' => 'Ruangan',
          'icon' => 'nav-icon fas fa-cube'
        ],
        [
          'link' => route('asset-inventory.supplier.index'),
          'name' => 'Supplier',
          'icon' => 'nav-icon fas fa-handshake'
        ],
      ];
        $transaksi_children = [
        [
          'link' => '#',
          'name' => 'Keuangan',
          'icon' => 'far fa-money-bill-alt nav-icon'
        ],
        [
          'link' => route('asset-inventory.transaksi.alat.index'),
          'name' => 'Aset Alat/Barang/Mesin',
          'icon' => 'nav-icon fas fa-tools'
        ],
        [
          'link' => route('asset-inventory.transaksi.bangunan.index'),
          'name' => 'Aset Bangunan',
          'icon' => 'nav-icon fas fa-building'
        ],
        [
          'link' => route('asset-inventory.transaksi.tanah.index'),
          'name' => 'Aset Tanah',
          'icon' => 'nav-icon fas fa-campground'
        ],
      ];
        $setting_children = [
        [
          'link' => '#',
          'name' => 'Profile / Data Diri',
          'icon' => 'nav-icon fas fa-user-cog',
        ],
        [
          'link' => '#',
          'name' => 'Ganti Password',
          'icon' => 'nav-icon fas fa-key',
        ],
      ];

        $sidebar_item = [
        'dashboard' => [
          'link' => route('asset-inventory.dashboard'),
          'name' => 'Dashboard',
          'icon' => 'nav-icon fas fa-th',
        ],
        'master_data' => [
          'link' => '#',
          'name' => 'Master Data',
          'icon' => 'nav-icon fas fa-copy',
          'children' => $master_data_children,
        ],
        'transaksi' => [
          'link' => '#',
          'name' => 'Transaksi',
          'icon' => 'nav-icon fas fa-exchange-alt',
          'children' => $transaksi_children,
        ],
      ];

        $sidebar_user_item = [
        'absensi' => [
          'link' => '#',
          'name' => 'Absensi',
          'icon' => 'nav-icon far fa-calendar-alt',
        ],
        'setting' => [
          'link' => '#',
          'name' => 'Pengaturan',
          'icon' => 'nav-icon fas fa-cogs',
          'children' => $setting_children,
        ],
        'logout' => [
          'link' => route('logout'),
          'name' => 'KELUAR',
          'icon' => 'nav-icon text-warning fas fa-power-off',
        ],
      ];
        $view->with('sidebar_item', $sidebar_item);
        $view->with('sidebar_user_item', $sidebar_user_item);
    }
}
