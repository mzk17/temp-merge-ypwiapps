<?php

namespace App\Http\Controllers\Prime;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Http\Controllers\Base\PrimeController;
use SevenArts\Validators\PersonalData as PersonalDataValidators;
use SevenArts\Models\Prime\AdministrasiWilayah;
use SevenArts\Models\Prime\Alamat;
use SevenArts\Models\DataPribadi;

class UserController extends PrimeController
{
    use PersonalDataValidators;

    protected $err_validation_messages;

    public function __construct()
    {
        $this->errorValidationMessages();
        $this->setActiveUser(); // $this->user;
    }

    public function showFormCompleteRegistration()
    {
        // Apabila user sudah punya data pribadi
        if ($this->user->hasCompletedRegistration()) {
            return redirect()->route('prime.main-home');
        }
        $admReg = AdministrasiWilayah::provinceList()->get();
        return view('prime.pages.complete-registration', compact('admReg'));
    }

    /**
     * Complete user registration with
     * create personal and address data
     *
     * @param \Illuminate\Http\Request $request
     * @return response $messages
     */
    public function completeRegistration(Request $request)
    {
        if (Auth::check()) {
            // Create first new address
            $newAddress = $this->createAddress($request);
    
            // Complete the user's personal data
            $newPersonal = $this->createPersonalData($request, $newAddress->id);
    
            // Link personal data to user
            $this->user->id_data_personal = $newPersonal->id;
            $this->user->save();
    
            // Response back
            $request->session()->flash('crg_message', 'Pendaftaran anda telah lengkap!');
            return redirect()->route('main-home');
        } else {
            // Logout akun pengguna
            Auth::logout();
            
            // Response
            $request->session()->flash('crg_message', 'Maaf, anda tidak terdaftar!');
            return redirect()->route('login');
        }
    }

    /**
     * Membuat informasi data pribadi pengguna baru.
     * HANYA digunakan pada proses melengkapi pendaftaran.
     *
     * @param \Illuminate\Http\Request $request
     * @param integer $addressId
     * @param boolean $responseBack - Provide new data model or other
     * @return response $messages
     */
    public function createPersonalData(Request $request, $addressId = 1)
    {
        $necessaryFields  = $this->necessaryFields(DataPribadi::class, ['id']);
        $data             = $request->only($necessaryFields);
        $data['alamat_1'] = $addressId;
        $validator        = $this->personalDataValidator($data);
        
        if ($validator->fails()) {
            dd($validator);
        }

        $newPersonal = $this->create(DataPribadi::class, $data);

        return $newPersonal;
    }

    /**
     * Membuat data alamat baru.
     * HANYA digunakan pada proses melengkapi pendaftaran
     * akun pengguna baru.
     *
     * @param \Illuminate\Http\Request $request
     * @param boolean $responseBack - Provide new data model or other
     * @return response $messages
     */
    public function createAddress(Request $request)
    {
        $necessaryFields = $this->necessaryFields(Alamat::class, ['id']);
        $data            = $request->only($necessaryFields);
        $validator       = $this->addressValidator($data);

        if ($validator->fails()) {
            dd($validator);
        }

        $newAddress = $this->create(Alamat::class, $data);

        return $newAddress;
    }
}
