<?php

namespace App\Http\Controllers\Prime;

use Illuminate\Http\Request;
use App\Http\Controllers\Base\PrimeController;

class MasterController extends PrimeController
{
    public function home()
    {
        return view('pages.prime.home');
    }
}
