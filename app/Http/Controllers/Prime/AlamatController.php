<?php

namespace App\Http\Controllers\Prime;

use Illuminate\Http\Request;
use App\Http\Controllers\Base\PrimeController;
use SevenArts\Models\Prime\AdministrasiWilayah;
use SevenArts\Models\Prime\Alamat;
use SevenArts\Validators\PersonalData as AddressValidator;

class AlamatController extends PrimeController
{
    use AddressValidator;

    protected $err_validation_messages;
    protected $reqPayload;

    public function __construct()
    {
        // Generator pada validator
        $this->errorValidationMessages();

        $this->reqPayload = new \stdClass();
        $this->reqPayload->model = Alamat::class;
        $this->reqPayload->entity_name = "Alamat";
        $this->reqPayload->controller  = $this;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    
    public function apiShowProvinceList()
    {
        return response()->json(
            AdministrasiWilayah::provinceList()
        );
    }

    public function apiShowDescendantsRegionList($previous_id)
    {
        switch (strlen($previous_id)) {
            case 2:
                $method = 'cityList';
                break;
            case 5:
                $method = 'districtList';
                break;
            case 8:
                $method = 'subDistrictList';
                break;
        }
        return response()->json(
            AdministrasiWilayah::{$method}($previous_id)->get()
        );
    }

    public function autoQuery($alamat)
    {
        # code...
    }
}
