<?php

namespace App\Http\Controllers\Prime;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Base\PrimeController;
use App\Repositories\Prime\LembagaRepository;
use SevenArts\Models\Prime\Lembaga;
use SevenArts\Services\AlamatService;
use SevenArts\Support\Traits\BasicCRUD\FieldsFilter;

class LembagaController extends PrimeController
{
    use FieldsFilter;

    protected $req_payload;

    /**
     * Controller ini untuk Zero / Master data,
     * dimana fungsi Create, Update, Delete ada pada PrimeController.
     * @var  $model
     * @var  $entity_name
     */
    public function __construct()
    {
        $this->req_payload = new \stdClass();
        $this->req_payload->model       = Lembaga::class;
        $this->req_payload->repository  = new LembagaRepository();
        $this->req_payload->next_route  = 'prime.lembaga.index';
        $this->req_payload->show_view   = 'pages.prime.lembaga.view';
        $this->req_payload->controller  = $this;
    }

    public function storeValidation()
    {
        return true;
    }

    public function updateValidation()
    {
        return true;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        list($daftar_lembaga, $adm_reg) = LembagaRepository::dashboard();

        return view('pages.prime.lembaga.index', compact('daftar_lembaga', 'adm_reg'));
    }

    public function edit($id)
    {
        list($lembaga, $adm_reg) = LembagaRepository::edit($id);

        return view('pages.prime.lembaga.edit', compact('lembaga', 'adm_reg'));
    }

    protected function validator($data)
    {
        // Error messages
        $messages = [
            'nama_yayasan.required' => 'Nama Yayasan / Lembaga tidak boleh kosong!',
            'id_alamat.required' => 'Pilih alamat terlebih dahulu'
        ];
        // Rules
        $rules = [
            'nama_yayasan' => 'required|string|min:6',
            'id_alamat' => 'required'
        ];

        // Validation
        return Validator::make($data, $rules, $messages);
    }

    protected function beforeStore()
    {
        // Simpan alamat terlebih dahulu
        $alamat = (new AlamatService)->createAlamat();
        if ($alamat['status'] === 'success')
            request()->merge(['id_alamat' => $alamat['data']->id]);
    }

    protected function beforeUpdate()
    {
        (new AlamatService)->updateAlamat(request()->get('id_alamat'));
    }
}
