<?php

namespace App\Http\Controllers\Prime;

use App\Http\Controllers\Base\PrimeController;
use App\Repositories\Prime\DataPribadiRepository;
use Illuminate\Support\Facades\Validator;
use SevenArts\Models\Prime\DataPribadi;

class DataPribadiController extends PrimeController
{
    public function __construct()
    {
        $this->req_payload              = new \stdClass();
        $this->req_payload->model       = DataPribadi::class;
        $this->req_payload->repository  = new DataPribadiRepository();
        $this->req_payload->next_route  = 'prime.personal-data.index';
        // $this->req_payload->show_view   = 'prime.pages.employee.view';
        $this->req_payload->controller  = $this;
    }

    public function updateValidation()
    {
        return true;
    }

    public function index()
    {
        list($daftar_data_pribadi) = DataPribadiRepository::dashboard();

        return primeView('pages.personal-data.index', compact('daftar_data_pribadi'));
    }

    public function edit(DataPribadi $data_pribadi)
    {
        return primeView('pages.personal-data.edit', compact('data_pribadi'));
    }

    protected function validator($data)
    {
        $rules = [
            'nama' => 'required|string|min:3',
            'jenis_kelamin' => 'required|string|max:1',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'agama' => 'required',
            'email' => 'email'
        ];
        
        return Validator::make($data, $rules);
    }
}
