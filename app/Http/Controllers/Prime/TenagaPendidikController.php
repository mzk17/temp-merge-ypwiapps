<?php

namespace App\Http\Controllers\Prime;

use App\Http\Controllers\Base\PrimeController;
use App\Repositories\Prime\TenagaPendidikRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use SevenArts\Models\Prime\TenagaPendidik;
use SevenArts\Services\DataPribadiService;

class TenagaPendidikController extends PrimeController
{
    public function __construct()
    {
        $this->req_payload              = new \stdClass();
        $this->req_payload->model       = TenagaPendidik::class;
        $this->req_payload->repository  = new TenagaPendidikRepository();
        $this->req_payload->next_route  = 'prime.tenaga-pendidik.index';
        $this->req_payload->controller  = $this;
    }

    public function storeValidation()
    {
        return true;
    }

    //membuka list TenagaPendidik
    public function index(Request $request)
    {
        list($daftar_tenaga_pendidik, $adm_reg) = TenagaPendidikRepository::dashboard();
        return primeView('tenaga-pendidik.index', compact('daftar_tenaga_pendidik', 'adm_reg'));
    }

    public function edit($id)
    {
        list($tenaga_pendidik, $adm_reg) = TenagaPendidikRepository::edit($id);
        return primeView('tenaga-pendidik.edit', compact('tenaga_pendidik', 'adm_reg'));
    }

    public function validator($data)
    {
        $messages = [
            'nip.required' => 'Nomor Induk Pegawai tidak boleh kosong!',
            'id_data_pribadi.required' => 'Data pribadi belum lengkap. Silahkan dicek kembali'
        ];
        $rules = [
            'nip' => 'required',
            'id_data_pribadi' => 'required'
        ];
        
        return Validator::make($data, $rules, $messages);
    }

    public function beforeStore()
    {
        // Simpan data pribadi dan alamat terlebih dahulu
        $data_pribadi = $this->setupDataPribadi();

        // Ambil ID data pribadi dan gabungkan ke request utama
        if($data_pribadi['status'] === 'success')
            request()->merge(['id_data_pribadi' => $data_pribadi['data']->id]);
    }

    public function beforeUpdate()
    {
        (new DataPribadiService())->updatePersonalData(
            request()->get('id_data_pribadi'),
            request()->get('id_alamat')
        );
    }

    private function setupDataPribadi()
    {
        return (new DataPribadiService())->createPersonalData();
    }
}