<?php

namespace App\Http\Controllers\Prime;

use App\Http\Controllers\Base\PrimeController;
use App\Repositories\Prime\PesertaDidikRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use SevenArts\Models\Prime\PesertaDidik;
use SevenArts\Services\DataPribadiService;

class PesertaDidikController extends PrimeController
{
    public function __construct()
    {
        $this->req_payload              = new \stdClass();
        $this->req_payload->model       = PesertaDidik::class;
        $this->req_payload->repository  = new PesertaDidikRepository();
        $this->req_payload->next_route  = 'prime.peserta-didik.index';
        $this->req_payload->controller  = $this;
    }

    public function storeValidation()
    {
        return true;
    }

    //membuka list PesertaDidik
    public function index(Request $request)
    {
        list($daftar_peserta_didik, $adm_reg) = PesertaDidikRepository::dashboard();
        return primeView('peserta-didik.index', compact('daftar_peserta_didik', 'adm_reg'));
    }

    public function edit($id)
    {
        list($peserta_didik, $adm_reg) = PesertaDidikRepository::edit($id);
        return primeView('peserta-didik.edit', compact('peserta_didik', 'adm_reg'));
    }

    public function validator($data)
    {
        $messages = [
            'nipd.required' => 'Nomor Induk Peserta Didik tidak boleh kosong!',
            'id_data_pribadi.required' => 'Data pribadi belum lengkap. Silahkan dicek kembali'
        ];
        $rules = [
            'nipd' => 'required',
            'id_data_pribadi' => 'required'
        ];
        
        return Validator::make($data, $rules, $messages);
    }

    public function beforeStore()
    {
        // Simpan data pribadi dan alamat terlebih dahulu
        $data_pribadi = $this->setupDataPribadi();

        // Ambil ID data pribadi dan gabungkan ke request utama
        if($data_pribadi['status'] === 'success')
            request()->merge(['id_data_pribadi' => $data_pribadi['data']->id]);
    }

    public function beforeUpdate()
    {
        (new DataPribadiService())->updatePersonalData(
            request()->get('id_data_pribadi'),
            request()->get('id_alamat')
        );
    }

    private function setupDataPribadi()
    {
        return (new DataPribadiService())->createPersonalData();
    }
}