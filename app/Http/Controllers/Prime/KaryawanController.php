<?php

namespace App\Http\Controllers\Prime;

use App\Http\Controllers\Base\PrimeController;
use App\Repositories\Prime\KaryawanRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use SevenArts\Models\Prime\Karyawan;
use SevenArts\Services\DataPribadiService;

class KaryawanController extends PrimeController
{
    public function __construct()
    {
        $this->req_payload              = new \stdClass();
        $this->req_payload->model       = Karyawan::class;
        $this->req_payload->repository  = new KaryawanRepository();
        $this->req_payload->next_route  = 'prime.karyawan.index';
        $this->req_payload->show_view   = 'pages.prime.karyawan.view';
        $this->req_payload->controller  = $this;
    }

    public function storeValidation()
    {
        return true;
    }

    //membuka list Karyawan
    public function index(Request $request)
    {
        list($daftar_karyawan, $adm_reg) = KaryawanRepository::dashboard();
        return primeView('karyawan.index', compact('daftar_karyawan', 'adm_reg'));
    }

    public function edit($id)
    {
        list($karyawan, $adm_reg) = KaryawanRepository::edit($id);
        return primeView('karyawan.edit', compact('karyawan', 'adm_reg'));
    }

    public function validator($data)
    {
        $messages = [
            'niy_nigk.required' => 'Nomor Induk Yayasan tidak boleh kosong!',
            'id_data_pribadi.required' => 'Data pribadi belum lengkap. Silahkan dicek kembali'
        ];
        $rules = [
            'niy_nigk' => 'required',
            'id_data_pribadi' => 'required'
        ];
        
        return Validator::make($data, $rules, $messages);
    }

    public function beforeStore()
    {
        // Simpan data pribadi dan alamat terlebih dahulu
        $data_pribadi = $this->setupDataPribadi();

        // Ambil ID data pribadi dan gabungkan ke request utama
        if($data_pribadi['status'] === 'success')
            request()->merge(['id_data_pribadi' => $data_pribadi['data']->id]);
    }

    public function beforeUpdate()
    {
        (new DataPribadiService())->updatePersonalData(
            request()->get('id_data_pribadi'),
            request()->get('id_alamat')
        );
    }

    private function setupDataPribadi()
    {
        return (new DataPribadiService())->createPersonalData();
    }
}