<?php

namespace App\Http\Controllers\Prime;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Base\PrimeController;
use App\Repositories\Prime\SekolahRepository;
use SevenArts\Models\Prime\Sekolah;
use SevenArts\Services\AlamatService;
use SevenArts\Support\Traits\BasicCRUD\FieldsFilter;

class SekolahController extends PrimeController
{
    use FieldsFilter;

    protected $req_payload;

    public function __construct()
    {
        $this->req_payload              = new \stdClass();
        $this->req_payload->model       = Sekolah::class;
        $this->req_payload->repository  = new SekolahRepository();
        $this->req_payload->next_route  = 'prime.sekolah.index';
        $this->req_payload->show_view   = 'pages.prime.sekolah.view';
        $this->req_payload->controller  = $this;
    }

    public function storeValidation()
    {
        return true;
    }

    public function updateValidation()
    {
        return true;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        list($daftar_sekolah, $tingkatan_sekolah, $adm_reg) = SekolahRepository::dashboard();
        
        return view('pages.prime.sekolah.index', compact('daftar_sekolah', 'tingkatan_sekolah', 'adm_reg'));
    }

    public function edit($id)
    {
        list($sekolah, $tingkatan_sekolah, $adm_reg) = SekolahRepository::edit($id);

        return view('pages.prime.sekolah.edit', compact('sekolah', 'tingkatan_sekolah', 'adm_reg'));
    }

    /**
     * Validator untuk pembuatan dan pembaruan entitas
     *
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Support\Facades\Validator
     */
    protected function validator($data)
    {
        // Error messages
        $messages = [
            'nama_sekolah.required' => 'Nama Sekolah tidak boleh kosong!',
            'id_alamat.required' => 'Pilih alamat terlebih dahulu',
            'id_tingkat_sekolah.required' => 'Tingkatan sekolah belum dipilih'
        ];
        // Validation rules
        $rules = [
            'nama_sekolah' => 'required|string|min:6',
            'id_alamat' => 'required',
            'id_tingkat_sekolah' => 'required'
        ];

        // Validation
        return Validator::make($data, $rules, $messages);
    }

    /**
     * Simpan alamat terlebih dahulu
     *
     * @param  array
     * @return void
     */
    protected function beforeStore()
    {
        $alamat = (new AlamatService)->createAlamat();
        if($alamat['status'] === 'success')
            request()->merge(['id_alamat' => $alamat['data']->id]);
    }

    protected function beforeUpdate()
    {
        (new AlamatService)->updateAlamat(request()->get('id_alamat'));
    }
}
