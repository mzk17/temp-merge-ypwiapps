<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Constants\JenisAkun as IDJenisAkun;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Auth\RegistersUsers;
use SevenArts\Models\Prime\JenisAkun;
use SevenArts\Models\Prime\JabatanAkun;
use SevenArts\Models\Prime\VerifikasiAkunPengguna;
use SevenArts\Models\Constant\TingkatSekolah;
use App\Mail\VerifyUserRegistered;
use Carbon\Carbon;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/pr/verify/eona';

    protected $id_jenis_akun;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show pre-registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showPreRegistrationForm()
    {
        $sekolah = TingkatSekolah::with([
            'sekolah:id,nama_sekolah,id_tingkat_sekolah,id_alamat',
            'sekolah.alamat:id,alamat'
            ])->get();
        $sekolah = json_encode($sekolah);
        
        return view('auth.pre-register', compact('sekolah'));
    }

    /**
     * Show normal registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $jenisAkun = JenisAkun::all();
        $jabatan = JabatanAkun::all();

        return view('auth.register', compact(['jenisAkun', 'jabatan']));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $this->id_jenis_akun = (int)$data['id_jenis_akun'];

        return Validator::make($data, [
            'id' => ['required', 'string', 'min:4', 'unique:akun_pengguna'],
            'id_jenis_akun' => ['required', 'integer', 'min:1'],
            'id_jabatan' => Rule::requiredIf($this->id_jenis_akun === IDJenisAkun::PEGAWAI_YAYASAN || $this->id_jenis_akun === IDJenisAkun::PEGAWAI_SEKOLAH),
            'username' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:akun_pengguna'],
            'password' => ['required', 'string', 'min:1', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        /**
         * Minimum registration's parameters.
         * Only for account table (akun_pengguna)
         */
        $baseData = [
            'id' => $data['id'],
            'id_jenis_akun' => $this->id_jenis_akun,
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ];

        /**
         * Additional parameter for
         * Employees registration
         */
        if ($this->id_jenis_akun === IDJenisAkun::PEGAWAI_YAYASAN ||
            $this->id_jenis_akun === IDJenisAkun::PEGAWAI_SEKOLAH) {
            $additionalData = [
                'id_jabatan' => $data['id_jabatan']
            ];

            $baseData = array_merge($baseData, $additionalData);
        }

        /**
         * Complete registration parameters,
         * including user personal data
         */

        return User::create($baseData);
    }
}
