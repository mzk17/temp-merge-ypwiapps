<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Helpers\RegistrationVerification;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\VerifiesEmails;
use SevenArts\Models\Prime\VerifikasiAkunPengguna;

class VerificationController extends Controller
{
    use RegistrationVerification;
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    | Token status:
    | 0 - Aktif
    | 1 - Verified
    | 2 - Expired
    */

    use VerifiesEmails;

    /**
     * Where to redirect users after verification.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('signed')->only('verify');
        $this->middleware('throttle:6,1')->only('verify', 'resend');
    }

    public function verificationAccount($token)
    {
        // Membandingkan waktu kadaluarsa token dengan waktu saat ini
        $sekarang = Carbon::now();

        // Verification data
        $verification = VerifikasiAkunPengguna::where('token', $token)
            ->where('status_token', 0)
            ->first();

        if ($verification && $sekarang->lessThan($verification->waktu_kadaluarsa_token)) {
            // Update token status
            $verification->status_token = 1;
            $verification->update();

            // Update verified email in User table
            $user = $verification->akunPengguna;
            $user->email_verified_at = $sekarang;
            $user->update();

            return redirect()->route('prime.show-fcr');
        } else {
            return view('auth.verification-expired');
        }
    }

    public function verificationNotice()
    {
        // Cek apakah user sudah mendapatkan email / token verifikasi
        $token_verifikasi = $this->tokenActiveUser();
        if (is_null($token_verifikasi)) {
            $this->sendEmailVerificationNotification();
        }
        
        return view('auth.verify');
    }

    public function resentEmailVerification()
    {
        $token_verifikasi = $this->tokenActiveUser();

        // Update existing token to expired
        if (! is_null($token_verifikasi)) {
            $token_verifikasi->status = 2;
            $token_verifikasi->update();
        }

        // Send email verification again with new token
        $this->sendEmailVerificationNotification();

        return redirect()->route('prime.verification-notice');
    }

    protected function tokenActiveUser()
    {
        return VerifikasiAkunPengguna::where('id_akun_pengguna', request()->user()->getAuthIdentifier())
            ->where('status_token', 0)
            ->first();
    }
}
