<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Constants\JenisAkun as IDJenisAkun;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Auth\RegistersUsers;
use SevenArts\Models\Prime\JenisAkun;
use SevenArts\Models\Prime\JabatanAkun;
use SevenArts\Models\Prime\VerifikasiAkunPengguna;
use SevenArts\Models\Constant\TingkatSekolah;
use App\Mail\VerifyUserRegistered;
use Carbon\Carbon;

class PreRegisterController extends Controller
{
    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/prime/verify/eona';

    protected $id_jenis_akun;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show pre-registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showPreRegistrationForm()
    {
        $sekolah = TingkatSekolah::with([
            'sekolah:id,nama_sekolah,id_tingkat_sekolah,id_alamat',
            'sekolah.alamat:id,alamat'
            ])->get();
        $sekolah = json_encode($sekolah);
        $new_id = \Str::uuid();
        
        return view('auth.pre-register', compact(['sekolah', 'new_id']));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        // For real condition, this should be check by a flag
        // For example, $data['jenis_lembaga']: 1 - Yayasan, 2 - Sekolah
        // $this->id_jenis_akun = (int)$data['id_jenis_akun'];

        return Validator::make($data, [
            'id' => ['required', 'string', 'min:4', 'unique:akun_pengguna'],
            'id_jenis_akun' => ['required', 'integer', 'min:1'],
            'id_data_pribadi' => ['required', 'string', 'unique:akun_pengguna'],
            'id_data_jenis_akun' => ['string', 'unique:akun_pengguna'],
            'username' => ['required', 'string', 'unique:akun_pengguna', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:akun_pengguna'],
            'password' => ['required', 'string', 'min:1', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $data['password'] = Hash::make($data['password']);
        return User::create($data);
    }
}
