<?php

namespace App\Http\Controllers\AssetInventory\Transaksi;

use Storage;
use Illuminate\Http\Request;
use App\Http\Controllers\Base\PrimeController;
use App\Repositories\{
    AssetInventory\Transaksi\TanahTransaksiRepository,
    AssetInventory\KategoriRepository,
    AssetInventory\RuanganRepository,
    Prime\SekolahRepository
};
use SevenArts\Models\AssetInventory\{ TanahTransaksi, JenisTanah };

class TanahController extends PrimeController
{
    protected $req_payload;

    public function __construct()
    {
        $this->req_payload = new \stdClass();
        $this->req_payload->model = TanahTransaksi::class;
        $this->req_payload->repository = new TanahTransaksiRepository();
        $this->req_payload->next_route = 'asset-inventory.transaksi.tanah.index';
        $this->req_payload->controller = $this;
    }

    //membuka list Tanah
    public function index(Request $request)
    {
        list($data_tanah_trans, $c_kategori, $c_sekolah) = ($request->has('cari'))
             ? TanahTransaksiRepository::cariTanahTransaksi($request->get('cari'))
             : TanahTransaksiRepository::dashboard();

        $c_jenis = JenisTanah::all();
        
        return assetInventoryView('tanah_trans.index', compact('data_tanah_trans', 'c_kategori', 'c_jenis', 'c_sekolah'));
    }

    //mengedit list Tanah
    public function edit($id)
    {
        $tanah_trans  = $this->req_payload->repository->find($id);

        return assetInventoryView('tanah_trans.edit', compact('tanah_trans'));
    }

    public function afterUpdate($tanah)
    {
        if(request()->hasFile('foto_tnh')) {
            Storage::delete(request()->get('foto_tnh'));
            request()->file('foto_tnh')->move('images/', request()->file('foto_tnh')->getClientOriginalName());
            $tanah->foto_tnh = request()->file('foto_tnh')->getClientOriginalName();
            $tanah->save();
        }
    }
}
