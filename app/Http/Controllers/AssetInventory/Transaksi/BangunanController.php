<?php

namespace App\Http\Controllers\AssetInventory\Transaksi;

use Illuminate\Http\Request;
use App\Http\Controllers\Base\PrimeController;
use App\Repositories\AssetInventory\Transaksi\BangunanRepository;
use SevenArts\Models\AssetInventory\{ BangunanTransaksi, Kategori, Ruangan };
use Storage;

class BangunanController extends PrimeController
{
    protected $req_payload;

    public function __construct()
    {
        $this->req_payload = new \stdClass();
        $this->req_payload->model = BangunanTransaksi::class;
        $this->req_payload->repository = new BangunanRepository();
        $this->req_payload->next_route = 'asset-inventory.transaksi.bangunan.index';
        $this->req_payload->controller = $this;
    }

    //membuka list Bangunan
    public function index(Request $request)
    {
        if($request->has('cari')){
            $data_bangunan_trans = BangunanRepository::cariBangunan($request->get('cari'));
        }else{
            list($data_bangunan_trans, $c_kategori, $c_ruangan, $c_sekolah) = BangunanRepository::dashboard();
        }

        return assetInventoryView('bangunan_trans.index', compact('data_bangunan_trans', 'c_kategori', 'c_ruangan', 'c_sekolah'));
    }

    //mengedit list Bangunan
    public function edit($id)
    {
        $bangunan_trans = $this->req_payload->repository->find($id);
        
        return assetInventoryView('bangunan_trans.edit', compact('bangunan_trans'));
    }

    /*
    //membuat dan menambah Bangunan
    public function create(Request $request)
    {
        BangunanTransaksi::create($request->all()); 
        return redirect('/bangunan_trans')->with('sukses','Data berhasil diinput');
    }

    //mengupdate proses Bangunan
    public function update(Request $request,$id)
    {
        $bangunan_trans = BangunanTransaksi::find($id);
        $bangunan_trans->update($request->all());        
        if($request->hasFile('foto_avatar')){
            Storage::delete($request->foto_avatar);
            $request->file('foto_avatar')->move('images/', $request->file('foto_avatar')->getClientOriginalName());
            $bangunan_trans->foto_avatar = $request->file('foto_avatar')->getClientOriginalName();
            $bangunan_trans->save();
        }
        return redirect('/bangunan_trans')->with('sukses','Data berhasil diupdate');
    }

    //mendelete proses Bangunan 
    public function delete($id)
    {
        $bangunan_trans = BangunanTransaksi::find($id);
        $bangunan_trans->delete();
        return redirect('/bangunan_trans')->with('sukses','Data berhasil dihapus');
    }

    //public function profile($id);
    //{
        //$sekolah = \App\Sekolah::all();
        //return view('bangunan_trans.') 
    //}
    */
}
