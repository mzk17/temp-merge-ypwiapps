<?php

namespace App\Http\Controllers\AssetInventory\Transaksi;

use Storage;
use Illuminate\Http\Request;
use App\Sekolah;
use App\Exports\AlatTransaksiExport;
use App\Http\Controllers\Base\PrimeController;
use App\Repositories\AssetInventory\Transaksi\AlatRepository;
use Maatwebsite\Excel\Facades\Excel;
use SevenArts\Models\AssetInventory\{ AlatTransaksi, Barang, Kategori };

class AlatController extends PrimeController
{
    protected $req_payload;

    public function __construct()
    {
        $this->req_payload = new \stdClass();
        $this->req_payload->model = AlatTransaksi::class;
        $this->req_payload->next_route = 'asset-inventory.transaksi.alat.index';
        $this->req_payload->repository = new AlatRepository();
        $this->req_payload->controller = $this;
    }
    
    //membuka list Aset Alat
    public function index(Request $request)
    {
        if ($request->has('cari')) {
            $data_alat_trans = AlatRepository::cariAlat($request->get('cari'));
        } else {
            list($data_alat_trans, $c_barang, $c_kategori, $c_sekolah) = AlatRepository::dashboard();
        }

        return assetInventoryView('alat_trans.index', compact('data_alat_trans', 'c_barang', 'c_kategori', 'c_sekolah'));
    }

    //mengedit list Aset 
    public function edit($id)
    {
        $alat_trans = $this->req_payload->repository->find($id);
        
        return assetInventoryView('alat_trans.edit', compact('alat_trans', 'barang', 'kategori', 'sekolah'));
    }

    /*
    //membuat dan menambah Aset
    public function create(Request $request)
    {
        AlatTransaksi::create($request->all());
        return redirect()
            ->route('asset-inventory.transaksi.alat.index')
            ->with('sukses','Data berhasil diinput');
    }
    //mengupdate proses Aset 
    public function update(Request $request,$id)
    {
        $alat_trans = AlatTransaksi::find($id);
        $alat_trans->update($request->all());
        if($request->hasFile('fotoavatar'));        
        Storage::$request->delete('images/', $request->file('fotoavatar')->getClientOriginalName());{
            $request->file('fotoavatar')->move('images/', $request->file('fotoavatar')->getClientOriginalName());
            $alat_trans->fotoavatar = $request->file('fotoavatar')->getClientOriginalName();
            $alat_trans->save();
        }
        return redirect()
            ->route('asset-inventory.transaksi.alat.index')
            ->with('sukses','Data berhasil diupdate');
    }
    //mendelete proses Aset 
    public function delete($id)
    {
        $alat_trans = AlatTransaksi::find($id);
        $alat_trans->delete();
        return redirect()
            ->route('asset-inventory.transaksi.alat.index')
            ->with('sukses','Data berhasil dihapus');
    }
    */

    public function export() 
    {
        return Excel::download(new AlatTransaksiExport, 'alat_transaksi.xlsx');
    }

    //public function profile($id);
    //{
        //$sekolah = Sekolah::all();
        //return view('alat_trans.') 
    //}
    
}
