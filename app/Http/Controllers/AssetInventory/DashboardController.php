<?php

namespace App\Http\Controllers\AssetInventory;

use App\Http\Controllers\Base\AssetInventoryController;

class DashboardController extends AssetInventoryController
{
    // index home
    public function index()
    {
        return assetInventoryView('dashboard.index');
    }
}
