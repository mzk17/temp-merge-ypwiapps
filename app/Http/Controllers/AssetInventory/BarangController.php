<?php

namespace App\Http\Controllers\AssetInventory;

use Illuminate\Http\Request;
use App\Http\Controllers\Base\PrimeController;
use App\Repositories\AssetInventory\BarangRepository;
use Illuminate\Support\Facades\Validator;
use SevenArts\Models\AssetInventory\Barang;
use SevenArts\Support\Traits\BasicCRUD\FieldsFilter;

class BarangController extends PrimeController
{
    use FieldsFilter;

    protected $req_payload;

    public function __construct()
    {
        $this->req_payload             = new \stdClass();
        $this->req_payload->model      = Barang::class;
        $this->req_payload->repository = new BarangRepository();
        $this->req_payload->next_route = 'asset-inventory.barang.index';
        $this->req_payload->controller = $this;
    }

    //membuka list Barang
    public function index(Request $request)
    {
        $data_barang = BarangRepository::dashboard($request->get('cari'));

        return assetInventoryView('barang.index', ['data_barang' => $data_barang]);
    }

    //mengedit list Barang
    public function edit($id)
    {
        $barang = $this->req_payload->repository->find($id);
        return assetInventoryView('barang.edit', compact('barang'));
    }

    public function storeValidation()
    {
        return true;
    }

    public function updateValidation()
    {
        return true;
    }

    protected function validator($data)
    {
        $rules            = ['nama_barang' => 'required|min:3'];

        return Validator::make($data, $rules);
    }
}
