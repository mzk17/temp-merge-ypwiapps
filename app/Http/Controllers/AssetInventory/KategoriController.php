<?php

namespace App\Http\Controllers\AssetInventory;

use Illuminate\Http\Request;
use App\Http\Controllers\Base\PrimeController;
use App\Repositories\AssetInventory\KategoriRepository;
use SevenArts\Models\AssetInventory\Kategori;

class KategoriController extends PrimeController
{
    protected $req_payload;

    public function __construct()
    {
        $this->req_payload             = new \stdClass();
        $this->req_payload->model      = Kategori::class;
        $this->req_payload->repository = new KategoriRepository();
        $this->req_payload->next_route = 'asset-inventory.kategori.index';
        $this->req_payload->controller = $this;
    }

    //membuka list Kategori
    public function index(Request $request)
    {
        $data_kategori = KategoriRepository::dashboard($request->get('cari'));

        return assetInventoryView('kategori.index', compact('data_kategori'));
    }

    //mengedit list Kategori
    public function edit($id)
    {
        $kategori = $this->req_payload->repository->find($id);
        return assetInventoryView('kategori.edit', compact('kategori'));
    }
}
