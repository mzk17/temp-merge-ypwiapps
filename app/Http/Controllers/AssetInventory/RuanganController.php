<?php

namespace App\Http\Controllers\AssetInventory;

use Illuminate\Http\Request;
use App\Http\Controllers\Base\PrimeController;
use App\Repositories\AssetInventory\RuanganRepository;
use SevenArts\Models\AssetInventory\Ruangan;

class RuanganController extends PrimeController
{
    protected $req_payload;

    public function __construct()
    {
        $this->req_payload             = new \stdClass();
        $this->req_payload->model      = Ruangan::class;
        $this->req_payload->repository = new RuanganRepository();
        $this->req_payload->next_route = 'asset-inventory.ruangan.index';
        $this->req_payload->controller = $this;   
    }

    //membuka list Ruangan
    public function index(Request $request)
    {
        $data_ruangan = RuanganRepository::dashboard($request->get('cari'));

        return assetInventoryView('ruangan.index', compact('data_ruangan'));
    }

    //mengedit list Ruangan
    public function edit($id)
    {
        $ruangan = $this->req_payload->repository->find($id);
        return assetInventoryView('ruangan.edit', compact('ruangan'));
    }
}
