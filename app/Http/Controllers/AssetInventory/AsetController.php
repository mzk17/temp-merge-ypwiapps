<?php

namespace App\Http\Controllers\AssetInventory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AsetController extends Controller
{
    //membuka list Aset
    public function index(Request $request)
    {
        if($request->has('cari')){
            $data_aset = \App\Aset::where('nama_aset','LIKE','%'.$request->cari.'%')->get();
        }else{
            $data_aset = \App\Aset::all();
        }        
        return view('aset.index', ['data_aset' => $data_aset]);
    }

    //membuat dan menambah Aset
    public function create(Request $request)
    {
        \App\Aset::create($request->all());
        return redirect('/aset')->with('sukses','Data berhasil diinput'); 
    }

    //mengedit list Aset 
    public function edit($id)
    {
        $aset = \App\Aset::find($id);
        return view('aset/edit',['aset' => $aset]);
    }

    //mengupdate proses Aset 
    public function update(Request $request,$id)
    {
        $aset = \App\Aset::find($id);
        $aset->update($request->all());
        return redirect('/aset')->with('sukses','Data berhasil diupdate');
    }

    //mendelete proses Aset 
    public function delete($id)
    {
        $aset = \App\Aset::find($id);
        $aset->delete();
        return redirect('/aset')->with('sukses','Data berhasil dihapus');
    }

    
}
