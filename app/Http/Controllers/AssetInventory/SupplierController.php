<?php

namespace App\Http\Controllers\AssetInventory;

use Illuminate\Http\Request;
use App\Http\Controllers\Base\PrimeController;
use App\Repositories\AssetInventory\SupplierRepository;
use Illuminate\Support\Facades\Validator;
use SevenArts\Models\AssetInventory\Supplier;

class SupplierController extends PrimeController
{
    protected $req_payload;

    public function __construct()
    {
        $this->req_payload             = new \stdClass();
        $this->req_payload->model      = Supplier::class;
        $this->req_payload->repository = new SupplierRepository();
        $this->req_payload->next_route = 'asset-inventory.supplier.index';
        $this->req_payload->controller = $this;
    }

    public function storeValidation()
    {
        return true;
    }

    public function updateValidation()
    {
        return true;
    }
    
    //membuka list Supplier
    public function index(Request $request)
    {
        $data_supplier = SupplierRepository::dashboard($request->get('cari'));

        return assetInventoryView('supplier.index', compact('data_supplier'));
    }

    //mengedit list Supplier
    public function edit($id)
    {
        $supplier = $this->req_payload->repository->find($id);
        return assetInventoryView('supplier.edit', compact('supplier'));
    }

    public function validator($data)
    {
        $rules = [
            'nama_supplier' => 'required|string|min:3',
            'alamat_supplier' => 'required|string|min:6',
            'telepon_supplier' => 'required|min:6',
            'email_supplier' => 'email'
        ];

        return Validator::make($data, $rules);
    }
}
