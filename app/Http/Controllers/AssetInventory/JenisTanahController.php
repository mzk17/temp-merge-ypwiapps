<?php

namespace App\Http\Controllers\AssetInventory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use SevenArts\Models\AssetInventory\JenisTanah;

class JenisTanahController extends Controller
{
    // Membuka list jenis tanah
    public function index(Request $request)
    {
        if($request->has('cari')){
            $data_jenis = JenisTanah::where('jenis_tanah','LIKE','%'.$request->cari.'%')->get();
        }else{
            $data_jenis = JenisTanah::all();
        }        
        return assetInventoryView('jenis-tanah.index', compact('data_jenis'));
    }

    // Membuat dan menambah Jenis tanah
    public function create(Request $request)
    {
        JenisTanah::create($request->all());
        return redirect()
            ->route('asset-inventory.jenis-tanah.index')
            ->with('sukses','Data berhasil di input'); 
    }

    // Mengedit list Jenis tanah
    public function edit($id)
    {
        $jenis= JenisTanah::find($id);
        return assetInventoryView('jenis-tanah.edit', compact('jenis'));
    }

    // Mengupdate proses Jenis tanah
    public function update(Request $request,$id)
    {
        $jenis = JenisTanah::find($id);
        $jenis->update($request->all());

        return redirect()
            ->route('asset-inventory.jenis-tanah.index')
            ->with('sukses','Data berhasil diupdate');
    }

    // Mendelete proses Jenis tanah
    public function delete($id)
    {
        $jenis = JenisTanah::find($id);
        $jenis->delete();

        return redirect()
            ->route('asset-inventory.jenis-tanah.index')
            ->with('sukses','Data berhasil dihapus');
    }
}
