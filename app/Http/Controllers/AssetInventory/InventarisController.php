<?php

namespace App\Http\Controllers\AssetInventory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InventarisController extends Controller
{
    //membuka list Inventaris
    public function index(Request $request)
    {
        if($request->has('cari')){
            $data_inventaris = \App\Inventaris::where('nama_inventaris','LIKE','%'.$request->cari.'%')->get();
        }else{
            $data_inventaris = \App\Inventaris::all();
        }        
        return view('inventaris.index', ['data_inventaris' => $data_inventaris]);
    }

    //membuat dan menambah Inventaris
    public function create(Request $request)
    {
        \App\Inventaris::create($request->all());
        return redirect('/inventaris')->with('sukses','Data berhasil di input'); 
    }

    //mengedit list Inventaris
    public function edit($id)
    {
        $inventaris = \App\Inventaris::find($id);
        return view('inventaris/edit',['inventaris' => $inventaris]);
    }

    //mengupdate proses Inventaris
    public function update(Request $request,$id)
    {
        $inventaris = \App\Inventaris::find($id);
        $inventaris->update($request->all());
        return redirect('/inventaris')->with('sukses','Data berhasil diupdate');
    }

    //mendelete proses Inventaris 
    public function delete($id)
    {
        $inventaris = \App\Inventaris::find($id);
        $inventaris->delete();
        return redirect('/inventaris')->with('sukses','Data berhasil dihapus');
    }

    
}
