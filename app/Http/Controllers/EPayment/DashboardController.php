<?php

namespace App\Http\Controllers\EPayment;

use App\Http\Controllers\Base\BasicController;

class DashboardController extends BasicController
{
    public function index()
    {
        return view('pages.epayment.dashboard');
    }
}