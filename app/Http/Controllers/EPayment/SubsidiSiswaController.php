<?php

namespace App\Http\Controllers\EPayment;

use Illuminate\Http\Request;
use App\Http\Controllers\Base\PrimeController;
use App\Repositories\EPayment\SubsidiSiswaRepository;
use SevenArts\Models\EPayment\SubsidiPembiayaan;
use SevenArts\Support\Facades\EPayment\SubsidiRequestor;

class SubsidiSiswaController extends PrimeController
{
    public function __construct()
    {
        $this->req_payload              = new \stdClass();
        $this->req_payload->model       = SubsidiPembiayaan::class;
        $this->req_payload->repository  = new SubsidiSiswaRepository();
        $this->req_payload->next_route  = 'prime.lembaga.index';
        $this->req_payload->show_view   = 'pages.prime.lembaga.view';
        $this->req_payload->controller  = $this;
    }

    public function earlyPhase(Request $request)
    {
        $payload = SubsidiRequestor::earlyPhasePayload($request->all());

        return ($this->req_payload->repository->simpanSekaligus($payload))
            ? response()->json("Hurray!")
            : response()->json("Failed!!!");
    }
}