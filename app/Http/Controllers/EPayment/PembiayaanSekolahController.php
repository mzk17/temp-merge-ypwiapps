<?php

namespace App\Http\Controllers\EPayment;

use Illuminate\Http\Request;
use App\Constants\FrekuensiItemPembiayaan;
use App\Http\Controllers\Base\BasicController;
use SevenArts\Services\Factories\EPayment\PembiayaanSekolahFactory;

class PembiayaanSekolahController extends BasicController
{
    public $factory;

    public function __construct(PembiayaanSekolahFactory $pembiayaan_sekolah_factory)
    {
        $this->factory = $pembiayaan_sekolah_factory;
    }

    public function buatPembiayaan(Request $request)
    {
        try {
            $pembiayaan = new \SevenArts\Services\Factories\EPayment\PembiayaanSekolahFactory();
            
            return ($request->frekuensi === FrekuensiItemPembiayaan::BULANAN)
                ? $pembiayaan->buatPembiayaanRutin($request)
                : $pembiayaan->buatPembiayaanTunggal($request);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
