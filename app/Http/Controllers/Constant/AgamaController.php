<?php

namespace App\Http\Controllers\Constant;

use SevenArts\Models\Constant\Agama;

class AgamaController
{
    public function index()
    {
        return response()->json([
            'agama' => Agama::all()
        ]);
    }
}