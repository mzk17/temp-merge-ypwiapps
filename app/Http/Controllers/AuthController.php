<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    //autentifikasi masuk
    public function login()
    {
        return view('auths.login');
    }

    //proseslogin
    public function postlogin(Request $request)
    {
        //dd($request->all());
        if(Auth::attempt($request->only('email','password'))){
            return redirect('/dashboard');
        }

        return redirect ('/login');
    }

    //autentifikasi keluar
    public function logout()
    {
        Auth::logout();
        return redirect ('/login');
    }
}
