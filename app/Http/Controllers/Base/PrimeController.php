<?php

/**
 * ------------------------------------------------------------------------
 * Controller ini memiliki fungsi Create, Update, Delete
 * secara default untuk zero / master data.
 * Fungsi - fungsi tersebut tidak memiliki logika tambahan.
 * Hanya logika dasar, dimana sangat cocok untuk pengolahan master data.
 * ------------------------------------------------------------------------
 */

namespace App\Http\Controllers\Base;

use Illuminate\Http\Request;
use SevenArts\Support\Traits\BasicCRUD\MethodCall;
use SevenArts\Contracts\BasicResourceMethods;
use SevenArts\Support\Traits\BasicCRUD\FieldsFilter;

class PrimeController extends BasicController
{
    use MethodCall, FieldsFilter;

    protected $user;

    /**
     * $this->req_payload
     * Di definisikan pada masing - masing child controller
     */
    public function __construct()
    {
        # $this->setActiveUser();
    }

    /**
     * Sejak Laravel 5.3,
     * constructor tidak bisa berinteraksi dengan session.
     * Itulah tujuan dibuatnya fungsi ini
     */
    protected function setActiveUser()
    {
        $this->middleware(function ($request, $next) {
            $this->user = \Auth::user();

            return $next($request);
        });
    }

    /**
     * Display the specified resource.
     *
     * @param BasicResourceMethods $brm
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(BasicResourceMethods $brm, $id)
    {
        $this->req_payload->id_model = $id;

        return $brm->actionShow($this->req_payload);
    }

    /**
     * Simpan data baru.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BasicResourceMethods $brm, Request $request)
    {
        // Jalankan fungsi beforeStore jika ada
        $this->sideActionExecution('beforeStore', $this);

        // Validasi data yang akan dibuat
        $this->defineValidatedData($request, __FUNCTION__);

        return $brm->actionStore($this->req_payload);
    }

    /**
     * Perbarui data.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BasicResourceMethods $brm, Request $request, $id)
    {
        // Jalankan fungsi beforeUpdate jika ada
        $this->sideActionExecution('beforeUpdate', $this);

        // Validasi data yang akan di-update
        $this->defineValidatedData($request, __FUNCTION__);

        return $brm->actionUpdate($this->req_payload, $id);
    }

    /**
     * Hapus data.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(BasicResourceMethods $brm, $id)
    {
        // Jalankan fungsi beforeDestroy jika ada
        $this->sideActionExecution('beforeDestroy', $this);

        return $brm->actionDestroy($this->req_payload, $id);
    }

    /**
     * Lakukan validasi data dan definisikan kedalam req_payload.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function defineValidatedData($request, $action_name)
    {
        // Ambil kolom sesuai pada tabel
        $fields = $this->getNecessaryFieldsFrom($this->req_payload->model);
        $data = $request->only($fields);

        $action_validation = $action_name . "Validation";
        if (method_exists($this, $action_validation) && $this->{$action_validation}()) {
            // Masukkan action_name kedalam request
            $request->merge(['action_name' => $action_name]);

            // Masukkan pesan error dari beforeAction jika validasinya gagal
            $this->req_payload->beforeActionFail = $request->get('before_action_errors');

            /**
             * Fungsi 'validator' dan property 'req_payload'
             * ada di child controller masing - masing
             */
            $this->req_payload->needValidated = true;
            $this->req_payload->data          = $this->validator($data);
        } else {
            $this->req_payload->needValidated = false;
            $this->req_payload->data          = $data;
        }
    }
}
