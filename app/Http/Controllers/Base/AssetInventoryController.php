<?php

namespace App\Http\Controllers\Base;

use App\Http\Controllers\Controller;

class AssetInventoryController extends Controller
{
    public $viewFolder = 'asset-inventory.';
}