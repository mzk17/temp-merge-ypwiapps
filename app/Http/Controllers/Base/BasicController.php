<?php

namespace App\Http\Controllers\Base;

use App\Http\Controllers\Controller;
use SevenArts\Support\Traits\BasicCRUD\FieldsFilter;

class BasicController extends Controller
{
    use FieldsFilter;

    protected $payload_response;
}
