<?php

namespace App\Http\Controllers;

use App\Constants\FrekuensiItemPembiayaan;
use App\Events\EPayment\PembiayaanSekolahCreated;
use App\Events\EPayment\TransaksiCreated;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Base\PrimeController;
use App\Helpers\Excel\Importers\PesertaDidikImport;
use App\Helpers\Excel\Importers\TenagaPendidikImport;
use Illuminate\Http\RedirectResponse;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Response;
use Illuminate\Routing\ResponseFactory as RoutingResponseFactory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use SevenArts\Models\EPayment\PembiayaanSekolah;
use SevenArts\Models\EPayment\PembiayaanSiswa;
use SevenArts\Models\EPayment\RincianPembiayaan;
use SevenArts\Models\EPayment\Transaksi;
use SevenArts\Models\Prime\TenagaPendidik;
use SevenArts\Models\Prime\Sekolah;
use SevenArts\Repositories\EPayment\PembiayaanSiswaRepository;
use SevenArts\Repositories\EPayment\TransaksiRepository;
// use Maatwebsite\Excel\Excel;
use SevenArts\Repositories\PersonalRepository;
use SevenArts\Support\Facades\EPayment\PembiayaanResponse;
use SevenArts\Support\Facades\EPayment\TransaksiResponse;
use SevenArts\Support\Traits\BasicCRUD\MethodCall as BasicCRUDMethodCall;
use SevenArts\Support\Traits\PayloadTimestamp;
use SevenArts\Validators\AlamatValidator;
use stdClass;

class ExperimentController extends PrimeController
{
    use BasicCRUDMethodCall, PayloadTimestamp;

    protected $inc;

    public function __construct()
    {
        $this->inc = 0;
    }

    public function repositoryDataLengkap()
    {
        $user_id = User::select('id')->first()->id;
        $repo = new PersonalRepository($user_id);

        return response()->json([
            'data' => $repo->dataLengkap()
        ]);
    }

    public function repositoryDataPribadi()
    {
        $user_id = User::select('id')->first()->id;
        $repo = new PersonalRepository($user_id);

        return response()->json([
            'data' => $repo->dataDiri()
        ]);
    }

    public function repositoryPesertaDidik()
    {
        $user_id = User::select('id')->first()->id;
        $repo = new PersonalRepository($user_id);

        return response()->json([
            'data' => $repo->dataPesertaDidik()
        ]);
    }

    public function repositoryGuru()
    {
        $user_id = User::select('id')->first()->id;
        $repo = new PersonalRepository($user_id);

        return response()->json([
            'data' => $repo->dataGuru()
        ]);
    }

    /**
     * Reques parameters:
     * @param excel_file       | file
     * @param model            | name of import classes, eg. PesertaDidik
     * @param tipe_tendik      | special flag for TenagaPendidikImport class
     */
    public function import(Request $request)
    {
        $import_class = "\App\Helpers\Excel\Importers\\" . $request->model . "Import";

        try {
            \Excel::import(
                ($request->model === "TenagaPendidik" && $request->guru)
                    ? new $import_class(\App\Constants\TipeTenagaKependidikan::GURU)
                    : new $import_class($request->model),
                $request->excel_file
            );

            return response()->json("Yeay");
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function daftarTendik($id_sekolah)
    {
        $sekolah = Sekolah::find($id_sekolah);
        foreach ($sekolah->tenagaPendidik as $key => $tendik) {
            if (is_null($tendik->akunPengguna)) {
                $tendik_ = $tendik->dataPribadi->only(['id', 'nama', 'nik', 'email']);
                $tendik_['nik'] = substr_replace($tendik_['nik'], "xxxxxx", 6, 6);
                $tendik_['id_data_jenis_akun'] = $tendik->id;
                $output[] = $tendik_;
            }
        }
        return response()->json([
            'tenaga_pendidik' => $output
        ]);
    }

    public function preRegisterUser(Request $request)
    {
        dd($request->all());
        // Daftar akun: simpan informasi pendaftaran ke dalam tabel akun_pengguna

        //
    }

    public function actionName()
    {
        $actn = 'bfact';
        return response()->json($this->{$actn}());
    }

    protected function bfact()
    {
        return "Yeay";
    }

    protected function afterStore()
    {
        Log::info("afterStore berhasil tereksekusi dengan increment " . $this->inc++);
        // return redirect()->route('test.hasil-redirect');
    }

    public function cobaRedirect()
    {
        if ($this->sideActionExecution('afterStore', $this) instanceof RedirectResponse)
            return $this->sideActionExecution('afterStore', $this);

        return "Ngga redirect";
    }

    public function changeReferenceData()
    {
        $payload = new \stdClass();
        $payload->data = "Initial data";
        $payload->needValidated = true;

        $output['initial'] = $payload->data;

        $this->defineData($payload);

        $output['setelah_define_data'] = $payload->data;

        $payload->data = "Diubah lagi setelah metode defineData";

        $output['perubahan_terakhir'] = $payload->data;

        return response()->json($output);
    }

    protected function defineData(&$payload)
    {
        $payload->data = ($payload->needValidated)
            ? "Perubahan setelah validasi"
            : $payload->data;
    }

    public function commonJson()
    {
        $additional_props = [
            'key_one' => 'Key One',
            'key_two' => 'Key Two'
        ];

        return $this->addMoreProperties([
            'model' => 'Sekolah',
            'additional_properties' => $additional_props
        ]);
    }

    public function addMoreProperties($var = null)
    {
        $payload = new stdClass();
        $payload->message = "Ini adalah properti default";

        if (gettype($var) === 'array' && array_key_exists('additional_properties', $var)) {
            foreach ($var['additional_properties'] as $additional_prop => $prop_value) {
                $payload->{$additional_prop} = $prop_value;
            }
        }

        return response()->json($payload);
    }

    public function cobaValidasi()
    {
        $data = request()->all();
        $validator = AlamatValidator::validate($data);
        
        if($validator->errors()->isNotEmpty()) {
            $errors = $validator->errors();
            $errors->add('ada', 'mencoba_coba');

            session()->flash('validation_error', $errors);
            return view('experiment.validation')->withErrors(session()->get('validation_error'));
        } else {
            dd("Berhasil hurray!");
        }
    }

    public function halamanValidasi()
    {
        return view('experiment.validation');
    }

    public function buatPembiayaanSekolah(Request $request)
    {
        /**
         * Proses pembuatan pembiayaan sekolah jika memiliki rincian pembiayaan
         * 1. Membuat pembiayaan sekolah terlebih dahulu
         * 2. Menggunakan eloquent relationship createMany() pada model pembiayaan_sekolah -> rincian_pembiayaan
         * 3. Menggunakan event listener, setelah pembiayaan sekolah dibuat, maka pembiayaan para siswa juga dibuat
         */

        /**
         * 1. Frekuensi
         * @param  string $request->frekuensi
         * Parameter ini dicek dengan konstanta \App\Constants\FrekuensiItemPembiayaan
         * lalu disimpan ke database sesuai dengan nilai pada konstanta
         */

        /**
         * 2. Biaya Dasar
         * @param  string $request->biaya_dasar
         * Nilai dari parameter ini harus dikonversi ke double / float
         */

        /**
         * 3. Rincian Pembiayaan
         * @param  boolean $request->rincian_pembiayaan
         * @param  string  $request->item_rincian_pembiayaan (JSON)
         * 
         * Jika parameter rincian_pembiayaan bernilai true, maka decode item_rincian_pembiayaan
         * item_rincian_pembiayaan
         *  item_1
         *  item_2
         *  ...
         */
        
        /**
         * 4. Suffix
         * @param  string $request->suffix
         */
        
        /**
         * 5. Sasaran Peserta Didik
         * @param array $request->target
         * 
         * Jika array kosong, maka sasarannya "SEMUA" peserta didik dalam sekolah
         * Jika sasarannya tertentu, maka isi arraynya adalah ID_KELAS yang dituju
         */
        
        try {
            $pembiayaan = new \SevenArts\Services\Factories\EPayment\PembiayaanSekolahFactory();
            
            return ($request->frek_biaya === FrekuensiItemPembiayaan::BULANAN)
                ? $pembiayaan->buatPembiayaanRutin($request)
                : $pembiayaan->buatPembiayaanTunggal($request);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function bayarPembiayaanSiswa(Request $request)
    {
        $transaksi_repository = new TransaksiRepository();

        $transaksi = $transaksi_repository->buatTransaksi($request);

        event(new TransaksiCreated($transaksi));
    }

    public function lihatPembayaran($id_transaksi)
    {
        $transaksi_repository = new TransaksiRepository();
        return response()->json(
            TransaksiResponse::tampilkan($transaksi_repository->lihatPembayaran($id_transaksi), 1)
        );
    }

    public function updateStatusPembiayaanSiswa($id_transaksi)
    {
        try {
            $sub = DB::table('ypwi_epayment.rincian_transaksi')
                ->select(DB::raw('left(id_item_pembiayaan, 36) as id_item_pembiayaan, bayar'))
                ->where('id_transaksi', $id_transaksi);
    
            $hasil = DB::table(DB::raw("({$sub->toSql()}) as sub"))
                ->mergeBindings($sub)
                ->select(DB::raw('id_item_pembiayaan, SUM(bayar) as total_dibayar'))
                ->groupBy('id_item_pembiayaan')
                ->get();
    
            foreach ($hasil as $informasi_trx) {
                if (PembiayaanSiswa::sudahLunas($informasi_trx)) {
                    PembiayaanSiswaRepository::updateStatus($informasi_trx->id_item_pembiayaan);
                }
            }
    
            return response()->json("Berhasil hurray!");
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function daftarPembiayaanYangTersedia($id_peserta_didik)
    {
        return response()->json(PembiayaanResponse::pembiayaanBelumLunas($id_peserta_didik));
    }
}
