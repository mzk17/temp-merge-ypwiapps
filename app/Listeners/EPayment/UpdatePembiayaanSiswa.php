<?php

namespace App\Listeners\EPayment;

use App\Events\EPayment\SubsidiSiswaCreated;
use Illuminate\Database\Eloquent\Collection;
use SevenArts\Repositories\EPayment\PembiayaanSiswaRepository;

class UpdatePembiayaanSiswa
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(SubsidiSiswaCreated $event)
    {
        $subsidi = $event->subsidi_siswa;
        $pembiayaan_siswa_repository = new PembiayaanSiswaRepository();

        try {
            \Log::info("[LISTENER] UpdatePembiayaanSiswa - Subsidi buat Siswa berhasil dibuat! Saatnya memperbarui pembiayaan siswa yang berhak mendapatkan subsidi");
            \Log::info("[LISTENER] UpdatePembiayaanSiswa - payload event -> " . json_encode($subsidi));
            // "UPDATE pembiayaan_siswa SET id_subsidi = {$id_subsidi} WHERE id AND id_peserta_didik"
            if ($subsidi instanceof Collection || is_array($subsidi)) {
                foreach ($subsidi as $sub) {
                    $where = $this->whereParamPayload($sub);
                    $pembiayaan_siswa_repository->updateSubsidi($where, $sub);
                }
            } else {
                $where = $this->whereParamPayload($subsidi);
                $pembiayaan_siswa_repository->updateSubsidi($where, $subsidi);
            }

            \Log::info("[LISTENER] UpdatePembiayaanSiswa - Berhasil memperbarui, menambahkan subsidi pada pembiayaan siswa");
        } catch (\Exception $e) {
            report($e);
            \Log::error("[LISTENER] UpdatePembiayaanSiswa - Gagal menambahkan subsidi pada siswa. Erorrnya adalah => " . $e->getMessage());
        }
    }

    private function whereParamPayload($subsidi)
    {
        $where = new \stdClass();
        $where->id_pembiayaan_siswa = $subsidi['id_pembiayaan_siswa'];
        $where->id_peserta_didik    = $subsidi['id_peserta_didik'];

        return $where;
    }
}
