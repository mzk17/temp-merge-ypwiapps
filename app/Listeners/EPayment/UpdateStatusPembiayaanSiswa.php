<?php

namespace App\Listeners\EPayment;

use App\Events\EPayment\TransaksiCreated;
use SevenArts\Repositories\EPayment\PembiayaanSiswaRepository;
use SevenArts\Repositories\EPayment\TransaksiRepository;

class UpdateStatusPembiayaanSiswa
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(TransaksiCreated $event)
    {
        $transaksi = $event->transaksi;
        $psr = new PembiayaanSiswaRepository();

        try {
            $hasil = TransaksiRepository::lihatInfoItemPembiayaanSiswa($transaksi->id);
    
            foreach ($hasil as $informasi_trx) {
                if ($psr->sudahLunas($informasi_trx)) {
                    $psr->updateStatus($informasi_trx->id_item_pembiayaan);
                }
            }
    
            \Log::info("[LISTENER] UpdateStatusPembiayaanSiswa - Pembaruan status pembiayaan siswa untuk transaksi '" . $transaksi->nama_transaksi . "' berhasil dilakukan.");
        } catch (\Exception $e) {
            \Log::error("[LISTENER] UpdateStatusPembiayaanSiswa - Gagal melakukan pembaruan status pembiayaan siswa untuk transaksi '" . $transaksi->nama_transaksi . 
                "'. Errornya -> " . json_encode($e->getMessage())
            );
        }
    }
}