<?php

namespace App\Listeners\EPayment;

use App\Events\EPayment\PembiayaanSekolahCreated;
use Illuminate\Database\Eloquent\Collection;
use SevenArts\Models\EPayment\PembiayaanSekolah;
use SevenArts\Models\EPayment\PembiayaanSiswa;
use SevenArts\Services\Factories\EPayment\PembiayaanSiswaFactory;

class CreatePembiayaanSiswa
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(PembiayaanSekolahCreated $event)
    {
        $pembiayaan_sekolah = $event->pembiayaan_sekolah;

        $factory = new PembiayaanSiswaFactory($pembiayaan_sekolah);
    
        try {
            \Log::info("Pembiayaan Sekolah berhasil dibuat! " . json_encode($pembiayaan_sekolah));
            \Log::info("Saatnya membuat pembiayaan siswa");
    
            if ($pembiayaan_sekolah instanceof Collection) {
                $factory->buatPembiayaanRutin();
                $ps = $pembiayaan_sekolah->first();
                $nama_pembiayaan = $ps->itemPembiayaan->nama_pembiayaan;
            } else {
                $factory->buatPembiayaanTunggal();
                $nama_pembiayaan = $pembiayaan_sekolah->nama_pembiayaan;
            }
    
            \Log::info("[BERHASIL] Pembuatan pembiayaan '". $nama_pembiayaan ."' untuk siswa telah selesai.");
        } catch (\Exception $e) {
            report($e);
            if ($pembiayaan_sekolah instanceof Collection) {
                $ps = $pembiayaan_sekolah->first();
                $nama_pembiayaan = $ps->itemPembiayaan->nama_pembiayaan;
            } elseif ($pembiayaan_sekolah instanceof PembiayaanSekolah) {
                $nama_pembiayaan = $pembiayaan_sekolah->nama_pembiayaan;
            }
    
            \Log::error("Gagal membuat pembiayaan '". $nama_pembiayaan ."' untuk siswa. ERR => " . $e->getMessage());
        }
    }
}
