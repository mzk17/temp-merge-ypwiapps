<?php

namespace App\Listeners\Prime;

use App\Events\Prime\TenagaPendidikDeleted;
use App\Repositories\Prime\AlamatRepository;
use App\Repositories\Prime\DataPribadiRepository;

class RemoveDataPribadiTenagaPendidik
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(TenagaPendidikDeleted $event)
    {
        $tendik_data_pribadi = $event->tenaga_pendidik->dataPribadi;
        \Log::info("[EVENT:App\Listeners\PrimeRemoveDataPribadiTenagaPendidik] Sedang menghapus data pribadi dan alamat tenaga pendidik " . $tendik_data_pribadi->nama);
        
        try {
            $data_pribadi = new DataPribadiRepository();
            $data_pribadi->destroy($tendik_data_pribadi->id);

            $alamat = new AlamatRepository();
            $alamat->destroy($tendik_data_pribadi->alamatUtama->id);
            \Log::info("[EVENT:App\Listeners\PrimeRemoveDataPribadiTenagaPendidik] Data pribadi dan alamat tenaga pendidik " . $tendik_data_pribadi->nama . " berhasil dihapus");
            
        } catch (\Exception $e) {
            report($e);
            \Log::error("[EVENT:App\Listeners\PrimeRemoveDataPribadiTenagaPendidik] Gagal menghapus data pribadi dan alamat tenaga pendidik. " . $e->getMessage());
        }
    }
}
