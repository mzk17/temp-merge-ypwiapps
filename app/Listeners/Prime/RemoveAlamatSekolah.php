<?php

namespace App\Listeners\Prime;

use App\Events\Prime\SekolahDeleted;
use App\Repositories\Prime\AlamatRepository;

class RemoveAlamatSekolah
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(SekolahDeleted $event)
    {
        try {
            $alamat = new AlamatRepository();
            $alamat->destroy($event->sekolah->id_alamat);
            \Log::info("Alamat lembaga " . $event->sekolah->nama_sekolah . " berhasil dihapus");
        } catch (\Event $e) {
            report($e);
            \Log::error("Gagal menghapus alamat sekolah. " . $e->getMessage());
        }
    }
}
