<?php

namespace App\Listeners\Prime;

use App\Events\Prime\PesertaDidikDeleted;
use App\Repositories\Prime\AlamatRepository;
use App\Repositories\Prime\DataPribadiRepository;

class RemoveDataPribadiPesertaDidik
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(PesertaDidikDeleted $event)
    {
        $pd_data_pribadi = $event->peserta_didik->dataPribadi;
        \Log::info("[EVENT:App\Listeners\PrimeRemoveDataPribadiPesertaDidik] Sedang menghapus data pribadi dan alamat peserta didik " . $pd_data_pribadi->nama);
        
        try {
            $data_pribadi = new DataPribadiRepository();
            $data_pribadi->destroy($pd_data_pribadi->id);

            $alamat = new AlamatRepository();
            $alamat->destroy($pd_data_pribadi->alamatUtama->id);
            \Log::info("[EVENT:App\Listeners\PrimeRemoveDataPribadiPesertaDidik] Data pribadi dan alamat peserta didik " . $pd_data_pribadi->nama . " berhasil dihapus");
            
        } catch (\Exception $e) {
            report($e);
            \Log::error("[EVENT:App\Listeners\PrimeRemoveDataPribadiPesertaDidik] Gagal menghapus data pribadi dan alamat peserta didik. " . $e->getMessage());
        }
    }
}
