<?php

namespace App\Listeners\Prime;

use App\Events\Prime\LembagaDeleted;
use App\Repositories\Prime\AlamatRepository;

class RemoveAlamatLembaga
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(LembagaDeleted $event)
    {
        try {
            $alamat = new AlamatRepository();
            $alamat->destroy($event->lembaga->id_alamat);
            \Log::info("Alamat lembaga " . $event->lembaga->nama_yayasan . " berhasil dihapus");
        } catch (\Exception $e) {
            report($e);
            \Log::error("Gagal menghapus alamat lembaga. " . $e->getMessage());
        }
    }
}
