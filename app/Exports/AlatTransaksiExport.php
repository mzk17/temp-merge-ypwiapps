<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use SevenArts\Models\AssetInventory\AlatTransaksi;

class AlatTransaksiExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return AlatTransaksi::all();
    }
    public function map($alat_trans): array
    {
        return [
            $alat_trans->nama_sekolah,
            $alat_trans->nama_barang,
            $alat_trans->kat
        ];
    }

    public function headings(): array
    {
        return [
        ];
    }
}

