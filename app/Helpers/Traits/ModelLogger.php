<?php

namespace App\Helpers\Traits;

use Illuminate\Support\Facades\Log;

trait ModelLogger
{
    public static function cudLogger()
    {
        // Create
        self::creating(function ($model) {
            Log::info("[MODEL_BOOT]|Proses penyimpanan data " . static::ENTITY_NAME . " baru...");
        });
    
        self::created(function ($model) {
            Log::info("[MODEL_BOOT]|Penyimpanan data " . static::ENTITY_NAME . " berhasil.");
            Log::info("[MODEL_BOOT]|Data baru: " . $model);
        });
    
        // Update
        self::updating(function ($model) {
            Log::info("[MODEL_BOOT]|Proses pembaruan data " . static::ENTITY_NAME . "...");
            Log::info("[MODEL_BOOT]|Data sebelumnya: " . $model); // perlu ditinjau ulang untuk mengambil data sebelumnya
        });
    
        self::updated(function ($model) {
            Log::info("[MODEL_BOOT]|Pembaruan data " . static::ENTITY_NAME . " berhasil.");
            Log::info("[MODEL_BOOT]|Data yang baru: " . $model);
        });
    
        // Delete
        self::deleting(function ($model) {
            Log::info("[MODEL_BOOT]|Proses penghapusan data " . static::ENTITY_NAME . "...");
        });
    
        self::deleted(function ($model) {
            Log::info("[MODEL_BOOT]|Data " . static::ENTITY_NAME . " berhasil dihapus.");
            Log::info("[MODEL_BOOT]|Data yang dihapus: " . $model);
        });
    }
}
