<?php

namespace App\Helpers\ZeroTraits;

trait ResourcesAction
{
    /**
     * Fungsi menampilkan sebuah data dari model
     *
     * @param  any  $id
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function actionShow($id)
    {
        return $this->model::find($id);
    }


    /**
     * Fungsi membuat data baru pada model
     *
     * @param \Illuminate\Database\Eloquent\Model  $model
     * @param  Array $data
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function actionStore($data)
    {
        return $this->create($this->model, $data);
    }


    /**
     * Fungsi memperbarui sebuah data pada model
     *
     * @param \Illuminate\Database\Eloquent\Model  $model
     * @param  Array $data
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function actionUpdate($data)
    {
        return $this->change($this->model, $data);
    }

    public function actionDestroy($data)
    {
        # code...
    }
}
