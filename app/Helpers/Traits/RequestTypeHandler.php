<?php

namespace App\Helpers\ZeroTraits;

trait RequestTypeHandler
{
    private function checkAdditionalParams(array $additionalParams)
    {
        $paramList = [
            'needValidated', 'afterStore'
        ];

        // Init. default values
        $params = new \stdClass();
        $params->needValidated = false;
        $params->afterStore    = false;

        for ($i = 0; $i < count($additionalParams); $i++) {
            $params->{$paramList[$i]} = $additionalParams[$i];
        }
        
        return $params;
    }

    /**
     * Handler untuk tipe HTTP request
     *
     * @param  string  $actionName
     * @param  mixed   $data
     * @param  array   $additionalParams
     * @return \Illuminate\Http\Response
     */
    protected function httpHandler($actionName, $data, array $additionalParams = [])
    {
        $params = $this->checkAdditionalParams($additionalParams);
        
        if ($params->needValidated && $data->fails()) {
            return redirect()
                ->back()
                ->with('pesan', $data->errors()->toArray())
                ->withErrors($data)
                ->withInput();
        }

        $payload = ($params->needValidated) ? $data->valid() : $data;

        $model = $this->{"action" . $actionName}($payload);

        if (! $model) {
            return redirect()
                ->back()
                ->withErrors(['gagal' => $this->{$actionName . "Message"}('gagal')])
                ->withInput();
        }

        // Run afterStore if specified
        if ($params->afterStore) {
            $this->afterStore($model);
        }

        return redirect()
            ->back()
            ->with('berhasil', $this->{$actionName . "Message"}('berhasil'));
    }

    /**
     * Handler untuk AJAX request.
     *
     * @param  String  $actionName
     * @param  mixed   $data
     * @param  array   $additionalParams
     */
    protected function ajaxHandler(String $actionName, $data, array $additionalParams = [])
    {
        $params = $this->checkAdditionalParams($additionalParams);

        if ($params->needValidated && $data->fails()) {
            $data->validate();
        }

        $payload = ($params->needValidated) ? $data->valid() : $data;

        $model = $this->{"action" . $actionName}($payload);

        if (! $model) {
            $this->payload_response->message = $this->{$actionName . "Message"}('gagal');
            return $this->commonJson(($params->needValidated) ? 500 : 404);
        }

        // Run afterStore if specified
        if ($params->afterStore) {
            $this->afterStore($model);
        }

        $this->payload_response->message = $this->{$actionName . "Message"}('berhasil');
        $this->payload_response->models = $model;
        return $this->commonJson();
    }
}
