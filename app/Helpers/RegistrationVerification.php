<?php

namespace App\Helpers;

use Mail;
use Carbon\Carbon;
use App\Mail\VerifyUserRegistered;
use SevenArts\Models\Prime\VerifikasiAkunPengguna;
use Illuminate\Contracts\Auth\MustVerifyEmail;

trait RegistrationVerification
{
    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        $user = $this->userDefine();
        $time = Carbon::now();
        $tokenExpiration = $time->addDay();

        // Create verification record for registered user\
        VerifikasiAkunPengguna::create([
            'id_akun_pengguna' => $user->getAuthIdentifier(),
            'token' => sha1(time()),
            'waktu_kadaluarsa_token' => $tokenExpiration,
            'status_token' => 0
        ]);

        // Send verification email
        Mail::to($user->email)
            ->send(new VerifyUserRegistered($user));
    }

    /**
     * Check if user already complete registration
     * by fill personal information.
     *
     * @return bool
     */
    public function hasCompletedRegistration()
    {
        return ! is_null($this->dataDiri);
    }

    public function userDefine()
    {
        return ($this instanceof MustVerifyEmail)
            ? $this
            : \Auth::user();
    }
}
