<?php

namespace App\Helpers\Excel\Importers;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use SevenArts\Models\Prime\TenagaPendidik;
use SevenArts\Models\Prime\DataPribadi;
use SevenArts\Models\Prime\Alamat;

class TenagaPendidikImport implements ToCollection, WithStartRow
{
    protected $guru;

    public function __construct($tipe_tendik)
    {
        $this->guru = $tipe_tendik === 'guru' ? true : false;
    }

    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            // Buat data pribadi dan alamat terlebih dahulu
            $id_data_pribadi = $this->personalDataCreator($row);

            TenagaPendidik::create([
                'id' => \Str::uuid(),
                'nuptk' => $row[2],
                'nip' => $row[6],
                'status_kepegawaian' => 1, // Harus disesuaikan dengan constant file $row[7],
                'jenis_ptk' => 1, // Harus disesuaikan dengan constant file $row[8],
                'tugas_tambahan' => 1,// Harus disesuaikan dengan constant file $row[20],
                'sk_cpns' => $row[21],
                'tanggal_cpns' => $row[22],
                'sk_pengangkatan' => $row[23],
                'tmt_pengangkatan' => $row[24],
                'lembaga_pengangkatan' => 1, // Harus disesuaikan dengan constant file $row[25],
                'pangkat_golongan' => 1, // Harus disesuaikan dengan constant file $row[26],
                'sumber_gaji' => 1, // Harus disesuaikan dengan constant file $row[27],
                'nama_ibu_kandung' => $row[28],
                'status_perkawinan' => 1, // Harus disesuaikan dengan constant file $row[29],
                'nama_suami_istri' => $row[30],
                'nip_suami_istri' => $row[31],
                'pekerjaan_suami_istri' => $row[32], // Harus disesuaikan dengan constant file
                'tmt_pns' => $row[33],
                'sudah_lisensi_kepala_sekolah' => $this->emptyCheck($row[34]),
                'pernah_diklat_kepengawasan' => $this->emptyCheck($row[35]),
                'keahlian_braille' => $this->emptyCheck($row[36]),
                'keahlian_bahasa_isyarat' => $this->emptyCheck($row[37]),
                'npwp' => $row[38],
                'nama_wajib_pajak' => $row[39],
                'kewarganegaraan' => $row[40],
                'bank' => $row[41],
                'nomor_rekening_bank' => $row[42],
                'rekening_atas_nama' => $row[43],
                'karpeg' => $row[46],
                'karis_karsu' => $row[47],
                'nuks' => $row[50],
                'sebagai_guru' => $this->guru,
                'id_data_pribadi' => $id_data_pribadi,
            ]);
        }
    }

    // Index started from 1
    public function startRow(): int
    {
        return 6;
    }

    protected function emptyCheck($data)
    {
        return !is_null($data) && $data !== '' ? 1 : 0;
    }

    private function personalDataCreator($row)
    {
        $id_data_pribadi = \Str::uuid();
        $alamat_utama = $this->addressCreator($row);

        DataPribadi::create([
            'id' => $id_data_pribadi,
            'nama' => $row[1],
            'jenis_kelamin' => $row[3],
            'tempat_lahir' => $row[4],
            'tanggal_lahir' => $row[5],
            'agama' => 1, // Harus disesuaikan dengan constant file $row[9]
            'alamat_1' => $alamat_utama,
            'telepon' => $row[17],
            'hp' => $row[18],
            'email' => $row[19],
            'nik' => $row[44],
            'no_kk' => $row[45],
        ]);

        return $id_data_pribadi;
    }

    private function addressCreator($row)
    {
        $alamat_id = \Str::uuid();
        
        Alamat::create([
            'id' => $alamat_id,
            'alamat' => $row[10],
            'rt' => $row[11],
            'rw' => $row[12],
            'dusun' => $row[13],
            'kelurahan' => $row[14],
            'kecamatan' => $row[15],
            'kode_pos' => $row[16],
            'lintang' => $row[48],
            'bujur' => $row[49],
        ]);

        return $alamat_id;
    }
}
