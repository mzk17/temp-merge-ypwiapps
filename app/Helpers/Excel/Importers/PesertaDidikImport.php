<?php

namespace App\Helpers\Excel\Importers;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use App\Constants\PesertaDidik as PESERTA_DIDIK;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use SevenArts\Models\Prime\PesertaDidik;
use SevenArts\Models\Prime\DataPribadi;
use SevenArts\Models\Prime\Alamat;

class PesertaDidikImport implements ToCollection, WithStartRow
{
    public function __construct($model = '')
    {
    }

    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            // Buat data pribadi dan alamat terlebih dahulu
            $id_data_pribadi = $this->personalDataCreator($row);

            PesertaDidik::create([
                'id' => \Str::uuid(),
                'nipd' => $row[2],
                'nisn' => $row[4],
                'alat_transportasi' => random_int(1, 7), // Harus disesuaikan dengan constant file $row[17]
                'skhun' => $row[21],
                'penerima_kps' => $row[22] === 'Ya' ? 1 : 0,
                'no_kps' => $row[23],
                'rombel_saat_ini' => $row[42], // Disesuaikan dengan tabel kelas nantinya
                'no_peserta_ujian_nasional' => $row[43],
                'no_seri_ijazah' => $row[44],
                'penerima_kip' => $row[45] === 'Ya' ? 1 : 0,
                'nomor_kip' => $row[46],
                'nama_di_kip' => $row[47],
                'nomor_kks' => $row[48],
                'no_registrasi_akta_lahir' => $row[49],
                'bank' => $row[50],
                'nomor_rekening_bank' => $row[51],
                'rekening_atas_nama' => $row[52],
                'layak_pip_usulan_dari_sekolah' => $row[53] === 'Ya' ? 1 : 0,
                'alasan_layak_pip' => $row[54],
                'kebutuhan_khusus' => $row[55] === 'Tidak ada' ? 0 : 1,
                'sekolah_asal' => $row[56],
                'jarak_rumah_ke_sekolah_km' => $row[65],
                'jenis_tinggal' => 1, // Harus disesuaikan dengan constant file $row[16],
                'nama_ayah' => $row[24],
                'nik_ayah' => $row[29],
                'tahun_lahir_ayah' => $row[25],
                'jenjang_pendidikan_ayah' => $row[26],
                'pekerjaan_ayah' => $row[27],
                'penghasilan_ayah' => ($row[28]) ? PESERTA_DIDIK::PENGASHILAN_ORTU_WALI[$row[28]] : $row[28],
                'nama_ibu' => $row[30],
                'nik_ibu' => $row[35],
                'tahun_lahir_ibu' => $row[31],
                'jenjang_pendidikan_ibu' => $row[32],
                'pekerjaan_ibu' => $row[33],
                'penghasilan_ibu' => ($row[34]) ? PESERTA_DIDIK::PENGASHILAN_ORTU_WALI[$row[34]] : $row[34],
                'nama_wali' => $row[36],
                'nik_wali' => $row[41],
                'tahun_lahir_wali' => $row[37],
                'jenjang_pendidikan_wali' => $row[38],
                'pekerjaan_wali' => $row[39],
                'penghasilan_wali' => ($row[40]) ? PESERTA_DIDIK::PENGASHILAN_ORTU_WALI[$row[40]] : $row[40],
                'anak_keberapa' => $row[57],
                'berat_badan' => $row[61],
                'tinggi_badan' => $row[62],
                'lingkar_kepala' => $row[63],
                'jml_saudara_kandung' => $row[64],
                'id_data_pribadi' => $id_data_pribadi,
            ]);
        }
    }

    // Index started from 1
    public function startRow(): int
    {
        return 7;
    }

    protected function emptyCheck($data)
    {
        return !is_null($data) && $data !== '' ? 1 : 0;
    }

    private function personalDataCreator($row)
    {
        $id_data_pribadi = \Str::uuid();
        $alamat_utama = $this->addressCreator($row);

        DataPribadi::create([
            'id' => $id_data_pribadi,
            'nama' => $row[1],
            'jenis_kelamin' => $row[3],
            'tempat_lahir' => $row[5],
            'tanggal_lahir' => $row[6],
            'agama' => 1, // Harus disesuaikan dengan constant file $row[9]
            'alamat_1' => $alamat_utama,
            'telepon' => $row[18],
            'hp' => $row[19],
            'email' => $row[20],
            'nik' => $row[7],
            'no_kk' => $row[60],
        ]);

        return $id_data_pribadi;
    }

    private function addressCreator($row)
    {
        $alamat_id = \Str::uuid();
        
        Alamat::create([
            'id' => $alamat_id,
            'alamat' => $row[9],
            'rt' => $row[10],
            'rw' => $row[11],
            'dusun' => $row[12],
            'kelurahan' => $row[13],
            'kecamatan' => $row[14],
            'kode_pos' => $row[15],
            'lintang' => $row[58],
            'bujur' => $row[59],
        ]);

        return $alamat_id;
    }
}
