<?php

use SevenArts\Models\Prime\Sekolah;
use SevenArts\Models\AssetInventory\{ AlatTransaksi, BangunanTransaksi, TanahTransaksi };
use Carbon\Carbon;

if (! function_exists('formatRupiah')) {
    function formatRupiah($angka)
    {
        return "Rp. ".number_format($angka,0,',','.');
    }
}

if (! function_exists('formatVolume')) {
    function formatVolume($luas)
    {
        return number_format($luas,0,',','.')." m2";
    }
}

if (! function_exists('formatJumlahBarang')) {
    function formatJumlahBarang($buah)
    {
        return number_format($buah,0,',','.')." pcs";
    }
}

if (! function_exists('totalAsetAlat')) {
    function totalAsetAlat()
    {
        return AlatTransaksi::count();   
    }
}

if (! function_exists('totalAsetBangunan')) {
    function totalAsetBangunan()
    {
        return BangunanTransaksi::count();   
    }
}
    
if (! function_exists('totalAsetTanah')) {
    function totalAsetTanah()
    {
        return TanahTransaksi::count();   
    }
}
    
if (! function_exists('totalSekolah')) {
    function totalSekolah()
    {
        return Sekolah::count();   
    }
}

if (! function_exists('manualTimestamps')) {
    function manualTimestamps(array &$payload)
    {
        $payload['created_at'] = Carbon::now();
        $payload['updated_at'] = Carbon::now();
    }
}
?>

