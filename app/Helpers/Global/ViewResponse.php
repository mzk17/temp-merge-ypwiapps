<?php

use Illuminate\Contracts\View\Factory as ViewFactory;

if (! function_exists('buildAppView')) {
    /**
     *
     * @param  string|null  $view
     * @param  \Illuminate\Contracts\Support\Arrayable|array  $data
     * @param  array  $mergeData
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    function buildAppView($appFolder = null, $view = null, $data = [], $mergeData = [])
    {
        $factory = app(ViewFactory::class);

        if (func_num_args() === 0) {
            return $factory;
        }

        $viewPath = $appFolder . "." . $view;

        return $factory->make($viewPath, $data, $mergeData);
    }
}

if (! function_exists('primeView')) {
    /**
     * Get the evaluated view contents from proper folder for the Asset & Inventory views.
     */
    function primeView($view = null, $data = [], $mergeData = [])
    {
        return buildAppView('pages.prime.', $view, $data, $mergeData);
    }
}

if (! function_exists('assetInventoryView')) {
    /**
     * Get the evaluated view contents from proper folder for the Asset & Inventory views.
     */
    function assetInventoryView($view = null, $data = [], $mergeData = [])
    {
        return buildAppView('asset-inventory', $view, $data, $mergeData);
    }
}