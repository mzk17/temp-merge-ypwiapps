<?php

namespace App\Constants;

class JenisAkun
{
  const MASYARAKAT = 1;
  const PEGAWAI_YAYASAN = 2;
  const PEGAWAI_SEKOLAH = 3;
  const SISWA = 4;
  const ORANG_TUA_SISWA = 5;
}