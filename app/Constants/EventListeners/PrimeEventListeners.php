<?php

namespace App\Constants\EventListeners;

class PrimeEventListeners
{
    const HAPUS_ALAMAT = [
        'App\Events\Prime\SekolahDeleted' => [
            'App\Listeners\Prime\RemoveAlamatSekolah',
        ],
        'App\Events\Prime\LembagaDeleted' => [
            'App\Listeners\Prime\RemoveAlamatLembaga',
        ],
        'App\Events\Prime\PesertaDidikDeleted' => [
            'App\Listeners\Prime\RemoveDataPribadiPesertaDidik',
        ],
        'App\Events\Prime\TenagaPendidikDeleted' => [
            'App\Listeners\Prime\RemoveDataPribadiTenagaPendidik',
        ],
    ];
}