<?php

namespace App\Constants\EventListeners;

class EPaymentEventListeners
{
    const PEMBIAYAAN_SISWA = [
        'App\Events\EPayment\PembiayaanSekolahCreated' => [
            'App\Listeners\EPayment\CreatePembiayaanSiswa',
        ],
        'App\Events\EPayment\SubsidiSiswaCreated' => [
            'App\Listeners\EPayment\UpdatePembiayaanSiswa',
        ],
        'App\Events\EPayment\TransaksiCreated' => [
            'App\Listeners\EPayment\UpdateStatusPembiayaanSiswa'
        ]
    ];
}