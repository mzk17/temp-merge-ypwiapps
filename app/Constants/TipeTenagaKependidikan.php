<?php

namespace App\Constants;

class TipeTenagaKependidikan
{
    const GURU = 'guru';
    const TENDIK = 'tenaga_kependidikan';
}