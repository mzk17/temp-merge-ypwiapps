<?php

namespace App\Constants;

class FrekuensiItemPembiayaan
{
    const BULANAN        = "BULANAN";
    const SEKALI_SETAHUN = "SEKALI_SETAHUN";
    const SATU_KALI      = "SATU_KALI";
    const MANUAL         = "MANUAL";
}