<?php

namespace App\Constants;

class Agama
{
    const ISLAM = 1;
    const KATOLIK = 2;
    const PROTESTAN = 3;
    const HINDU = 4;
    const BUDDHA = 5;
}