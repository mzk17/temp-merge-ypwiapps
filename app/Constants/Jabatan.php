<?php

namespace App\Constants;

class Jabatan
{
    // Yayasan
    const KETUA = 1;
    const BENDAHARA = 2;
    const SEKRETARIS = 3;
    const ADMIN = 4;
    const STAFF = 5;

    // Sekolah
    const KEPALA_SEKOLAH = 6;
    const WAKIL_KEPALA_SEKOLAH = 7;
    const BENDAHARA_SEKOLAH = 8;
    const GURU = 9;
    const ADMIN_SEKOLAH = 10;
    const PUSTAKAWAN = 11;
    const LABORAN = 12;
    const STAFF_SEKOLAH = 13;
}