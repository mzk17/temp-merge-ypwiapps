<?php

namespace App\Constants;

class AttributSekolah
{
    const TINGKATAN_SEKOLAH = [
        'Pendidikan Anak Usia Dini (PAUD)' => 1,
        'Sekolah Dasar (SD)' => 2,
        'Sekolah Menengah Pertama (SMP)' => 3,
        'Sekolah Menengah Atas (SMA)' => 4,
        'Perguruan Tinggi / Universitas' => 5
    ];
}

