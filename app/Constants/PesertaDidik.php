<?php

namespace App\Constants;

class PesertaDidik
{
    const PENGASHILAN_ORTU_WALI = [
        'Tidak Berpenghasilan' => 1,
        'Kurang dari Rp. 500,000' => 2,
        'Rp. 500,000 - Rp. 999,999' => 3,
        'Rp. 1,000,000 - Rp. 1,999,999' => 4,
        'Rp. 2,000,000 - Rp. 4,999,999' => 5,
        'Rp. 5,000,000 - Rp. 20,000,000' => 6,
        'Lebih dari Rp 2.000.000' => 7,
    ];
}