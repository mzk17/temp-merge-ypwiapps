<?php

namespace App\Events\EPayment;

use Illuminate\Queue\SerializesModels;
use SevenArts\Models\EPayment\PembiayaanSekolah;

class PembiayaanSekolahCreated
{
    use SerializesModels;

    public $pembiayaan_sekolah;

    public $collection;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($pembiayaan_sekolah, $collection = false)
    {
        $this->pembiayaan_sekolah = $pembiayaan_sekolah;
        $this->collection         = $collection;
        \Log::info("Pembiayaan Sekolah created event " . json_encode($pembiayaan_sekolah));
    }
}
