<?php

namespace App\Events\EPayment;

use Illuminate\Queue\SerializesModels;
use SevenArts\Models\EPayment\Transaksi;

class TransaksiCreated
{
    use SerializesModels;

    public $transaksi;

    public $collection;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Transaksi $transaksi)
    {
        $this->transaksi = $transaksi;
        \Log::info("[EVENT] Transaksi telah dibuat -> " . json_encode($transaksi));
    }
}
