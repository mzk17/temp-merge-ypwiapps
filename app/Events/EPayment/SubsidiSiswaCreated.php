<?php

namespace App\Events\EPayment;

use Illuminate\Queue\SerializesModels;

class SubsidiSiswaCreated
{
    use SerializesModels;

    public $subsidi_siswa;

    public $collection;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($subsidi_siswa)
    {
        $this->subsidi_siswa = $subsidi_siswa;
        \Log::info("[EVENT] Subsidi buat Siswa telah dibuat -> " . json_encode($subsidi_siswa));
    }
}
