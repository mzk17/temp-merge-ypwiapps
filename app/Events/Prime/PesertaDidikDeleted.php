<?php

namespace App\Events\Prime;

use Illuminate\Queue\SerializesModels;
use SevenArts\Models\Prime\PesertaDidik;

class PesertaDidikDeleted
{
    use SerializesModels;

    public $peserta_didik;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(PesertaDidik $peserta_didik)
    {
        $this->peserta_didik = $peserta_didik;
        \Log::info("PesertaDidik deleted event " . json_encode($peserta_didik));
    }
}
