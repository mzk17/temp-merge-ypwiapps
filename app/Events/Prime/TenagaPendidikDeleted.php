<?php

namespace App\Events\Prime;

use Illuminate\Queue\SerializesModels;
use SevenArts\Models\Prime\TenagaPendidik;

class TenagaPendidikDeleted
{
    use SerializesModels;

    public $tenaga_pendidik;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(TenagaPendidik $tenaga_pendidik)
    {
        $this->tenaga_pendidik = $tenaga_pendidik;
        \Log::info("TenagaPendidik deleted event " . json_encode($tenaga_pendidik));
    }
}
