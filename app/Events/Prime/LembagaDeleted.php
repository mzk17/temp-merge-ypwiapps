<?php

namespace App\Events\Prime;

use Illuminate\Queue\SerializesModels;
use SevenArts\Models\Prime\Lembaga;

class LembagaDeleted
{
    use SerializesModels;

    public $lembaga;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Lembaga $lembaga)
    {
        $this->lembaga = $lembaga;
        \Log::info("Lembaga deleted event " . json_encode($lembaga));
    }
}
