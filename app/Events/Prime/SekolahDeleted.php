<?php

namespace App\Events\Prime;

use Illuminate\Queue\SerializesModels;
use SevenArts\Models\Prime\Sekolah;

class SekolahDeleted
{
    use SerializesModels;

    public $sekolah;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Sekolah $sekolah)
    {
        $this->sekolah = $sekolah;
        \Log::info("Sekolah deleted event " . json_encode($sekolah));
    }
}
