<?php

namespace App;

use App\Constants\Jabatan;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Helpers\RegistrationVerification;
use SevenArts\Models\Prime\JabatanAkun;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable, RegistrationVerification;

    protected $connection = 'ypwi_prime';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'akun_pengguna';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    protected $keyType = 'string';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'id_jenis_akun', 'id_data_pribadi', 'id_data_jenis_akun', 'username', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'email_verified_at' => 'datetime',
    ];

    public function dataDiri()
    {
        return $this->belongsTo('SevenArts\Models\Prime\DataPribadi', 'id_data_pribadi');
    }

    /**
     * "Jenis Akun" pengguna
     */
    public function jenisAkun()
    {
        return $this->belongsTo('SevenArts\Models\Prime\JenisAkun', 'id_jenis_akun');
    }

    /**
     * Detail data peserta didik sesuai format DAPODIK
     */
    public function dataPesertaDidik()
    {
        return $this->belongsTo('SevenArts\Models\Prime\PesertaDidik', 'id_data_jenis_akun');
    }

    /**
     * Detail data tenaga pendidik sesuai format DAPODIK
     * @flag sebagai_guru untuk menentukan seseorang termasuk guru atau bukan
     */
    public function dataTenagaPendidik()
    {
        return $this->belongsTo('SevenArts\Models\Prime\TenagaPendidik', 'id_data_jenis_akun');
    }

    /**
     * Detail data pegawai yayasan
     */
    public function dataPegawaiYayasan()
    {
        return $this->belongsTo('SevenArts\Models\Prime\PegawaiYayasan', 'id_data_jenis_akun');
    }

    /**
     * "Jabatan" pengguna
     */
    // public function jabatan()
    // {
    //     return $this->belongsToMany('SevenArts\Models\Prime\Jabatan', 'jabatan_akun', 'id_akun_pengguna', 'id_jabatan');
    // }

    public function getJabatanAttribute()
    {
        $jabatan_akun = JabatanAkun::where('id_akun_pengguna', $this->id)->first();
        return $jabatan_akun->id_jabatan;
    }

    public function tokenVerifikasi()
    {
        return $this->hasMany('SevenArts\Models\Prime\VerifikasiAkunPengguna', 'id_akun_pengguna');
    }

    public function tokenAktif()
    {
        $verif = $this->tokenVerifikasi()
                      ->where('status_token', 0)
                      ->first();
        return $verif->token;
    }
}
