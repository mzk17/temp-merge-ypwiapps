<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use SevenArts\Models\AssetInventory\AlatTransaksi;

class Sekolah extends Model
{
    //protected table
    protected $table = 'sekolah';
    protected $fillable = ['id','nama_sekolah','alamat','telepon','email'];

     //  DEFINISI RELATIONSHIP:  
     public function alat_trans()
     {
         return $this->hasMany(AlatTransaksi::class);
     }
}

