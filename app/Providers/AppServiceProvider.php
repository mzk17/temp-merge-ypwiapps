<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use SevenArts\Contracts\BasicResourceMethods;
use SevenArts\Support\Handlers\BasicCRUD\AjaxHandler;
use SevenArts\Support\Handlers\BasicCRUD\HttpHandler;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->miscellaneous();
        // Request methods handler
        $this->requestMethodHandler();
    }

    protected function miscellaneous()
    {
        // Migration recursively
        $this->migrationRecursiveFolder();
        // Log raw SQL queries for development
        $this->logRawQuery();
    }

    protected function requestMethodHandler()
    {
        $this->app->singleton(BasicResourceMethods::class, function ($brm) {
            if (request()->ajax()) {
                return new AjaxHandler();
            }

            return new HttpHandler();
        });
    }

    protected function logRawQuery()
    {
        if (config('app.env') !== 'production') {
            DB::listen(function ($query) {
                Log::info($query->sql);
            });
        }
    }

    protected function migrationRecursiveFolder()
    {
        $mainPath = database_path('migrations');
        $directories = glob($mainPath . '/*', GLOB_ONLYDIR);
        $paths = array_merge([$mainPath], $directories);

        $this->loadMigrationsFrom($paths);
    }
}
