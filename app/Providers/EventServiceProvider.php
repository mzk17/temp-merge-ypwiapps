<?php

namespace App\Providers;

use App\Constants\EventListeners\EPaymentEventListeners;
use App\Constants\EventListeners\PrimeEventListeners;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     * This function taken from EventServiceProvider
     * instead using $listen array in order to use
     * constant classes that provide the contents
     *
     * @var array
     */
    public function listens()
    {
        return array_merge(
            PrimeEventListeners::HAPUS_ALAMAT,
            EPaymentEventListeners::PEMBIAYAAN_SISWA
        );
    }

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
