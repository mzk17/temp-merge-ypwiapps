<?php

namespace App\Repositories\Prime;

use App\Repositories\BasicResourcesRepository;
use SevenArts\Models\Prime\DataPribadi;

class DataPribadiRepository extends BasicResourcesRepository
{
    public function __construct()
    {
        $this->model = DataPribadi::class;
    }

    private static function model()
    {
        return DataPribadi::class;
    }

    public static function dashboard()
    {
        $daftar_data_pribadi = self::model()::all();

        return [$daftar_data_pribadi];
    }

    public static function profile($user_id)
    {
        // code
    }
}
