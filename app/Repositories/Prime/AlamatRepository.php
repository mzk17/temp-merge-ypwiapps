<?php

namespace App\Repositories\Prime;

use App\Repositories\BasicResourcesRepository;
use SevenArts\Models\Prime\AdministrasiWilayah;
use SevenArts\Models\Prime\Alamat;

class AlamatRepository extends BasicResourcesRepository
{
    public function __construct()
    {
        $this->model = Alamat::class;
    }

    private static function model()
    {
        return Alamat::class;
    }
    
    public static function daftarProvinsi()
    {
        return AdministrasiWilayah::provinceList()->get();
    }
}
