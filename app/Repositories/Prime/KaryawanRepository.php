<?php

namespace App\Repositories\Prime;

use App\Repositories\BasicResourcesRepository;
use SevenArts\Models\Prime\Karyawan;

class KaryawanRepository extends BasicResourcesRepository
{
    public function __construct()
    {
        $this->model = Karyawan::class;
    }

    private static function model()
    {
        return Karyawan::class;
    }
    
    public static function dashboard()
    {
        $karyawan = self::semuaKaryawan();
        $adm_reg  = AlamatRepository::daftarProvinsi();

        return [$karyawan, $adm_reg];
    }

    public static function edit($id)
    {
        return [
            self::model()::find($id),
            $adm_reg  = AlamatRepository::daftarProvinsi()
        ];
    }

    public static function semuaKaryawan()
    {
        return self::model()::all();
    }
}
