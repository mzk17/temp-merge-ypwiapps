<?php

namespace App\Repositories\Prime;

use App\Repositories\BasicResourcesRepository;
use SevenArts\Models\Prime\PesertaDidik;

class PesertaDidikRepository extends BasicResourcesRepository
{
    public function __construct()
    {
        $this->model = PesertaDidik::class;
    }

    private static function model()
    {
        return PesertaDidik::class;
    }
    
    public static function dashboard()
    {
        $daftar_peserta_didik = self::semuaPesertaDidik();
        $adm_reg  = AlamatRepository::daftarProvinsi();

        return [$daftar_peserta_didik, $adm_reg];
    }

    public static function edit($id)
    {
        return [
            self::model()::find($id),
            $adm_reg  = AlamatRepository::daftarProvinsi()
        ];
    }

    public static function semuaPesertaDidik()
    {
        return self::model()::all();
    }
}
