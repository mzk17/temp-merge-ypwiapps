<?php

namespace App\Repositories\Prime;

use App\Repositories\BasicResourcesRepository;
use App\Constants\AttributSekolah;
use SevenArts\Models\Prime\Sekolah;
use SevenArts\Models\Sidik\KelasSekolah;

class SekolahRepository extends BasicResourcesRepository
{
    public function __construct($id_sekolah = null)
    {
        $this->model = (is_null($id_sekolah)) ? Sekolah::class : Sekolah::find($id_sekolah);
    }

    private static function model()
    {
        return Sekolah::class;
    }
    
    public static function dashboard()
    {
        $sekolah         = self::semuaSekolah();
        $tingkat_sekolah = self::tingkatSekolah();
        $adm_reg         = AlamatRepository::daftarProvinsi();

        return [$sekolah, $tingkat_sekolah, $adm_reg];
    }

    public static function edit($id)
    {
        return [
            self::model()::find($id),
            SekolahRepository::tingkatSekolah(),
            AlamatRepository::daftarProvinsi(),
        ];
    }

    public static function semuaSekolah()
    {
        return self::model()::all();
    }

    public static function tingkatSekolah()
    {
        return AttributSekolah::TINGKATAN_SEKOLAH;
    }

    public function semuaKelas()
    {
        return $this->model->kelas;
    }

    public function daftarSiswa($kelas_tertentu = [])
    {
        if (is_string($kelas_tertentu)) {
            $kelas_tertentu = explode(",", $kelas_tertentu);
        }

        if (count($kelas_tertentu) > 0) {
            return $this->semuaKelas()
                ->whereIn('id', $kelas_tertentu)
                ->flatMap(function ($kelas) {
                    return $kelas->siswa;
                });
        }

        return $this->semuaKelas()->flatMap(function ($kelas) {
            return $kelas->siswa;
        });
    }
}
