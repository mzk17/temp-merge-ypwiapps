<?php

namespace App\Repositories\Prime;

use App\Repositories\BasicResourcesRepository;
use SevenArts\Models\Prime\TenagaPendidik;

class TenagaPendidikRepository extends BasicResourcesRepository
{
    public function __construct()
    {
        $this->model = TenagaPendidik::class;
    }

    private static function model()
    {
        return TenagaPendidik::class;
    }
    
    public static function dashboard()
    {
        $daftar_tenaga_pendidik = self::semuaTenagaPendidik();
        $adm_reg  = AlamatRepository::daftarProvinsi();

        return [$daftar_tenaga_pendidik, $adm_reg];
    }

    public static function edit($id)
    {
        return [
            self::model()::find($id),
            $adm_reg  = AlamatRepository::daftarProvinsi()
        ];
    }

    public static function semuaTenagaPendidik()
    {
        return self::model()::all();
    }
}
