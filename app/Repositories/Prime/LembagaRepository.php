<?php

namespace App\Repositories\Prime;

use App\Repositories\BasicResourcesRepository;
use SevenArts\Models\Prime\Lembaga;

class LembagaRepository extends BasicResourcesRepository
{
    public function __construct()
    {
        $this->model = Lembaga::class;
    }

    private static function model()
    {
        return Lembaga::class;
    }
    
    public static function dashboard()
    {
        $lembaga         = self::semuaLembaga();
        $adm_reg         = AlamatRepository::daftarProvinsi();

        return [$lembaga, $adm_reg];
    }

    public static function edit($id)
    {
        return [
            self::model()::find($id),
            AlamatRepository::daftarProvinsi(),
        ];
    }

    public static function semuaLembaga()
    {
        return self::model()::all();
    }
}
