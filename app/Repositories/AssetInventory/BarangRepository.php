<?php

namespace App\Repositories\AssetInventory;

use App\Repositories\BasicResourcesRepository;
use SevenArts\Models\AssetInventory\Barang;

class BarangRepository extends BasicResourcesRepository
{
    public function __construct()
    {
        $this->model = Barang::class;
    }

    public static function model()
    {
        return Barang::class;
    }

    public static function dashboard($cari = null)
    {
        return ($cari)
            ? self::cariBarang($cari)
            : self::semuaBarang();
    }

    public static function semuaBarang()
    {
        return self::model()::all();
    }

    public static function cariBarang($keyword)
    {
        return self::model()::where('nama_barang','LIKE','%'.$keyword.'%')->get();
    }
}