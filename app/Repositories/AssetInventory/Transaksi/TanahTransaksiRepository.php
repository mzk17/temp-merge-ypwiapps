<?php

namespace App\Repositories\AssetInventory\Transaksi;

use App\Repositories\AssetInventory\KategoriRepository;
use App\Repositories\BasicResourcesRepository;
use App\Repositories\Prime\SekolahRepository;
use SevenArts\Models\AssetInventory\TanahTransaksi;

class TanahTransaksiRepository extends BasicResourcesRepository
{
    public function __construct()
    {
        $this->model = TanahTransaksi::class;
    }

    private static function model()
    {
        return TanahTransaksi::class;
    }

    public static function dashboard()
    {
        return [
            self::semuaTanahTransaksi(),
            KategoriRepository::semuaKategori(),
            SekolahRepository::semuaSekolah()
        ];
    }

    public static function semuaTanahTransaksi()
    {
        return self::model()::all();
    }

    public function cariTanahTransaksi($keyword)
    {
        return [
            $this->model::where('id','LIKE','%'.$keyword.'%')->get(),
            KategoriRepository::semuaKategori(),
            SekolahRepository::semuaSekolah()
        ];
    }
}