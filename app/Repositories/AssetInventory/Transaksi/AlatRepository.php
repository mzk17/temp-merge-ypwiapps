<?php

namespace App\Repositories\AssetInventory\Transaksi;

use App\Repositories\BasicResourcesRepository;
use App\Repositories\Prime\SekolahRepository;
use App\Repositories\AssetInventory\BarangRepository;
use App\Repositories\AssetInventory\KategoriRepository;
use SevenArts\Models\AssetInventory\AlatTransaksi;

class AlatRepository extends BasicResourcesRepository
{
    public function __construct()
    {
        $this->model = AlatTransaksi::class;
    }

    public static function model()
    {
        return AlatTransaksi::class;
    }

    public static function dashboard()
    {
        return [
            self::semuaAlat(), // $data_alat_trans
            BarangRepository::semuaBarang(), // $c_barang
            KategoriRepository::semuaKategori(), // $c_kategori
            SekolahRepository::semuaSekolah() // $c_sekolah
        ];
    }

    public static function semuaAlat()
    {
        return self::model()::all();
    }

    public static function cariAlat($keyword)
    {
        return self::model()::where('nama_barang','LIKE','%'.$keyword.'%')->get();
    }
}