<?php

namespace App\Repositories\AssetInventory\Transaksi;

use App\Repositories\BasicResourcesRepository;
use App\Repositories\AssetInventory\KategoriRepository;
use App\Repositories\AssetInventory\RuanganRepository;
use App\Repositories\Prime\SekolahRepository;
use SevenArts\Models\AssetInventory\BangunanTransaksi;

class BangunanRepository extends BasicResourcesRepository
{
    public function __construct()
    {
        $this->model = BangunanTransaksi::class;
    }

    public static function model()
    {
        return BangunanTransaksi::class;
    }

    public static function dashboard()
    {
        return [
            self::semuaBangunan(), // $data_bangunan_trans
            KategoriRepository::semuaKategori(), // $c_kategori
            RuanganRepository::semuaRuangan(), // $c_ruangan
            SekolahRepository::semuaSekolah() // $c_sekolah
        ];
    }

    public static function semuaBangunan()
    {
        return self::model()::all();
    }

    public static function cariBangunan($keyword)
    {
        return self::model()::where('id','LIKE','%'.$keyword.'%')->get();
    }
}