<?php

namespace App\Repositories\AssetInventory;

use App\Repositories\BasicResourcesRepository;
use SevenArts\Models\AssetInventory\Kategori;

class KategoriRepository extends BasicResourcesRepository
{
    public function __construct()
    {
        $this->model = Kategori::class;
    }

    private static function model()
    {
        return Kategori::class;
    }

    public static function dashboard($cari)
    {
        return ($cari)
            ? self::cariKategori($cari)
            : self::semuaKategori();
    }

    public static function semuaKategori()
    {
        return self::model()::all();
    }

    public static function cariKategori($keyword, $tag = 'kategori')
    {
        return self::model()::where($tag,'LIKE','%'.$keyword.'%')->get();
    }
}