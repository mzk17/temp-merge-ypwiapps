<?php

namespace App\Repositories\AssetInventory;

use App\Repositories\BasicResourcesRepository;
use SevenArts\Models\AssetInventory\Supplier;

class SupplierRepository extends BasicResourcesRepository
{
    public function __construct()
    {
        $this->model = Supplier::class;
    }

    private static function model()
    {
        return Supplier::class;
    }

    public static function dashboard($cari)
    {
        return ($cari)
            ? self::cariSupplier($cari)
            : self::semuaSupplier();
    }

    public static function semuaSupplier()
    {
        return self::model()::all();
    }

    public static function cariSupplier($keyword, $tag = 'nama_supplier')
    {
        return self::model()::where($tag,'LIKE','%'.$keyword.'%')->get();
    }
}