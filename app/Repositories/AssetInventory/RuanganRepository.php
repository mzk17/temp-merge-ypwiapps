<?php

namespace App\Repositories\AssetInventory;

use App\Repositories\BasicResourcesRepository;
use SevenArts\Models\AssetInventory\Ruangan;

class RuanganRepository extends BasicResourcesRepository
{
    public function __construct()
    {
        $this->model = Ruangan::class;
    }

    private static function model()
    {
        return Ruangan::class;
    }

    public static function dashboard($cari)
    {
        return ($cari)
            ? self::cariRuangan($cari)
            : self::semuaRuangan();
    }

    public static function semuaRuangan()
    {
        return self::model()::all();
    }

    public static function cariRuangan($keyword)
    {
        return self::model()::where('nama_ruangan','LIKE','%'.$keyword.'%')->get();
    }
}