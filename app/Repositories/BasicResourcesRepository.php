<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

class BasicResourcesRepository
{
    public $model;
    /**
     * Fungsi dasar untuk memilih sebuah entitas
     * pada model.
     *
     * @param  \Illuminate\Database\Eloquent\Model $model
     * @param  string  $id
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function find($id)
    {
        try {
            return $this->model::findOrFail($id);
        } catch (\Exception $e) {
            report($e);
            throw $e;
        }
    }

    /**
     * Fungsi dasar untuk membuat sebuah entitas
     * pada model.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(array $data)
    {
        try {
            return $this->model::create($data);
        } catch (\Exception $e) {
            report($e);
            throw $e;
        }
    }

    /**
     * Fungsi dasar untuk memperbarui sebuah entitas
     * pada model.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     * @param string $id
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function update($id, array $data)
    {
        try {
            $updated_model = $this->model::findOrFail($id);
            foreach ($data as $field_name => $new_value) {
                $updated_model->{$field_name} = $new_value;
            }
            $updated_model->save();

            return $updated_model;
        } catch (\Exception $e) {
            report($e);
            throw $e;
        }
    }

    /**
     * Fungsi dasar untuk menghapus sebuah entitas
     * pada model.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     * @param string $id
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function destroy($id)
    {
        try {
            $entity = $this->model::findOrFail($id);
            $destroyed_entity = $entity;
            // Action
            $entity->delete();

            return [
                'result' => true,
                'entity' => $destroyed_entity
            ];
        } catch (\Exception $e) {
            report($e);
            return [
                'result' => false
            ];
            // throw $e;
        }
    }
}
