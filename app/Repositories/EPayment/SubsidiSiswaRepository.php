<?php

namespace App\Repositories\EPayment;

use App\Events\EPayment\SubsidiSiswaCreated;
use App\Repositories\BasicResourcesRepository;
use Illuminate\Support\Facades\DB;
use SevenArts\Models\EPayment\SubsidiPembiayaan;

class SubsidiSiswaRepository extends BasicResourcesRepository
{
    public function __construct()
    {
        $this->model = SubsidiPembiayaan::class;
    }

    public function simpanSekaligus(array $data)
    {
        \Log::info('[REPOSITORY] SubsidiSiswaRepository@simpanSekaligus - Menyimpan data subsidi siswa untuk tahun ajaran baru dimulai');
        try {
            DB::connection('ypwi_epayment')->table('subsidi_pembiayaan')->insert($data);

            $id_pembiayaan_siswa = [];
            foreach ($data as $d) {
                $id_pembiayaan_siswa[] = $d['id_pembiayaan_siswa'];
            }

            $subsidi = SubsidiPembiayaan::select('id_referensi_subsidi', 'id_pembiayaan_siswa', 'id_peserta_didik', 'total_biaya_setelah_subsidi')
                ->whereIn('id_pembiayaan_siswa', $id_pembiayaan_siswa)
                ->groupBy('id_referensi_subsidi', 'id_pembiayaan_siswa', 'id_peserta_didik', 'total_biaya_setelah_subsidi')
                ->get();

            event(new SubsidiSiswaCreated($subsidi));

            \Log::info('[REPOSITORY] SubsidiSiswaRepository@simpanSekaligus - Data subsidi siswa untuk tahun ajaran baru berhasil disimpan');

            return true;
        } catch (\Exception $e) {
            \Log::error('[REPOSITORY] SubsidiSiswaRepository@simpanSekaligus - Data subsidi siswa untuk tahun ajaran baru gagal disimpan. Berikut errornya -> ' . json_encode($e->getMessage()));
            
            return false;
        }
    }
}