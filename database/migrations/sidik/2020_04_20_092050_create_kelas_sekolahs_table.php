<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKelasSekolahsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('ypwi_sidik')->create('kelas_sekolah', function (Blueprint $table) {
            $table->increments('id');
            $table->char('id_sekolah', 36);
            $table->string('nama_kelas');
            $table->tinyInteger('urutan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('ypwi_sidik')->dropIfExists('kelas_sekolah');
    }
}
