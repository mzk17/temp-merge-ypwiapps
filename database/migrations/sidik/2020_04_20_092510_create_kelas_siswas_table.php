<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKelasSiswasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('ypwi_sidik')->create('kelas_siswa', function (Blueprint $table) {
            $table->integer('id_kelas');
            $table->char('id_siswa', 36);
            $table->string('tahun_ajaran')->nullable();
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('ypwi_sidik')->dropIfExists('kelas_siswas');
    }
}
