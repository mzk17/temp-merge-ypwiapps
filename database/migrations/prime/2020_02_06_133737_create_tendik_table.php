<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTendikTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('ypwi_prime')->create('tendik', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('nuptk')->nullable();
            $table->string('nip')->nullable();
            $table->unsignedTinyInteger('status_kepegawaian')->nullable(); // ada tabelnya
            $table->unsignedTinyInteger('jenis_ptk')->nullable(); // ada tabelnya
            $table->unsignedTinyInteger('tugas_tambahan')->nullable(); // ada tabelnya
            $table->string('sk_cpns')->nullable();
            $table->string('tanggal_cpns')->nullable();
            $table->string('sk_pengangkatan')->nullable();
            $table->string('tmt_pengangkatan')->nullable();
            $table->unsignedTinyInteger('lembaga_pengangkatan')->nullable(); // ada tabelnya
            $table->unsignedTinyInteger('pangkat_golongan')->nullable(); // ada tabelnya
            $table->unsignedTinyInteger('sumber_gaji')->nullable(); // ada tabelnya
            $table->string('tmt_pns')->nullable();
            $table->boolean('sudah_lisensi_kepala_sekolah')->default(0)->nullable();
            $table->boolean('pernah_diklat_kepengawasan')->nullable();
            $table->boolean('keahlian_braille')->default(0);
            $table->boolean('keahlian_bahasa_isyarat')->default(0);
            $table->string('npwp')->nullable();
            $table->string('nama_wajib_pajak')->nullable();
            $table->string('kewarganegaraan', 3)->default("ID")->nullable();
            $table->string('bank')->nullable();
            $table->string('nomor_rekening_bank')->nullable();
            $table->string('rekening_atas_nama')->nullable();
            $table->string('karpeg')->nullable();
            $table->string('karis_karsu')->nullable();
            $table->string('nuks')->nullable();
            $table->string('nama_ibu_kandung')->nullable();
            $table->unsignedTinyInteger('status_perkawinan')->nullable(); // ada tabelnya
            $table->string('nama_suami_istri')->nullable();
            $table->string('nip_suami_istri')->nullable();
            $table->string('pekerjaan_suami_istri')->nullable();
            $table->boolean('sebagai_guru')->default(1);
            $table->char('id_data_pribadi', 36);
            $table->timestamps();

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('ypwi_prime')->dropIfExists('tendik');
    }
}
