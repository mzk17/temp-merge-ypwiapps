<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePesertaDidikTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('ypwi_prime')->create('peserta_didik', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('nipd')->nullable();
            $table->string('nisn');
            $table->unsignedTinyInteger('alat_transportasi')->nullable(); // ada tabelnya
            $table->string('skhun')->nullable();
            $table->boolean('penerima_kps')->default(0);
            $table->string('no_kps')->nullable();
            $table->string('rombel_saat_ini');
            $table->string('no_peserta_ujian_nasional')->nullable();
            $table->string('no_seri_ijazah')->nullable();
            $table->boolean('penerima_kip')->default(0);
            $table->string('nomor_kip')->nullable();
            $table->string('nama_di_kip')->nullable();
            $table->string('nomor_kks')->nullable();
            $table->string('no_registrasi_akta_lahir')->nullable();
            $table->string('bank')->nullable();
            $table->string('nomor_rekening_bank')->nullable();
            $table->string('rekening_atas_nama')->nullable();
            $table->boolean('layak_pip_usulan_dari_sekolah')->default(0);
            $table->string('alasan_layak_pip')->nullable();
            $table->string('kebutuhan_khusus')->default("Tidak ada"); // ini harusnya boolean
            $table->string('sekolah_asal')->nullable();
            $table->unsignedSmallInteger('jarak_rumah_ke_sekolah_km')->nullable();
            $table->unsignedTinyInteger('jenis_tinggal')->nullable(); // ada tabelnya
            $table->string('nama_ayah')->nullable();
            $table->string('nik_ayah')->nullable();
            $table->string('tahun_lahir_ayah', 4)->nullable();
            $table->string('jenjang_pendidikan_ayah')->nullable();
            $table->string('pekerjaan_ayah')->nullable();
            $table->unsignedTinyInteger('penghasilan_ayah')->nullable();
            $table->string('nama_ibu')->nullable();
            $table->string('nik_ibu')->nullable();
            $table->string('tahun_lahir_ibu', 4)->nullable();
            $table->string('jenjang_pendidikan_ibu')->nullable();
            $table->string('pekerjaan_ibu')->nullable();
            $table->unsignedTinyInteger('penghasilan_ibu')->nullable();
            $table->string('nama_wali')->nullable();
            $table->string('nik_wali')->nullable();
            $table->string('tahun_lahir_wali', 4)->nullable();
            $table->string('jenjang_pendidikan_wali')->nullable();
            $table->string('pekerjaan_wali')->nullable();
            $table->unsignedTinyInteger('penghasilan_wali')->nullable();
            $table->tinyInteger('anak_keberapa')->nullable();
            $table->unsignedTinyInteger('berat_badan')->nullable();
            $table->unsignedTinyInteger('tinggi_badan')->nullable();
            $table->string('lingkar_kepala')->nullable();
            $table->tinyInteger('jml_saudara_kandung')->nullable();
            $table->char('id_data_pribadi', 36);
            $table->timestamps();

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('ypwi_prime')->dropIfExists('peserta_didik');
    }
}
