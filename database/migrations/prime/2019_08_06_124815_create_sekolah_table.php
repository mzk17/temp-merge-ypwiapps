<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSekolahTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('ypwi_prime')->create('sekolah', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('nama_sekolah');
            $table->string('id_alamat');
            $table->tinyInteger('id_tingkat_sekolah');
            $table->string('telepon', 15)->nullable();
            $table->string('email')->nullable();
            $table->timestamps();

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('ypwi_prime')->dropIfExists('sekolah');
    }
}
