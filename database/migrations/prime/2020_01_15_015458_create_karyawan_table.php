<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKaryawanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('ypwi_prime')->create('karyawan', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('nuptk', 25)->nullable();
            $table->string('niy_nigk', 20)->nullable();
            $table->unsignedTinyInteger('status_kepegawaian')->nullable();
            $table->unsignedTinyInteger('jenis_ptk')->nullable();
            $table->unsignedTinyInteger('tugas_tambahan')->nullable();
            $table->string('sk_ypwi', 35)->nullable();
            $table->date('tanggal_sk_ypwi')->nullable();
            $table->string('sk_pengangkatan', 35)->nullable();
            $table->date('tmt_pengangkatan')->nullable();
            $table->unsignedTinyInteger('lembaga_pengangkatan')->nullable();
            $table->unsignedTinyInteger('sumber_gaji')->nullable();
            $table->string('nama_ibu_kandung', 191)->nullable();
            $table->tinyInteger('status_perkawinan')->nullable();
            $table->string('nama_suami_istri')->nullable();
            $table->string('niy_nigk_suami_istri', 20)->nullable();
            $table->string('pekerjaan_suami_istri')->nullable();
            $table->date('tmt_pengangkatan_suami_istri')->nullable();
            $table->boolean('sudah_lisensi_kepala_sekolah')->nullable();
            $table->boolean('pernah_diklat_kepengawasan')->nullable();
            $table->boolean('keahlian_braille')->nullable();
            $table->boolean('keahlian_bahasa_isyarat')->nullable();
            $table->string('npwp', 25)->nullable();
            $table->string('nama_wajib_pajak')->nullable();
            $table->string('kewarganegaraan', 3)->nullable();
            $table->string('bank')->nullable();
            $table->string('nomor_rekening_bank')->nullable();
            $table->string('rekening_atas_nama')->nullable();
            $table->string('karpeg', 30)->nullable();
            $table->string('karis_karsu', 30)->nullable();
            $table->string('lintang', 30)->nullable();
            $table->string('bujur', 30)->nullable();
            $table->string('nuks', 30)->nullable();
            $table->char('id_data_pribadi', 36);
            $table->timestamps();

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('ypwi_prime')->dropIfExists('karyawan');
    }
}
