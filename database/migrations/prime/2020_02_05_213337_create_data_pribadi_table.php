<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataPribadiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('ypwi_prime')->create('data_pribadi', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('nama');
            $table->string('jenis_kelamin', 1); // 'jk' di DAPODIK
            $table->string('tempat_lahir');
            $table->string('tanggal_lahir');
            $table->tinyInteger('agama')->default(1); // ada tabelnya
            $table->uuid('alamat_1');
            $table->uuid('alamat_2')->nullable();
            $table->uuid('alamat_3')->nullable();
            $table->string('telepon')->nullable();
            $table->string('hp')->nullable();
            $table->string('email')->nullable();
            $table->string('nik')->nullable();
            $table->string('no_kk')->nullable();
            $table->timestamps();

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('ypwi_prime')->dropIfExists('data_pribadi');
    }
}
