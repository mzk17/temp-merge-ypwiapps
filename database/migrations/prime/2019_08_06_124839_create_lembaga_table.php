<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLembagaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('ypwi_prime')->create('lembaga', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('nama_yayasan');
            $table->bigInteger('id_alamat');
            $table->timestamps();

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('ypwi_prime')->dropIfExists('lembaga');
    }
}
