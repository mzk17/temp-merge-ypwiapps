<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJabatanAkunsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('ypwi_prime')->create('jabatan_akun', function (Blueprint $table) {
            $table->char('id_akun_pengguna', 36);
            $table->tinyInteger('id_jabatan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('ypwi_prime')->dropIfExists('jabatan_akun');
    }
}
