<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTanahTransaksiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('ypwi_asset_inventory')->create('tanah_transaksi', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('sekolah_id');
            $table->unsignedInteger('kategori_id');
            $table->unsignedInteger('jenis_tanah_id');
            $table->string('kode_tanah', 128)->nullable();
            $table->string('no_reg_tanah', 75)->nullable();
            $table->string('luas', 10)->nullable();
            $table->year('tahun_pengadaan')->nullable();
            $table->text('letak_alamat')->nullable();
            $table->string('stts_tnh_hak', 75)->nullable();
            $table->date('stts_tnh_tglsertifikat')->nullable();
            $table->string('stts_tnh_nmrsertifikat', 75)->nullable();
            $table->string('penggunaan', 128)->nullable();
            $table->string('sumber_dana', 75)->nullable();
            $table->double('harga')->nullable();
            $table->text('keterangan')->nullable();
            $table->string('foto_tnh')->nullable();
            $table->timestamps();

            // Menjadikan kolom2 dibawah sebagai foreign key dan mereferensi kolom id pada tabel masing2
            // $table->foreign('sekolah_id')->references('id')->on('sekolah');
            $table->foreign('kategori_id')->references('id')->on('kategori');
            $table->foreign('jenis_tanah_id')->references('id')->on('jenis_tanah');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('ypwi_asset_inventory')->dropIfExists('tanah_transaksi');
    }
}
