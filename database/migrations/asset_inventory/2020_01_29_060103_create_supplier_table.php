<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupplierTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('ypwi_asset_inventory')->create('supplier', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_supplier');
            $table->text('alamat_supplier')->nullable();
            $table->string('telepon_supplier', 15)->nullable();
            $table->string('email_supplier')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('ypwi_asset_inventory')->dropIfExists('supplier');
    }
}
