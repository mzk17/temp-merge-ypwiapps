<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBangunanTransaksiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('ypwi_asset_inventory')->create('bangunan_transaksi', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('sekolah_id');
            $table->unsignedInteger('kategori_id');
            $table->unsignedInteger('ruangan_id');
            $table->string('kode_bangunan', 10)->nullable();
            $table->string('no_reg_bangunan', 25)->nullable();
            $table->string('kondisi_bangunan', 12)->nullable();
            $table->string('konstruksi_bangunan_bertingkat', 5)->nullable();
            $table->string('konstruksi_bangunan_beton', 5)->nullable();
            $table->string('luas_lantai', 20)->nullable();
            $table->text('letak_lokasi')->nullable();
            $table->date('tanggal_dokumen')->nullable();
            $table->string('nomor_dokumen', 75)->nullable();
            $table->string('luas_tanah', 20)->nullable();
            $table->string('status_tanah', 75)->nullable();
            $table->string('nomor_kode_tanah', 20)->nullable();
            $table->string('asal_usul', 191)->nullable();
            $table->double('harga')->nullable();
            $table->string('sumber_dana', 75)->nullable();
            $table->text('keterangan')->nullable();
            $table->string('foto_avatar')->nullable();
            $table->timestamps();

            // Menjadikan kolom2 dibawah sebagai foreign key dan mereferensi kolom id pada tabel masing2
            // $table->foreign('sekolah_id')->references('id')->on('sekolah');
            $table->foreign('kategori_id')->references('id')->on('kategori');
            $table->foreign('ruangan_id')->references('id')->on('ruangan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('ypwi_asset_inventory')->dropIfExists('bangunan_transaksi');
    }
}
