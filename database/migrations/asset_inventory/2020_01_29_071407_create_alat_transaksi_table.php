<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlatTransaksiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('ypwi_asset_inventory')->create('alat_transaksi', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('sekolah_id');
            $table->unsignedInteger('kategori_id');
            $table->unsignedInteger('barang_id');
            $table->string('kode_alat', 128)->nullable();
            $table->string('no_reg_alat', 75)->nullable();
            $table->string('merek_tipe')->nullable();
            $table->string('spesifikasi_ukuran_cc')->nullable();
            $table->string('bahan', 191)->nullable();
            $table->year('thn_pembelian')->nullable();
            $table->string('no_pabrik')->nullable();
            $table->string('no_rangka')->nullable();
            $table->string('no_mesin')->nullable();
            $table->string('no_polisi')->nullable();
            $table->string('no_bpkb')->nullable();
            $table->string('asal_usul')->nullable();
            $table->double('harga')->nullable();
            $table->string('sumber_dana', 75)->nullable();
            $table->char('kondisi', 5)->nullable();
            $table->text('keterangan')->nullable();
            $table->string('fotoavatar')->nullable();
            $table->timestamps();

            // Menjadikan kolom2 dibawah sebagai foreign key dan mereferensi kolom id pada tabel masing2
            // $table->foreign('sekolah_id')->references('id')->on('sekolah');
            $table->foreign('kategori_id')->references('id')->on('kategori');
            $table->foreign('barang_id')->references('id')->on('barang');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('ypwi_asset_inventory')->dropIfExists('alat_transaksi');
    }
}
