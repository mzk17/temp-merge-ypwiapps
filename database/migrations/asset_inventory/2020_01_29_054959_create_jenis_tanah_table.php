<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJenisTanahTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('ypwi_asset_inventory')->create('jenis_tanah', function (Blueprint $table) {
            $table->increments('id');
            $table->string('jenis_tanah', 191);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('ypwi_asset_inventory')->dropIfExists('jenis_tanah');
    }
}
