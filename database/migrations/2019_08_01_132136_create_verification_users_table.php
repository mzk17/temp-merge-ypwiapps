<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVerificationUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('ypwi_prime')->create('verifikasi_akun_pengguna', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_akun_pengguna');
            $table->string('token');
            $table->dateTime('waktu_kadaluarsa_token');
            $table->tinyInteger('status_token');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('ypwi_prime')->dropIfExists('verifikasi_akun_pengguna');
    }
}
