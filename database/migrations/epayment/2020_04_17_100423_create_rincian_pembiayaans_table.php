<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRincianPembiayaansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('ypwi_epayment')->create('rincian_pembiayaan', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('id_pembiayaan_sekolah');
            $table->string('nama_item');
            $table->decimal('biaya', 20, 6);
            $table->timestamps();

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('ypwi_epayment')->dropIfExists('rincian_pembiayaan');
    }
}
