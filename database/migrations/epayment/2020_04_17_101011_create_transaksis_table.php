<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransaksisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('ypwi_epayment')->create('transaksi', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('id_peserta_didik');
            $table->text('nama_transaksi')->comment('formatnya "#[8 karakter pertama dari UUID transaksi] Pembayaran [nama item pembiayaan] & [angka] pembiayaan lainnya"');
            $table->decimal('total', 20, 6);
            $table->longText('catatan')->nullable();
            $table->timestamps();

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('ypwi_epayment')->dropIfExists('transaksi');
    }
}
