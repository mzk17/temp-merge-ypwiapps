<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemPembiayaansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('ypwi_epayment')->create('item_pembiayaan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_pembiayaan');
            $table->string('frekuensi');
            
            /**
             * Kolom ini memberikan kesempatan untuk tiap sekolah merequest
             * item pembiayaan khusus untuk di sekolah mereka masing - masing
             */
            $table->uuid('id_sekolah')->nullable();
            $table->uuid('requestor')->nullable();          // USERID dari orang yang melakukan request
            $table->boolean('approved')->nullable();        // Status persetujuan dari item pembiayaan yang di-request. Moderasi oleh YPWI Pusat
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('ypwi_epayment')->dropIfExists('item_pembiayaan');
    }
}
