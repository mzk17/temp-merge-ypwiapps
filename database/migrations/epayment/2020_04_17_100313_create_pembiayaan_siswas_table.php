<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePembiayaanSiswasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('ypwi_epayment')->create('pembiayaan_siswa', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('id_pembiayaan_sekolah');
            $table->uuid('id_peserta_didik');
            $table->uuid('id_referensi_subsidi')->nullable();
            $table->decimal('biaya', 20, 6);
            $table->longText('catatan')->nullable();
            $table->boolean('status');
            $table->timestamps();

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('ypwi_epayment')->dropIfExists('pembiayaan_siswa');
    }
}
