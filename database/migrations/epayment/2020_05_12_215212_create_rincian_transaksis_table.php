<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRincianTransaksisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('ypwi_epayment')->create('rincian_transaksi', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('id_transaksi');
            $table->string('id_item_pembiayaan')->comment('Untuk item pembiayaan yang memiliki rincian maka formatnya [ID_ITEM_PEMBIAYAAN_SISWA]:[ID_RINCIAN_PEMBIAYAAN (Tabel rincian_pembiayaan)]');
            $table->decimal('bayar', 16, 2);
            $table->timestamps();

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('ypwi_epayment')->dropIfExists('rincian_transaksi');
    }
}
