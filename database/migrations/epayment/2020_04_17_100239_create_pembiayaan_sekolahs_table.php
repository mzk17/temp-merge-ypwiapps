<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePembiayaanSekolahsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('ypwi_epayment')->create('pembiayaan_sekolah', function (Blueprint $table) {
            $table->uuid('id');
            $table->integer('id_pembiayaan');
            $table->uuid('id_sekolah');
            $table->string('tahun_ajaran');
            $table->decimal('biaya_dasar', 20, 6);
            $table->string('nama_pembiayaan')->nullable();
            $table->string('kelas')->nullable();
            $table->string('suffix')->nullable();
            $table->date('tanggal_aktif')->nullable();
            $table->timestamps();

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('ypwi_epayment')->dropIfExists('pembiayaan_sekolah');
    }
}
