<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubsidiPembiayaansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('ypwi_epayment')->create('subsidi_pembiayaan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('id_referensi_subsidi')->comment('Kolom ini digunakan untuk memberikan gambaran untuk tahun ajaran baru bahwa item pembiayaan apa saja yang disubsidi untuk peserta didik yang bersangkutan');
            $table->uuid('id_pembiayaan_siswa');
            $table->uuid('id_rincian_pembiayaan')->nullable();
            $table->uuid('id_peserta_didik');
            $table->decimal('jumlah_subsidi', 12, 2);
            $table->decimal('biaya_setelah_subsidi', 12, 2);
            $table->decimal('total_biaya_setelah_subsidi', 12, 2);
            $table->longText('catatan')->nullable();
            $table->boolean('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('ypwi_epayment')->dropIfExists('subsidi_pembiayaan');
    }
}
