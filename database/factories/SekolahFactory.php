<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use SevenArts\Models\Prime\Sekolah;
use Faker\Generator as Faker;

$factory->define(Sekolah::class, function (Faker $faker) {
    return [
        'id' => Str::uuid(),
        'nama_sekolah' => $faker->name,
        'id_tingkat_sekolah' => rand(1, 5),
        'telepon' => $faker->e164PhoneNumber,
        'email' => $faker->email
    ];
});
