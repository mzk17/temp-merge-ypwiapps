<?php

use SevenArts\Models\Prime\Alamat;
use Faker\Generator as Faker;

$factory->define(Alamat::class, function (Faker $faker) {
    return [
        'id' => Str::uuid(),
        'nama_alamat' => 'Alamat Utama',
        'alamat_utama' => 1,
        'alamat' => $faker->address,
        'provinsi' => $faker->state,
        'kota' => $faker->city,
        'kecamatan' => $faker->country,
        'kode_pos' => $faker->postcode
    ];
});
