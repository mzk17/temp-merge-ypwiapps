<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use SevenArts\Models\Prime\DataPribadi;
use Faker\Generator as Faker;

$factory->define(DataPribadi::class, function (Faker $faker) {
    return [
        'id' => Str::uuid(),
        'nama' => 'Abdullah Faqih',
        'jenis_kelamin' => 'L',
        'tempat_lahir' => 'Makassar',
        'tanggal_lahir' => '12 Juni 2016',
        'agama' => 1,
        'telepon' => '0411863785',
        'hp' => '082296731729',
        'email' => 'nosky@gmail.com'
    ];
});
