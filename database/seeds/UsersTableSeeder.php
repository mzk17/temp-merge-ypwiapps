<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('akun_pengguna')->insert([
            'id' => \Str::uuid(),
            'id_jenis_akun' => 2,
            'username' => 'mizuki',
            'email' => 'izzuddin.alfikri@kymzk.com',
            'password' => Hash::make('rahasia'),
            'remember_token' => Str::random(60),
            'id_jabatan' => 99
        ]);
    }
}
