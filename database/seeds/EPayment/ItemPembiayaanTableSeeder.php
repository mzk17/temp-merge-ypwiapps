<?php

namespace DatabaseSeeder\EPayment;

use App\Constants\FrekuensiItemPembiayaan;
use Illuminate\Support\Facades\DB;

class ItemPembiayaanSeeder
{
    public static function createItemPembiayaan()
    {
        $spp = [
            'nama_pembiayaan' => 'SPP',
            'frekuensi' => FrekuensiItemPembiayaan::BULANAN
        ];
        $ppdb = [
            'nama_pembiayaan' => 'Pendaftaran Peserta Didik Baru (PPDB)',
            'frekuensi' => FrekuensiItemPembiayaan::SEKALI_SETAHUN
        ];
        manualTimestamps($spp);
        manualTimestamps($ppdb);

        $ip = array($spp, $ppdb);
        DB::connection('ypwi_epayment')->table('item_pembiayaan')
            ->insert($ip);
    }
}