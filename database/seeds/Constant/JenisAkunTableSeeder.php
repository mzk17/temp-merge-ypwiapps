<?php

namespace DatabaseSeeder\Constant;

use Illuminate\Support\Facades\DB;

class JenisAkunTableSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public static function createJenisAkun()
    {
        $jenisAkun = [
            'Masyarakat',
            'Pegawai Yayasan',
            'Pegawai Sekolah',
            'Siswa',
            'Orang Tua Siswa'
        ];

        foreach ($jenisAkun as $key => $namaJenisAkun) {
            DB::table('jenis_akun')->insert([
                'nama' => $namaJenisAkun
            ]);
        }
    }
}
