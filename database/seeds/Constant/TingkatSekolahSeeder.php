<?php

namespace DatabaseSeeder\Constant;

use Illuminate\Support\Facades\DB;

class TingkatSekolahSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public static function createTingkatSekolah()
    {
        $tingkatan = [
            'Pendidikan Anak Usia Dini (PAUD)',
            'Sekolah Dasar (SD)',
            'Sekolah Menengah Pertama (SMP)',
            'Sekolah Menengah Atas (SMA)',
            'Perguruan Tinggi / Universitas'
        ];

        foreach ($tingkatan as $id => $nama_tingkatan) {
            DB::connection('constant')
                ->table('tingkat_sekolah')
                ->insert([
                    'id' => $id,
                    'nama_tingkatan' => $nama_tingkatan
                ]);
        }
    }
}
