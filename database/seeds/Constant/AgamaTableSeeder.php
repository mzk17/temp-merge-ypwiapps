<?php

namespace DatabaseSeeder\Constant;

use Illuminate\Support\Facades\DB;

class AgamaTableSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public static function createAgama()
    {
        $agama = [
            1 => "Islam",
            2 => "Katolik",
            3 => "Protestan",
            4 => "Hindu",
            5 => "Buddha"
        ];

        foreach ($agama as $id => $nama_agama) {
            DB::connection('constant')
                ->table('agama')
                ->insert([
                    'nama' => $nama_agama
                ]);
        }
    }
}
