<?php

namespace DatabaseSeeder\Constant;

use Illuminate\Support\Facades\DB;

class JabatanTableSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public static function createJabatan()
    {
        $jabatan = [
            1 => "Ketua Yayasan",
            2 => "Bendahara Yayasan",
            3 => "Sekretaris Yayasan",
            4 => "Admin Yayasan",
            5 => "Staff Yayasan",
            6 => "Kepala Sekolah",
            7 => "Wakil Kepala Sekolah",
            8 => "Bendahara Sekolah",
            9 => "Sekretaris Sekolah",
            10 => "Guru", 
            11 => "Admin Sekolah",
            12 => "Pustakawan",
            13 => "Laboran",
            14 => "Staff Sekolah",
            15 => "Lainnya",
            99 => "Administrator"
        ];

        foreach ($jabatan as $id => $jabatan) {
            DB::connection('constant')
                ->table('jabatan')
                ->insert([
                    'id' => $id,
                    'nama' => $jabatan
                ]);
        }
    }
}
