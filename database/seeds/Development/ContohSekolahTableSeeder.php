<?php

namespace DatabaseSeeder\Development;

use Illuminate\Database\Seeder;

class ContohSekolahTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $sekolah = [
            ["tingkatan" => 1, "nama_sekolah" => "TK Islam Nabilah"],
            ["tingkatan" => 1, "nama_sekolah" => "TK Islam Wihdatul Ummah"],
            ["tingkatan" => 2, "nama_sekolah" => "SD Islam Terpadu Wihdatul Ummah"],
            ["tingkatan" => 2, "nama_sekolah" => "SD Islam Wahdah Islamiyah 01"],
            ["tingkatan" => 3, "nama_sekolah" => "SMP Islam Terpadu Wahdah Islamiyah"],
            ["tingkatan" => 4, "nama_sekolah" => "SMA Islam Terpadu Wahdah Islamiyah"],
        ];

        foreach ($sekolah as $key => $value) {
            // Buat alamat terlebih dahulu
            $id_alamat = \Str::uuid();

            DB::connection('ypwi_prime')->table('alamat')
                ->insert([
                    'id' => \Str::uuid(),
                    'alamat' => $faker->address
                ]);
            
            DB::connection('ypwi_prime')->table('sekolah')
                ->insert([
                    'id' => \Str::uuid(),
                    'nama_sekolah' => $value['nama_sekolah'],
                    'id_alamat' => $id_alamat,
                    'id_tingkat_sekolah' => $value['tingkatan']
                ]);
        }
    }
}
