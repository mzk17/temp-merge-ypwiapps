<?php

namespace DatabaseSeeder\Development;

use Illuminate\Database\Seeder;

class SekolahTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(SevenArts\Models\Prime\Alamat::class, 1)->create()->each(function ($alamat) {
            $alamat->sekolah()->save(factory(SevenArts\Models\Prime\Sekolah::class)->make());
        });
    }
}
