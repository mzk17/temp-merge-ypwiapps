<?php

namespace DatabaseSeeder\Development;

use Illuminate\Database\Seeder;

class DataPribadiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            'id' => Str::uuid(),
            'nama' => 'Abdullah Faqih',
            'jenis_kelamin' => 'L',
            'tempat_lahir' => 'Makassar',
            'tanggal_lahir' => '12 Juni 2016',
            'agama' => 1,
            'alamat_1' => Str::uuid(),
            'telepon' => '0411863785',
            'hp' => '082296731729',
            'email' => 'nosky@gmail.com'
        );
        DB::connection('ypwi_zero')->table('data_pribadi')
            ->insert($data);
    }
}
