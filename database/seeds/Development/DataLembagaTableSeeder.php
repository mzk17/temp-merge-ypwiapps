<?php

namespace DatabaseSeeder\Development;

use Illuminate\Database\Seeder;

class DataLembagaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('ypwi_zero')->table('data_lembaga')
            ->insert([
                'id' => 'FDT001',
                'nama_yayasan' => 'Yayasan Pesantren Wahdah Islamiyah',
                'id_alamat' => 1
            ]);
    }
}
