<?php

use DatabaseSeeder\Constant\AgamaTableSeeder;
use DatabaseSeeder\Constant\JabatanTableSeeder;
use DatabaseSeeder\Constant\JenisAkunTableSeeder;
use DatabaseSeeder\Constant\TingkatSekolahSeeder;
use DatabaseSeeder\EPayment\ItemPembiayaanSeeder;

class ProductionSeeder
{
    public static function seed($modules = [])
    {
        /**
         * ----------------------------------------------------------------------
         * Item Konstanta
         * Dijalankan saat pertama kali deploy
         * ----------------------------------------------------------------------
         */
        if (in_array('constant', $modules)) {
            self::constant();
        }
        
        // Modul e-Payment
        if (in_array('epayment', $modules)) {
            self::epayment();
        }
    }

    private static function constant()
    {
        // Master Jenis Akun
        JenisAkunTableSeeder::createJenisAkun();
        
        // Master Jabatan
        JabatanTableSeeder::createJabatan();

        // Master Agama
        AgamaTableSeeder::createAgama();

        // Master Tingkat Sekolah
        TingkatSekolahSeeder::createTingkatSekolah();
    }

    private static function epayment()
    {
        // Buat master item pembiayaan
        ItemPembiayaanSeeder::createItemPembiayaan();
    }
}