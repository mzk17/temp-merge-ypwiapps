<?php

use Illuminate\Support\Facades\DB;

class DevelopmentSeeder
{
    public static function seed()
    {
        // Daftarkan Pegawai ke Sekolah
        self::registerEmployersToSchools();

        // Relasi Siswa & Sekolah
        $daftar_sekolah = \SevenArts\Models\Prime\Sekolah::all();
        foreach ($daftar_sekolah as $key => $sekolah) {
            self::createClassesForSchool($sekolah);
        }
        self::registerStudentsToSchools();
    }

    /**
     * Sekolah <> Pegawai
     */
    private static function registerEmployersToSchools()
    {
        $output = [];
        $sekolah = \SevenArts\Models\Prime\Sekolah::select('id')->get();
        $tendik = \SevenArts\Models\Prime\TenagaPendidik::select('id')->get()->toArray();

        $pembagian = array_chunk($tendik, ceil(count($tendik) / $sekolah->count()));

        foreach ($pembagian as $key => $chunk) {
            foreach ($chunk as $chunk_key => $tendik_id) {
                $output[] = [
                    'id_sekolah' => $sekolah[$key]['id'],
                    'id_tendik' => $tendik_id['id']
                ];
            }
        }
        DB::connection('ypwi_prime')->table('sekolah_tendik')
            ->insert($output);
    }

    /**
     * Sekolah <> Kelas <> Peserta Didik
     */
    private static function createClassesForSchool($sekolah)
    {
        switch ($sekolah->id_tingkat_sekolah) {
            case 1:
                $kelas = ['Nol Besar', 'Nol Kecil'];
                break;
            case 2:
                $kelas = self::classNameFactory(1, 6);
                break;
            case 3:
                $kelas = self::classNameFactory(7, 9);
                break;
            case 4:
                $kelas = self::classNameFactory(10, 12);
                break;
        }

        $output = [];

        foreach ($kelas as $key => $nama_kelas) {
            $output[] = [
                'id_sekolah' => $sekolah->id,
                'nama_kelas' => $nama_kelas,
                'tingkatan' => ($key + 1 > 6) ? ceil(($key+1)/2) : $key + 1,
            ];
        }

        DB::connection('ypwi_sidik')->table('kelas_sekolah')
            ->insert($output);
    }

    private static function classNameFactory($pertama, $terakhir)
    {
        $romawi = [
            1 => 'I',
            2 => 'II',
            3 => 'III',
            4 => 'IV',
            5 => 'V',
            6 => 'VI',
            7 => 'VII',
            8 => 'VIII',
            9 => 'IX',
            10 => 'X',
            11 => 'XI',
            12 => 'XII'
        ];
        $ab = [1 => 'A', 2 => 'B'];
        $kelas = [];

        for($i = $pertama; $i <= $terakhir; $i++) {
            for($j = 1; $j <= 2; $j++) {
                array_push($kelas, $romawi[$i] . " " . $ab[$j]);
            }
        }

        return $kelas;
    }

    private static function registerStudentsToSchools()
    {
        $output = [];
        $pd = \SevenArts\Models\Prime\PesertaDidik::select('id', 'rombel_saat_ini')->get();
        $daftar_kelas =  \SevenArts\Models\Sidik\KelasSekolah::where('id_sekolah', '7cc9c68c-a056-4507-961d-3e4def4d54d6')->get();

        foreach ($pd as $siswa) {
            $spl_kelas = explode(" ", $siswa->rombel_saat_ini, 2);
            $kelas = $daftar_kelas->where('nama_kelas', $spl_kelas[1])->first();

            $output[] = [
                'id_kelas' => $kelas->id,
                'id_siswa' => $siswa->id,
                'tahun_ajaran' => '2020/2021',
                'status' => 1
            ];
        }

        DB::connection('ypwi_sidik')->table('kelas_siswa')
            ->insert($output);
    }
}