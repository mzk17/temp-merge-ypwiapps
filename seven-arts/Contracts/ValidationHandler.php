<?php

namespace SevenArts\Contracts;

interface ValidationHandler
{
    public static function onlyNeededFields(array $source, array $validated_fields);

    public static function validateAction(array $data, array $all_fields, $error_validation_messages);
}
