<?php

namespace SevenArts\Contracts;

interface BasicResourceMethods
{
    public function actionShow($payload);

    public function actionStore($data);

    public function actionUpdate($data, $id);

    public function actionDestroy($data, $id);
}
