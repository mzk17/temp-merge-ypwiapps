<?php

namespace SevenArts\Repositories\EPayment;

use Illuminate\Support\Facades\DB;
use SevenArts\Models\EPayment\RincianPembiayaan;
use SevenArts\Models\EPayment\Transaksi;

class TransaksiRepository
{
    /**
     * Getters
     */
    public function lihatPembayaran($id_transaksi)
    {
        return Transaksi::find($id_transaksi);
    }
    
    public function transaksiBatch($transaksi)
    {
        return Transaksi::whereIn('batch', explode(",", $transaksi->batch))->where('id', '!=', $transaksi->id)->get();
    }

    public static function lihatInfoItemPembiayaanSiswa($id_transaksi)
    {
        $sub = DB::table('ypwi_epayment.rincian_transaksi')
            ->select(DB::raw('left(id_item_pembiayaan, 36) as id_item_pembiayaan, bayar'))
            ->where('id_transaksi', $id_transaksi);
    
        return DB::table(DB::raw("({$sub->toSql()}) as sub"))
            ->mergeBindings($sub)
            ->select(DB::raw('id_item_pembiayaan, SUM(bayar) as total_dibayar'))
            ->groupBy('id_item_pembiayaan')
            ->get();
    }

    public function lihatRincianPembiayaan($id_rincian)
    {
        if (is_array($id_rincian)) {
            $output = [];
            foreach ($id_rincian as $id) {
                array_push($output, RincianPembiayaan::find($id));
            }

            return $output;
        } else {
            return RincianPembiayaan::find($id_rincian);
        }
    }

    /**
     * Setters
     */
    public function buatTransaksi($request)
    {
        try {
            // Terdapat event listener pada action ini, mengupdate status pembiayaan apakah LUNAS atau belum
            $transaksi_payload = $request->only([
                'id_peserta_didik',
                'nama_transaksi',
                'total',
                'catatan'
            ]);

            // Buat Transaksi
            $transaksi = Transaksi::create($transaksi_payload);

            // Catat rincian transaksi
            $this->catatRincianTransaksi($request, $transaksi);

            return $transaksi;
        } catch (\Exception $e) {
            \Log::error("[REPOSITORY] - Gagal membuat transaksi. Pesan errornya -> " . json_encode($e->getMessage()));
            return false;
        }
    }

    private function catatRincianTransaksi($request, $transaksi)
    {
        $rincian_transaksi = [];

        foreach($request->rincian as $rincian) {
            $payload = [
                'id' => \Str::uuid(),
                'id_transaksi' => $transaksi->id,
                'id_item_pembiayaan' => $rincian['id_item_pembiayaan'],
                'bayar' => $rincian['bayar']
            ];
            manualTimestamps($payload);

            array_push($rincian_transaksi, $payload);
        }

        try {
            DB::connection('ypwi_epayment')->table('rincian_transaksi')->insert($rincian_transaksi);

            return true;
        } catch (\Exception $e) {
            \Log::error('[REPOSITORY] TransaksiRepository@catatRincianTransaksi - Gagal menyimpan rincian transaksi dengan nomor transaksi ' . $transaksi->id . ' | Errornya adalah -> ' . json_encode($e->getMessage()));
            return false;
        }
    }
}