<?php

namespace SevenArts\Repositories\EPayment;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use SevenArts\Models\EPayment\PembiayaanSiswa;

class PembiayaanSiswaRepository
{
    /**
     * Getters
     */
    public function semuaPembiayaan($id_peserta_didik)
    {
        return PembiayaanSiswa::where('id_peserta_didik', $id_peserta_didik)->get();
    }

    public function pembiayaanBelumLunas($id_peserta_didik)
    {
        return $this->semuaPembiayaan($id_peserta_didik)->where('status', 0)->all();
    }

    public function sudahLunas($informasi_transaksi)
    {
        $pembiayaan = PembiayaanSiswa::where('id', $informasi_transaksi->id_item_pembiayaan)->first();

        return $pembiayaan->sisa() <= 0;
    }

    /**
     * Setters
     */
    public function simpan(array $payload_pembiayaan_siswa)
    {
        try {
            // Update fitur untuk masa depan
            // Pembiayaan yang lebih dari satu kali bisa memiliki rincian pembiayaan
            DB::connection('ypwi_epayment')->table('pembiayaan_siswa')
                ->insert($payload_pembiayaan_siswa);

            return true;
        } catch (\Throwable $th) {
            \Log::error(
                "[REPOSITORY] PembiayaanSiswaRepository@simpan - Gagal menyimpan pembiayaan siswa. Errornya => " . 
                json_encode($th->getMessage())
            );

            return false;
        }
    }

    public function updateSubsidi($where, $payload)
    {
        try {
            PembiayaanSiswa::where('id', $where->id_pembiayaan_siswa)
                ->where('id_peserta_didik', $where->id_peserta_didik)
                ->update([
                    'id_referensi_subsidi' => $payload['id_referensi_subsidi'],
                    'biaya' => $payload['total_biaya_setelah_subsidi']
                ]);
        } catch (\Exception $e) {
            \Log::error("[REPOSITORY] PembiayaanSiswaRepository@updateSubsidi - Gagal memperbarui data pembiayaan siswa sesuai dengan subsidi yang diberikan. Errornya adalah -> " . json_encode($e->getMessage()));
        }
    }

    public function updateStatus($id_pembiayaan)
    {
        try {
            PembiayaanSiswa::where('id', $id_pembiayaan)->update(['status' => 1]);
        } catch (\Exception $e) {
            \Log::error('[REPOSITORY] PembiayaanSiswaRepository@updateStatus - Gagal memperbarui status pembiayaan siswa. Errornya adalah -> ' . json_encode($e->getMessage()));
        }
    }
}