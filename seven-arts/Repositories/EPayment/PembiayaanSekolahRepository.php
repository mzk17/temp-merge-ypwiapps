<?php

namespace SevenArts\Repositories\EPayment;

use Illuminate\Support\Facades\DB;
use SevenArts\Models\EPayment\PembiayaanSekolah;
use SevenArts\Support\Traits\EPayment\PayloadPembiayaanSekolah;

class PembiayaanSekolahRepository
{
    use PayloadPembiayaanSekolah;

    public function simpan($payload, $rincian_pembiayaan = [])
    {
        try {
            $pembiayaan_sekolah = PembiayaanSekolah::create($payload);
    
            // Jika ada rincian pembiayaan
            if ($rincian_pembiayaan['use']) {
                $pembiayaan_sekolah->rincian()->saveMany(
                    $this->payloadRincianPembiayaanSekolah($rincian_pembiayaan['item'])
                );
            }

            return $pembiayaan_sekolah;
        } catch (\Throwable $th) {
            \Log::error(
                "[REPOSITORY] PembiayaanSekolahRepository@simpan - Gagal menyimpan pembiayaan sekolah. Errornya => " . 
                json_encode($th->getMessage())
            );

            return false;
        }
    }

    public function simpanBanyak(array $payload)
    {
        try {
            $pembiayaan_sekolah = DB::connection('ypwi_epayment')->table('pembiayaan_sekolah')
                ->insert($payload);

            if($pembiayaan_sekolah)
                $ids = collect($payload)->pluck('id');

            // Update fitur untuk masa depan
            // Pembiayaan yang lebih dari satu kali bisa memiliki rincian pembiayaan

            return PembiayaanSekolah::whereIn('id', $ids)->get();
        } catch (\Throwable $th) {
            \Log::error(
                "[REPOSITORY] PembiayaanSekolahRepository@simpanBanyak - Gagal menyimpan pembiayaan sekolah. Errornya => " . 
                json_encode($th->getMessage())
            );

            return false;
        }
    }
}