<?php

namespace SevenArts\Repositories;

use App\User;
use App\Constants\JenisAkun;
use SevenArts\Models\Constant\Agama;
use SevenArts\Models\Prime\Alamat;
use SevenArts\Support\ArrayModifier;

class PersonalRepository
{
    protected $user;

    public function __construct($user_id)
    {
        $this->user = User::find($user_id);
    }

    public function dataLengkap()
    {
        /**
         * 1. Cek jenis akun
         * 2. Apabila dia siswa, maka ambil dataPesertaDidik saja
         * 3. Ambil data pegawai sesuai dengan jenisnya
         * 4. Apabila dia pegawai sekolah, maka cek apakah guru atau tendik
        */
        switch ($this->user->id_jenis_akun) {
            case JenisAkun::SISWA:
                $data_jenis_akun = $this->dataPesertaDidik();
                break;
            case JenisAkun::PEGAWAI_SEKOLAH:
                $data_jenis_akun = $this->user->dataTendik->sebagai_guru ?
                    $this->dataGuru() :
                    $this->dataTenagaPendidik();
                break;
            case JenisAkun::PEGAWAI_YAYASAN && !$this->user->dataTendik->sebagai_guru:
                $data_jenis_akun = $this->dataPegawaiYayasan();
                break;
        }

        return array_merge(
            $this->dataDiri(),
            $data_jenis_akun
        );
    }

    public function dataDiri()
    {
        // args $fields
        // Example of efficient way to fetch only necessary fields
        // $output = $this->user->dataDiri->select(['nama', 'jenis_kelamin'])->get();

        return [
            'nama' => $this->user->dataDiri->nama,
            'jenis_kelamin' => $this->user->dataDiri->jenis_kelamin === 'L' ? 'Laki - laki' : 'Perempuan',
            'tempat_lahir' => $this->user->dataDiri->tempat_lahir,
            'tanggal_lahir' => $this->user->dataDiri->tanggal_lahir,
            'agama' => $this->agama(),
            'daftar_alamat' => $this->alamat(),
            'telepon' => $this->user->dataDiri->telepon,
            'hp' => $this->user->dataDiri->hp,
            'email' => $this->user->dataDiri->email,
            'nik' => $this->user->dataDiri->nik,
            'no_kk' => $this->user->dataDiri->no_kk
        ];
    }

    public function dataPesertaDidik()
    {
        // Apabila (jenis_akun === siswa)
        if ($this->user->id_jenis_akun === JenisAkun::SISWA) {
            return ArrayModifier::renameKey(
                $this->user->dataPesertaDidik->toArray(),
                'id', // key lama
                'id_peserta_didik' // key baru
            );
        }
    }

    public function dataGuru()
    {
        // Apabila ((jenis_akun === pegawai_sekolah) DAN (tendik->sebagai_guru === true))
        if (($this->user->id_jenis_akun === JenisAkun::PEGAWAI_SEKOLAH) && ($this->user->dataTenagaPendidik->sebagai_guru)) {
            return ArrayModifier::renameKey(
                $this->user->dataTenagaPendidik->toArray(),
                'id', // key lama
                'id_guru' // key baru
            );
        }
    }

    public function dataTenagaPendidik()
    {
        // Apabila ((jenis_akun === pegawai_sekolah) DAN (tendik->sebagai_guru === false))
        if (($this->user->id_jenis_akun === JenisAkun::PEGAWAI_SEKOLAH) && (!$this->user->dataTenagaPendidik->sebagai_guru)) {
            return ArrayModifier::renameKey(
                $this->user->dataTenagaPendidik->toArray(),
                'id', // key lama
                'id_tenaga_pendidik' // key baru
            );
        }
    }

    public function dataPegawaiYayasan()
    {
        // Apabila (jenis_akun === pegawai_yayasan)
        if ($this->user->id_jenis_akun === JenisAkun::PEGAWAI_SEKOLAH) {
            return ArrayModifier::renameKey(
                $this->user->dataPegawaiYayasan->toArray(),
                'id', // key lama
                'id_pegawai_yayasan' // key baru
            );
        }
    }

    protected function alamat()
    {
        return $this->user->dataDiri->daftar_alamat;
    }

    protected function agama()
    {
        return Agama::select('nama')->find($this->user->dataDiri->agama)->nama;
    }
}
