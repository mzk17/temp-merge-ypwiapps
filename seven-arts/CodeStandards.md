# Class Constants, Properties, dan Methods

## Constants

Sama seperti PSR-1

## Properties

Nama variabel lokal pada sebuah method menggunakan `camelCase`.
Nama variabel global pada sebuah \_\_constructor class menggunakan `under_score`.

## Methods

Sama seperti PSR-1
