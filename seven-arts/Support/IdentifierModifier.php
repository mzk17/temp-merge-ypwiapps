<?php

namespace SevenArts\Support;

use Illuminate\Support\Facades\Schema;

trait IdentifierModifier
{
    protected $baseModels = [
        'SevenArts\Models\Base\YPWIPrime'
    ];

    public function autoIncrementStaticID($id_word, $model)
    {
        $prefix = '';

        // Cek apabila model tidak berasal dari BaseModel
        if (! in_array(get_parent_class($model), $this->baseModels)) {
            return "BASE MODEL SALAH";
        }

        // Ambil ID terakhir dari model
        $last = $model::orderBy('created_at', 'desc')->first();
        
        // First model
        if (is_null($last) || empty($last)) {
            return $id_word . "0001";
        }

        /**
         * Ambil angka ID - explode($id_word, $last->id)
         * Casting angka ID dari tipe string menjadi integer (number)
         * Tambahkan 1
         */
        $newID = (int)explode($id_word, $last->id)[1] + 1;

        if (strlen($newID) < 4) {
            for ($i = 0; $i < (4 - strlen($newID)); $i++) {
                $prefix .= "0";
            }
            $newID = $prefix . $newID;
        }

        return $id_word . $newID;
    }
}
