<?php

namespace SevenArts\Support;

class ArrayModifier
{
    public static function renameKey($array, $old_key, $new_key)
    {
        $array[$new_key] = $array[$old_key];
        unset($array[$old_key]);
    
        return $array;
    }
}
