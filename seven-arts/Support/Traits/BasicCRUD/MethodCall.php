<?php

namespace SevenArts\Support\Traits\BasicCRUD;

trait MethodCall
{
    /**
     * Method yang berfungsi untuk menjalankan method tertentu pada sebuah class.
     */
    public function sideActionExecution($method_name, $method_owner, $arg = null)
    {
        if (method_exists($method_owner, $method_name)) {
            return $method_owner->{$method_name}($arg);
        }
    }

    /**
     * Method untuk menambahkan properti pada response setiap action yg dituju.
     * Caranya:
     * Di Child Controller buat method dengan format
     * [action_name]Response
     * lalu isinya cuma mengembalikan nilai dari properti tambahan tersebut berbentuk array 1D.
     */
    protected function populateResponses($action_name, $controller, $models = null)
    {
        $responses = $this->sideActionExecution(substr($action_name, 6) . 'Response', $controller, $models);

        if ($responses) {
            foreach ($responses as $property => $value) {
                $this->payload_response->{$property} = $value;
            }
        }
    }
}
