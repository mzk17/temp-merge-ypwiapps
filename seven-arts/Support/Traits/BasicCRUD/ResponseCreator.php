<?php

namespace SevenArts\Support\Traits\BasicCRUD;

trait ResponseCreator
{
    protected $payload_response;
    /**
     * Message responses
     */
    protected function showMessage(String $type)
    {
        return ($type === 'gagal')
            ? "Data {$this->entity_name} tidak ditemukan."
            : "";
    }

    protected function storeMessage(String $type)
    {
        return ($type === 'gagal')
            ? "Terjadi kesalahan, penambahan {$this->entity_name} baru gagal."
            : "Berhasil menambahkan {$this->entity_name}.";
    }

    protected function updateMessage(String $type)
    {
        return ($type === 'gagal')
            ? "Terjadi kesalahan, pada pembaruan data {$this->entity_name} gagal."
            : "Berhasil memperbarui data {$this->entity_name}.";
    }

    protected function destroyMessage(String $type)
    {
        return ($type === 'gagal')
            ? "Data {$this->entity_name} tidak ditemukan."
            : "Berhasil menghapus data {$this->entity_name}";
    }

    /**
     * AJAX Responses
     */
    protected function generatePayload()
    {
        $this->payload_response = new \stdClass();
        $this->payload_response->redirect    = false;
        $this->payload_response->redirect_to = '';
        $this->payload_response->message     = '';
        $this->payload_response->models      = null;
    }

    protected function commonJson($statusCode = null)
    {
        return (is_null($statusCode))
            ? response()->json($this->payload_response)
            : response()->json($this->payload_response, $statusCode);
    }
}
