<?php

namespace SevenArts\Support\Traits\BasicCRUD;

use SevenArts\Support\Handlers\ErrorHandler;

trait DefineData
{
    /**
     * Validation and define processed data
     *
     * @param  object  $payload
     * @return void
     */
    protected function defineData(&$payload)
    {
        // Jika ada validasi gagal
        if ($payload->needValidated && ($payload->data->fails() || ErrorHandler::sideActionFail())) {
            ErrorHandler::multipleActionFormValidation($payload->data);
            // Redirect kembali jika validasi gagal
            return $this->redirectWhenValidationFail();
        }
        // Deklarasikan data jika tidak ada masalah
        $payload->data = ($payload->needValidated)
            ? $payload->data->valid()
            : $payload->data;
    }

    /**
     * Definisikan nama entitas
     * 
     * @return void
     */
    protected function defineEntityName($model)
    {
        $this->entity_name = ($model::ENTITY_NAME) ?: get_class($model);
    }

    /**
     * Redirect ke halaman sebelumnya jika validasi gagal
     * 
     * @return \Illuminate\Http\RedirectResponse
     */
    private function redirectWhenValidationFail()
    {
        // Ambil semua error yang dikumpulkan dalam session
        $errors = session()->get('validation_error');

        // Log
        \Log::error("DefineData@redirectWhenValidationFail: Error ". request()->get('action_name') ." $this->entity_name " . $errors);

        if (request()->ajax()) {
            $this->payload_response->message = $errors->toArray();
            return $this->commonJson(422);
        }

        return redirect()
            ->back()
            ->withInput()
            ->withErrors($errors);
    }
}
