<?php

namespace SevenArts\Support\Traits\BasicCRUD;

use Illuminate\Support\Facades\Schema;

trait FieldsFilter
{
    /**
     * Take necessary /needed fields from
     * an array or request.
     *
     * @param class $model
     * @param array $exlcuded_fields - this is additonal excluded fields
     */
    protected function getNecessaryFieldsFrom($model, $additional_excluded_fields = [])
    {
        $table           = new $model;
        $table_name      = $table->getTable();
        $connection_name = $table->getConnectionName();
        $excluded_fields = ['created_at', 'updated_at', 'deleted_at'];

        if (count($additional_excluded_fields) > 0) {
            $excluded_fields = array_merge($excluded_fields, $additional_excluded_fields);
        }

        $get_table_columns = collect(Schema::connection($connection_name)->getColumnListing($table_name));
        $table_columns     = $get_table_columns->filter(function ($value, $key) use ($excluded_fields) {
            return !in_array($value, $excluded_fields, true);
        });

        return $table_columns->all();
    }
}
