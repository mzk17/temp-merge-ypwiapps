<?php

namespace SevenArts\Support\Traits\EPayment;

use SevenArts\Models\EPayment\PembiayaanSekolah;
// use SevenArts\Models\EPayment\PembiayaanSiswa;
use SevenArts\Models\Prime\PesertaDidik;

trait PayloadPembiayaanSiswa
{
    public function payloadPembiayaanSiswa(PesertaDidik $siswa, PembiayaanSekolah $pembiayaan_sekolah)
    {
        $payload_pembiayaan_siswa = [
            'id' => \Str::uuid(),
            "id_pembiayaan_sekolah" => $pembiayaan_sekolah->id,
            // 'id_subsidi' => ada pengecekan subsidi nantinya,
            "id_peserta_didik" => $siswa->id,
            "biaya" => $pembiayaan_sekolah->biaya_dasar,
            "status" => 0 // 0: Belum lunas, 1: Sudah lunas
        ];

        return $payload_pembiayaan_siswa;
    }
}