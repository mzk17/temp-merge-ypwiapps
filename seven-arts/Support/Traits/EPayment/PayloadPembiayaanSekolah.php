<?php

namespace SevenArts\Support\Traits\EPayment;

use SevenArts\Models\EPayment\RincianPembiayaan;

/**
 * 
 */
trait PayloadPembiayaanSekolah
{
    protected function payloadPembiayaanSekolah($request, $is_array = false, $key = null)
    {
        $payload_pembiayaan_sekolah = [
            'id_pembiayaan'     => $request->id_pembiayaan,
            'id_sekolah'        => $request->id_sekolah,
            'tahun_ajaran'      => $request->tahun_ajaran,
            'biaya_dasar'       => $request->biaya_dasar,
            'nama_pembiayaan'   => ($is_array) ? $request->nama_pembiayaan[$key] : $request->nama_pembiayaan,
            'kelas'             => implode(",",$request->sasaran_peserta_didik),
            'suffix'            => $request->suffix
        ];

        if($is_array)
            $payload_pembiayaan_sekolah['id'] = \Str::uuid();

        if($payload_pembiayaan_sekolah['kelas'] === "")
            unset($payload_pembiayaan_sekolah['kelas']);

        if($is_array) {
            $now = \Carbon\Carbon::now()->toDateTimeString();
            $payload_pembiayaan_sekolah['created_at'] = $now;
            $payload_pembiayaan_sekolah['updated_at'] = $now;
        }
        \Log::info("[TRAITS] PayloadPembiayaanSekolah@payloadPembiayaanSekolah - Hasil payloadnya adalah " . json_encode($payload_pembiayaan_sekolah));

        return $payload_pembiayaan_sekolah;
    }

    protected function payloadRincianPembiayaanSekolah($rincian_pembiayaan)
    {
        $output = [];
        foreach ($rincian_pembiayaan as $rincian) {
            array_push($output, new RincianPembiayaan([
                'nama_item' => $rincian['nama_item'],
                'biaya' => $rincian['biaya']
            ]));
        }

        return $output;
    }
}
