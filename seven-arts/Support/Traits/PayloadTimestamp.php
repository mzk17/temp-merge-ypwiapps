<?php

namespace SevenArts\Support\Traits;

use Carbon\Carbon;

trait PayloadTimestamp
{
    private function addTimestamps(array &$payload)
    {
        $payload['created_at'] = Carbon::now();
        $payload['updated_at'] = Carbon::now();
    }
}