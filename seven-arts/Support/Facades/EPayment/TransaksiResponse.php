<?php

namespace SevenArts\Support\Facades\EPayment;

class TransaksiResponse
{
    public static function tampilkan($transaksi, $tampilkan_info_siswa = false)
    {
        $base_response = self::base($transaksi);

        // Jika transaksi memiliki lebih dari satu item
        if (!is_null($transaksi->item)) {
            $base_response['item'] = self::tampilkanRincianTransaksi($transaksi->item);
        }

        // Jika detail transaksi ingin dilihat bukan dari halaman siswa
        if ($tampilkan_info_siswa) {
            $base_response['nama_siswa'] = $transaksi->siswa->dataPribadi->nama;
            $base_response['kelas']      = $transaksi->siswa->kelasAktif();
        }

        return $base_response;
    }

    private static function base($transaksi)
    {
        return [
            'id' => $transaksi->id,
            'nama_transaksi' => $transaksi->nama_transaksi,
            'total' => $transaksi->total,
            'catatan' => $transaksi->catatan,
            'waktu_transaksi' => $transaksi->created_at
        ];
    }

    private static function tampilkanRincianTransaksi($item)
    {
        $output = [];
        foreach ($item as $rincian) {
            array_push($output, [
                'nama' => $rincian['nama_pembiayaan'],
                'bayar' => $rincian['bayar'],
                'status' => $rincian->sudahLunas()
            ]);
        }

        return $output;
    }
}