<?php

namespace SevenArts\Support\Facades\EPayment;

use SevenArts\Repositories\EPayment\PembiayaanSiswaRepository;

class PembiayaanResponse
{
    public static function pembiayaanBelumLunas($id_peserta_didik)
    {
        $output = [];
        $daftar_pembiayaan = (new PembiayaanSiswaRepository())->pembiayaanBelumLunas($id_peserta_didik);

        foreach($daftar_pembiayaan as $pembiayaan_siswa) {
            array_push($output, [
                'id' => $pembiayaan_siswa->id,
                'nama_pembiayaan' => $pembiayaan_siswa->pembiayaan->nama_pembiayaan,
                'rincian' => $pembiayaan_siswa->rincianPembiayaan($pembiayaan_siswa->id),
                'biaya' => $pembiayaan_siswa->biaya, // segera akan di-update jika fungsi subsidi sudah ada
                'sisa' => $pembiayaan_siswa->sisa(),
            ]);
        }

        return $output;
    }
}