<?php

namespace SevenArts\Support\Facades\EPayment;

use SevenArts\Support\Facades\CommonPayload;

class SubsidiRequestor
{
    public static function earlyPhasePayload(array $request)
    {
        $payload = [];
        foreach($request as $subsidi) {
            $current_payload = [
                'id_referensi_subsidi'        => \Str::uuid(),
                'id_pembiayaan_siswa'         => $subsidi['id_pembiayaan_siswa'],
                'id_rincian_pembiayaan'       => $subsidi['id_rincian_pembiayaan'],
                'id_peserta_didik'            => $subsidi['id_peserta_didik'],
                'jumlah_subsidi'              => $subsidi['jumlah_subsidi'],
                'biaya_setelah_subsidi'       => $subsidi['biaya_setelah_subsidi'],
                'total_biaya_setelah_subsidi' => $subsidi['total_biaya_setelah_subsidi'],
                'catatan'                     => $subsidi['catatan']
            ];
            CommonPayload::addTimestamps($current_payload);

            array_push($payload, $current_payload);
        }

        return $payload;
    }
}