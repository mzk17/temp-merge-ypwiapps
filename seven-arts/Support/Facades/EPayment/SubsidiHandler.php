<?php

namespace SevenArts\Support\Facades\EPayment;

use SevenArts\Models\Prime\Sekolah;

class SubsidiHandler
{
    public static function cekSubsidi()
    {
        
    }

    public static function penerimaSubsidiTahunLalu($id_sekolah)
    {
        return Sekolah::find($id_sekolah)->daftarSiswa();
    }
}