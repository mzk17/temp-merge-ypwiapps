<?php

namespace SevenArts\Support\Facades;

use Carbon\Carbon;

class CommonPayload
{
    public static function uuid()
    {
        return \Str::uuid();
    }

    public static function addTimestamps(array &$payload)
    {
        $payload['created_at'] = Carbon::now();
        $payload['updated_at'] = Carbon::now();
    }
}