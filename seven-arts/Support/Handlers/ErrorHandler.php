<?php

namespace SevenArts\Support\Handlers;

class ErrorHandler
{
    public static function multipleActionFormValidation($validator)
    {
        // Simpan error validasi di session (flash karena hanya dipakai sekali)
        if(session()->has('validation_error')) {
            $errors = session()->get('validation_error');
            $current_errors = $validator->errors()->toArray();

            foreach ($current_errors as $key => $message) {
                $errors->add($key, $message);
            }
        } else {
            session()->flash('validation_error', $validator->errors());
        }
    }

    public static function sideActionFail()
    {
        return session()->has('validation_error');
    }
}