<?php

namespace SevenArts\Support\Handlers\BasicCRUD;

use Illuminate\Http\RedirectResponse;
use SevenArts\Contracts\BasicResourceMethods;

class AjaxHandler extends BaseHandler implements BasicResourceMethods
{
    protected $entity_name;

    protected $payload_response;

    public function __construct()
    {
        $this->generatePayload();
    }

    public function actionShow($payload)
    {
        $this->defineEntityName($payload->model);

        $this->payload_response->models = $payload->repository->find($payload->id_model);

        $this->populateResponses(__FUNCTION__, $payload->controller, $this->payload_response->models);

        // If no data
        if (!$this->payload_response->models || $this->payload_response->models instanceof \Exception) {
            $this->payload_response->message = $this->showMessage('gagal');

            return $this->commonJson(500);
        }

        return $this->commonJson();
    }

    public function actionStore($payload)
    {
        $this->defineEntityName($payload->model);

        // Define and validation data
        if ($this->defineData($payload) instanceof \Illuminate\Http\JsonResponse) {
            return $this->defineData($payload);
        }

        // Action!
        $model = $payload->repository->create($payload->data);

        // If creating fail
        if (!$model || $model instanceof \Exception) {
            $this->payload_response->message = $this->storeMessage('gagal');

            return $this->commonJson(500);
        }

        // Jalankan fungsi 'afterStore' jika ada pada controller
        $this->sideActionExecution('afterStore', $payload->controller, $model);

        $this->payload_response->message = $this->storeMessage('berhasil');
        $this->payload_response->models = $model;

        $this->populateResponses(__FUNCTION__, $payload->controller, $model);

        return $this->commonJson();
    }

    public function actionUpdate($payload, $id)
    {
        $this->defineEntityName($payload->model);

        // Validation, if needed
        if ($this->defineData($payload) instanceof RedirectResponse) {
            return $this->defineData($payload);
        }

        // Action!
        $model = $payload->repository->update($id, $payload->data);

        // If creating fail
        if (!$model) {
            $this->payload_response->message = $this->updateMessage('gagal');

            return $this->commonJson(500);
        }

        // Jalankan fungsi 'afterUpdate' jika ada pada controller
        $this->sideActionExecution('afterUpdate', $payload->controller, $model);

        $this->payload_response->message = $this->updateMessage('berhasil');
        $this->payload_response->models = $model;

        $this->populateResponses(__FUNCTION__, $payload->controller, $model);

        return $this->commonJson();
    }

    public function actionDestroy($payload, $id)
    {
        $this->defineEntityName($payload->model);

        $destroy = $payload->repository->destroy($id);

        if ($destroy['result']) {
            // Jalankan fungsi 'afterDestroy' jika ada pada controller
            $this->sideActionExecution('afterDestroy', $payload->controller);

            $this->payload_response->message = $this->destroyMessage('berhasil');

            $this->populateResponses(__FUNCTION__, $payload->controller, $destroy['entity']);

            return $this->commonJson();
        }
        $this->payload_response->message = $this->destroyMessage('gagal');

        return $this->commonJson(500);
    }
}
