<?php

namespace SevenArts\Support\Handlers\BasicCRUD;

use Illuminate\Http\RedirectResponse;
use SevenArts\Support\Traits\BasicCRUD\DefineData;
use SevenArts\Support\Traits\BasicCRUD\MethodCall;
use SevenArts\Support\Traits\BasicCRUD\ResponseCreator;

class BaseHandler
{
    use DefineData, MethodCall, ResponseCreator;
}
