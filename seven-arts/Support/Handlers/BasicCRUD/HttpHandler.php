<?php

namespace SevenArts\Support\Handlers\BasicCRUD;

use Illuminate\Http\RedirectResponse;
use SevenArts\Contracts\BasicResourceMethods;

class HttpHandler extends BaseHandler implements BasicResourceMethods
{
    /**
     * Nama entitas model yang sedang eksekusi
     *
     * @var string
     */
    protected $entity_name;

    /**
     * Show an information from a record
     *
     * @param object $payload
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function actionShow($payload)
    {
        $this->defineEntityName($payload->model);

        $model = $payload->repository->find($payload->id_model);

        if (!$model) {
            return $this->actionFail('showMessage');
        }

        return view($payload->show_view, compact('model'));
    }

    /**
     * Create a new record of a model
     *
     * @param  object  $payload
     * @return \Illuminate\Http\RedirectResponse
     */
    public function actionStore($payload)
    {
        $this->defineEntityName($payload->model);

        // Define and validation data
        if ($this->defineData($payload) instanceof RedirectResponse) {
            return $this->defineData($payload);
        }

        // Action!
        $model = $payload->repository->create($payload->data);

        // If creating fail
        if (!$model) {
            return $this->actionFail('storeMessage');
        }

        // Jalankan fungsi 'afterStore' jika ada pada controller
        if ($this->sideActionExecution('afterStore', $payload->controller, $model) instanceof RedirectResponse)
            return $this->sideActionExecution('afterStore', $payload->controller, $model);

        return redirect()->route($payload->next_route)
            ->with('berhasil', $this->storeMessage('berhasil'));
    }

    /**
     * Update an existing record of a model
     *
     * @param  object  $payload
     * @return \Illuminate\Http\RedirectResponse
     */
    public function actionUpdate($payload, $id)
    {
        $this->defineEntityName($payload->model);

        // Define and validation data
        if ($this->defineData($payload) instanceof RedirectResponse) {
            return $this->defineData($payload);
        }

        // Action!
        $model = $payload->repository->update($id, $payload->data);

        // If updating fail
        if (!$model) {
            return $this->actionFail('updateMessage');
        }

        // Jalankan fungsi 'afterUpdate' jika ada pada controller
        if ($this->sideActionExecution('afterUpdate', $payload->controller, $model) instanceof RedirectResponse)
            return $this->sideActionExecution('afterUpdate', $payload->controller, $model);

        return redirect()->route($payload->next_route)
            ->with('berhasil', $this->updateMessage('berhasil'));
    }

    /**
     * Remove a record of a model
     *
     * @param  object  $payload
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function actionDestroy($payload, $id)
    {
        $this->defineEntityName($payload->model);

        $destroy = $payload->repository->destroy($id);
        if ($destroy['result']) {
            // Jalankan fungsi 'afterDestroy' jika ada pada controller
            $this->sideActionExecution('afterDestroy', $payload->controller);

            return redirect()->route($payload->next_route)->with('berhasil', $this->destroyMessage('berhasil'));
        }
        return $this->actionFail('destroyMessage');
    }

    /**
     * Redirection if action failed
     *
     * @param  string  $method_name
     * @return \Illuminate\Http\RedirectResponse;
     */
    protected function actionFail($method_name)
    {
        return redirect()
            ->back()
            ->withInput()
            ->withErrors(['gagal' => $this->{$method_name}('gagal')]);
    }
}
