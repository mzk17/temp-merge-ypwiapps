<?php

namespace SevenArts\Models\Constant;

use SevenArts\Models\Base\YPWIConstant;
use SevenArts\Models\Prime\JabatanAkun;

class Jabatan extends YPWIConstant
{
    protected $table = 'jabatan';

    // public function akunPengguna()
    // {
    //     return $this->belongsToMany('App\User', 'jabatan_akun', 'id_jabatan', 'id_akun_pengguna');
    // }

    public function getAkunPenggunaAttribute()
    {
        $jabatan_akun = JabatanAkun::where('id_jabatan', $this->id)->first();
        return $jabatan_akun->id_akun_pengguna;
    }
}
