<?php

namespace SevenArts\Models\Constant;

use SevenArts\Models\Base\YPWIConstant;

class Agama extends YPWIConstant
{
    protected $table = 'agama';
    
    protected $guarded = [];

    public function person()
    {
        return $this->hasMany('SevenArts\Model\Prime\DataPribadi', 'agama', 'id');
    }
}
