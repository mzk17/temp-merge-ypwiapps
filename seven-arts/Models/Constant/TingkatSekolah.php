<?php

namespace SevenArts\Models\Constant;

use SevenArts\Models\Base\YPWIConstant;

class TingkatSekolah extends YPWIConstant
{
    protected $table = 'tingkat_sekolah';
    
    protected $guarded = [];

    public function sekolah()
    {
        return $this->hasMany('SevenArts\Models\Prime\Sekolah', 'id_tingkat_sekolah', 'id');
    }
}
