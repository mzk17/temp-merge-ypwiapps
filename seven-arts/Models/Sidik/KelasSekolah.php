<?php

namespace SevenArts\Models\Sidik;

use SevenArts\Models\Base\YPWISidik;

class KelasSekolah extends YPWISidik
{
    const ENTITY_NAME = 'Kelas';

    protected $table = 'kelas_sekolah';

    public function sekolah()
    {
        return $this->belongsTo('SevenArts\Models\Prime\Sekolah', 'id_sekolah');
    }

    public function siswa()
    {
        return $this->belongsToMany('SevenArts\Models\Prime\PesertaDidik', 'ypwi_sidik.kelas_siswa', 'id_kelas', 'id_siswa');
    }
}
