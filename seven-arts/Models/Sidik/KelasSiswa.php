<?php

/**
 * Tabel intermediate
 * Relasi antara siswa dan kelas
 */
namespace SevenArts\Models\Sidik;

use SevenArts\Models\Base\YPWISidik;

class KelasSiswa extends YPWISidik
{
    protected $table = 'kelas_siswa';
}
