<?php

namespace SevenArts\Models\Sidik;

use SevenArts\Models\Base\YPWISidik;

class TahunAjaran extends YPWISidik
{
    const ENTITY_NAME = "Tahun Ajaran";

    protected $table = "tahun_ajaran";
}
