<?php

namespace SevenArts\Models\Prime;

use Illuminate\Database\Eloquent\Relations\Pivot;

class SekolahTendik extends Pivot
{
    protected $table = 'sekolah_tendik';
}
