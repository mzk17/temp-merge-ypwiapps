<?php

namespace SevenArts\Models\Prime;

use SevenArts\Models\Base\YPWIPrime;

class JenisAkun extends YPWIPrime
{
    const ENTITY_NAME = 'Jenis Akun';

    protected $table = 'jenis_akun';

    /**
     * Relasi kepada akun pengguna
     */
    public function akunPengguna()
    {
        return $this->hasMany('App\User', 'id_jenis_akun');
    }
}
