<?php

namespace SevenArts\Models\Prime;

use SevenArts\Models\Base\YPWIPrime;

class AdministrasiWilayah extends YPWIPrime
{
    const ENTITY_NAME = 'Administrasi Wilayah';

    protected $table = 'administrasi_wilayah';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'code';

    /**
     * Indicates if the primary keys are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The "type" of the primary key.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Local scope untuk menampilkan daftar Provinsi
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeProvinceList($query)
    {
        return $query->whereRaw('CHAR_LENGTH(code) <= 2');
    }

    /**
     * Local scope untuk menampilkan Kabupaten / Kota
     * berdasarkan provinsi yang telah dipilih user
     *
     * @param  string  $province_id
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCityList($query, $province_id)
    {
        return $this->admRegionChainingQueries($query, $province_id, 5);
    }

    /**
     * Local scope untuk menampilkan Kecamatan
     * berdasarkan kabupaten / kota yang telah dipilih user
     *
     * @param  string  $city_id
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDistrictList($query, $city_id)
    {
        return $this->admRegionChainingQueries($query, $city_id, 8);
    }

    /**
     * Local scope untuk menampilkan Kelurahan
     * berdasarkan kecamatan yang telah dipilih user
     *
     * @param  string  $district_id
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSubDistrictList($query, $district_id)
    {
        return $query->whereRaw("code LIKE '{$district_id}%'");
    }

    /**
     * Fungsi untuk menampilkan wilayah administrasi
     * berdasarkan tingkat wilayah yang telah dipilih user sebelumnya.
     * Parameter ketiga `$num` merupakan jumlah string pada kolom kode
     * wilayah administrasi yang telah dipilih.
     *
     * @param  string  $province_id
     * @param  int  $num
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function admRegionChainingQueries($query, $location_id, $num)
    {
        return $query->whereRaw("code LIKE '{$location_id}%'")
            ->whereRaw("CHAR_LENGTH(code) = {$num}");
    }
}
