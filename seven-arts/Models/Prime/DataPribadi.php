<?php

namespace SevenArts\Models\Prime;

use SevenArts\Models\Base\YPWIPrime;

class DataPribadi extends YPWIPrime
{
    const ENTITY_NAME = 'Data Personal';

    protected $table = 'data_pribadi';
    
    protected $guarded = [];
    
    protected $keyType = 'string';
    
    public $incrementing = false;

    protected $casts = [
        'id' => 'string'
    ];

    public function akun()
    {
        return $this->hasOne('App\User', 'id_data_pribadi');
    }

    public function karyawan()
    {
        return $this->hasOne('SevenArts\Models\Prime\Karyawan', 'id_data_pribadi');
    }

    public function tenagaPendidik()
    {
        return $this->hasOne('SevenArts\Models\Prime\TenagaPendidik', 'id_data_pribadi');
    }

    public function pesertaDidik()
    {
        return $this->hasOne('SevenArts\Models\Prime\PesertaDidik', 'id_data_pribadi');
    }

    public function getDaftarAlamatAttribute()
    {
        return [
            'alamat_utama'  => $this->alamatUtama,
            'alamat_kedua'  => $this->alamatKedua,
            'alamat_ketiga' => $this->alamatKetiga,
        ];
    }

    public function agama()
    {
        return $this->belongsTo('SevenArts\Models\Constant\Agama', 'agama');
    }

    public function alamatUtama()
    {
        return $this->belongsTo('SevenArts\Models\Prime\Alamat', 'alamat_1');
    }

    public function alamatKedua()
    {
        return $this->belongsTo('SevenArts\Models\Prime\Alamat', 'alamat_2');
    }

    public function alamatKetiga()
    {
        return $this->belongsTo('SevenArts\Models\Prime\Alamat', 'alamat_3');
    }
}
