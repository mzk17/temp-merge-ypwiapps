<?php

namespace SevenArts\Models\Prime;

use App\Events\Prime\TenagaPendidikDeleted;
use SevenArts\Models\Base\YPWIPrime;

class TenagaPendidik extends YPWIPrime
{
    const ENTITY_NAME = 'Tenaga Pendidik';

    protected $table = 'tendik';

    protected $guarded = [];

    public $incrementing = false;

    protected $keyType = 'string';

    protected $casts = [
        'id' => 'string'
    ];

    protected $dispatchesEvents = [
        'deleted' => TenagaPendidikDeleted::class
    ];

    /**
     * Relationships
     */
    public function akunPengguna()
    {
        return $this->hasOne('App\User', 'id_data_jenis_akun');
    }

    public function dataPribadi()
    {
        return $this->belongsTo('SevenArts\Models\Prime\DataPribadi', 'id_data_pribadi');
    }

    public function sekolah()
    {
        return $this->belongsToMany('SevenArts\Models\Prime\Sekolah', 'sekolah_tendik', 'id_tendik', 'id_sekolah')
            ->withTimestamps();
    }

    public function sebagaiGuru()
    {
        return $this->sebagai_guru;
    }
}
