<?php

namespace SevenArts\Models\Prime;

use Illuminate\Database\Eloquent\Relations\Pivot;

class JabatanAkun extends Pivot
{
    const ENTITY_NAME = 'Jabatan Akun';

    protected $table = 'jabatan_akun';
}
