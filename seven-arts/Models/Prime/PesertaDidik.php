<?php

namespace SevenArts\Models\Prime;

use App\Events\Prime\PesertaDidikDeleted;
use SevenArts\Models\Base\YPWIPrime;

class PesertaDidik extends YPWIPrime
{
    const ENTITY_NAME = 'Peserta Didik';

    protected $table = 'peserta_didik';

    protected $guarded = [];

    public $incrementing = false;

    protected $keyType = 'string';

    protected $casts = [
        'id' => 'string'
    ];

    protected $dispatchesEvents = [
        'deleted' => PesertaDidikDeleted::class
    ];

    public function akunPengguna()
    {
        return $this->hasOne('App\User', 'id_data_jenis_akun');
    }

    public function dataPribadi()
    {
        return $this->belongsTo('SevenArts\Models\Prime\DataPribadi', 'id_data_pribadi');
    }

    public function pembiayaan()
    {
        return $this->hasMany('SevenArts\Models\EPayment\PembiayaanSiswa', 'id_peserta_didik', 'id');
    }
    
    public function kelas()
    {
        return $this->belongsToMany('SevenArts\Models\Sidik\KelasSekolah', 'ypwi_sidik.kelas_siswa', 'id_siswa', 'id_kelas');
    }

    /**
     * Custom Scopes
     */
    public function scopeKelasAktif()
    {
        return $this->kelas()->where('status', 1)->select('nama_kelas')->first()->nama_kelas;
    }
}
