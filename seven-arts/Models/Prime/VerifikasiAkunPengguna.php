<?php

namespace SevenArts\Models\Prime;

use SevenArts\Models\Base\YPWIPrime;

class VerifikasiAkunPengguna extends YPWIPrime
{
    protected $table = 'verifikasi_akun_pengguna';

    protected $guarded = [];

    public function akunPengguna()
    {
        return $this->belongsTo('App\User', 'id_akun_pengguna');
    }
}
