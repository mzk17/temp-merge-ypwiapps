<?php

namespace SevenArts\Models\Prime;

use App\Events\Prime\LembagaDeleted;
use SevenArts\Models\Base\YPWIPrime;

class Lembaga extends YPWIPrime
{
    const ENTITY_NAME = 'Lembaga';

    protected $table = 'lembaga';

    protected $guarded = [];

    protected $keyType = 'string';

    public $incrementing = false;

    protected $casts = [
        'id' => 'string',
    ];

    protected $dispatchesEvents = [
        'deleted' => LembagaDeleted::class
    ];

    public function jabatan()
    {
        return $this->hasMany('SevenArts\Models\JabatanAkun', 'id_lembaga', 'id');
    }

    public function alamat()
    {
        return $this->belongsTo('SevenArts\Models\Prime\Alamat', 'id_alamat');
    }
}
