<?php

namespace SevenArts\Models\Prime;

use SevenArts\Models\Base\YPWIPrime;

class Karyawan extends YPWIPrime
{
    const ENTITY_NAME = 'Karyawan';

    protected $table = 'karyawan';

    protected $guarded = [];

    public $incrementing = false;

    protected $keyType = 'string';

    protected $casts = [
        'id' => 'string'
    ];

    public function akunPengguna()
    {
        return $this->hasOne('App\User', 'id_data_jenis_akun');
    }

    public function dataPribadi()
    {
        return $this->belongsTo('SevenArts\Models\Prime\DataPribadi', 'id_data_pribadi');
    }
}
