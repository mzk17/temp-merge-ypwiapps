<?php

namespace SevenArts\Models\Prime;

use App\Events\Prime\SekolahDeleted;
use SevenArts\Models\Base\YPWIPrime;

class Sekolah extends YPWIPrime
{
    const ENTITY_NAME = 'Sekolah';

    protected $table = 'sekolah';

    protected $guarded = [];

    protected $casts = [
        'id' => 'string'
    ];

    protected $keyType = 'string';

    public $incrementing = false;

    protected $appends = [
        'siswa'
    ];

    protected $dispatchesEvents = [
        'deleted' => SekolahDeleted::class
    ];

    /**
     * Relasi - Prime
     */
    public function tingkatan()
    {
        return $this->belongsTo('SevenArts\Models\Constant\TingkatSekolah', 'id_tingkat_sekolah', 'id');
    }

    public function alamat()
    {
        return $this->belongsTo('SevenArts\Models\Prime\Alamat', 'id_alamat');
    }

    public function tenagaPendidik()
    {
        return $this->belongsToMany('SevenArts\Models\Prime\TenagaPendidik', 'sekolah_tendik', 'id_sekolah', 'id_tendik')
            ->withTimestamps();
    }

    /**
     * Relasi - Aset dan Inventaris
     */
    public function alatTransaksi()
    {
        return $this->hasMany('SevenArts\Models\AssetInventory\AlatTransaksi', 'sekolah_id');
    }

    /**
     * Relasi - Sistem Informasi Pendidikan
     */
    public function kelas()
    {
        return $this->hasMany('SevenArts\Models\Sidik\KelasSekolah', 'id_sekolah');
    }

    public function getSiswaAttribute()
    {
        $output = [];
        foreach ($this->kelas as $kelas) {
            if(!is_null($kelas->siswa)) {
                foreach ($kelas->siswa as $siswa) {
                    array_push($output, $siswa);
                }
            }
        }

        return $output;
    }

    /**
     * Custom Scopes
     */
    public function scopeDaftarSiswa()
    {
        $output = [];
        foreach ($this->kelas as $kelas) {
            if(!is_null($kelas->siswa)) {
                foreach ($kelas->siswa as $siswa) {
                    array_push($output, [
                        'id' => $siswa->id,
                        'nisn' => $siswa->nisn,
                        'nama' => $siswa->dataPribadi->nama,
                        'kelas' => $siswa->kelasAktif()
                    ]);
                }
            }
        }

        return $output;
    }
}
