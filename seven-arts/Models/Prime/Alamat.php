<?php

namespace SevenArts\Models\Prime;

use SevenArts\Models\Base\YPWIPrime;

class Alamat extends YPWIPrime
{
    const ENTITY_NAME = 'Alamat';

    protected $table = 'alamat';

    public $incrementing = false;

    protected $keyType = 'string';

    protected $guarded = [];

    protected $casts = [
        'id' => 'string'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /**
     * Relasi
     */
    public function alamatUtama()
    {
        return $this->hasOne('SevenArts\Models\DataPribadi', 'alamat_1', 'id');
    }

    public function alamatKedua()
    {
        return $this->hasOne('SevenArts\Models\DataPribadi', 'alamat_2', 'id');
    }

    public function alamatKetiga()
    {
        return $this->hasOne('SevenArts\Models\DataPribadi', 'alamat_3', 'id');
    }

    public function sekolah()
    {
        return $this->hasOne('SevenArts\Models\Prime\Sekolah', 'id_alamat');
    }

    public function lembaga()
    {
        return $this->hasOne('SevenArts\Models\Prime\Lembaga', 'id_alamat');
    }

    /**
     * Accessor
     */
    public function getAlamatUtamaAttribute($value)
    {
        return ($value > 0) ? true : false;
    }
}
