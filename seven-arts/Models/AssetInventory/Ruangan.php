<?php

namespace SevenArts\Models\AssetInventory;

use SevenArts\Models\Base\YPWIAssetInventory;

class Ruangan extends YPWIAssetInventory
{
    const ENTITY_NAME = 'Ruangan';
    //memprotect tabel barang
    protected $table = 'ruangan';
    protected $fillable = ['id', 'nama_ruangan'];

    //DEFINISI RELATIONSHIP  
    public function bangunan_transaksi()
    {
        return $this->hasMany(BangunanTransaksi::class);
    }

    public function tanah_transaksi()
    {
        return $this->hasMany(TanahTransaksi::class);
    }
}
