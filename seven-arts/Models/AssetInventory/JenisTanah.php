<?php

namespace SevenArts\Models\AssetInventory;

use SevenArts\Models\Base\YPWIAssetInventory;

class JenisTanah extends YPWIAssetInventory
{
    const ENTITY_NAME = 'Jenis Tanah';
    protected $table = 'jenis_tanah';
    protected $fillable = ['id', 'jenis_tanah'];

    //  DEFINISI RELATIONSHIP:  
    public function tanah_transaksi()
    {
        return $this->hasMany(TanahTransaksi::class, 'jenis_tanah_id', 'id');
    }
}
