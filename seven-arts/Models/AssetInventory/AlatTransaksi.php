<?php

namespace SevenArts\Models\AssetInventory;

use SevenArts\Models\Base\YPWIAssetInventory;

class AlatTransaksi extends YPWIAssetInventory
{
    const ENTITY_NAME = 'Data relasi alat';
    protected $table = 'alat_transaksi';
    protected $fillable = [
        'sekolah_id', 'kategori_id', 'barang_id', 'kode_alat', 'no_reg_alat',
        'merek_tipe', 'ukuran_cc', 'bahan', 'thn_pembelian', 'no_pabrik',
        'no_rangka', 'no_mesin', 'no_polisi', 'no_bpkb', 'asal_usul', 'harga',
        'sumber_dana', 'kondisi', 'keterangan', 'fotoavatar'
    ];

    // DEFINISI RELATIONSHIP:  
    public function barang()
    {
        return $this->belongsTo(Barang::class);
    }

    public function kategori()
    {
        return $this->belongsTo(Kategori::class);
    }

    public function sekolah()
    {
        return $this->belongsTo('SevenArts\Models\Prime\Sekolah', 'sekolah_id');
    }
}
