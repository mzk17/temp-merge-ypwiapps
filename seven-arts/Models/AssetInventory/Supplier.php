<?php

namespace SevenArts\Models\AssetInventory;

use SevenArts\Models\Base\YPWIAssetInventory;

class Supplier extends YPWIAssetInventory
{
    const ENTITY_NAME = 'Supplier';
    protected $table = 'supplier';
    protected $fillable = ['nama_supplier', 'alamat_supplier', 'telepon_supplier', 'email_supplier'];
}
