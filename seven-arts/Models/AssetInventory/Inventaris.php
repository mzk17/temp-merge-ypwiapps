<?php

namespace SevenArts\Models\AssetInventory;

use SevenArts\Models\Base\YPWIAssetInventory;

class Inventaris extends YPWIAssetInventory
{
    const ENTITY_NAME = 'Inventaris';

    protected $table = 'inventaris';
    protected $fillable = ['id', 'nama_inventaris', 'foto_inventaris', 'id_lokasi', 'kategori', 'jumlah', 'harga', 'tanggal', 'bulan', 'tahun', 'keterangan'];

    //    public function kategori()
    //    {
    //        return $this->belongsToMany(Kategori::class);
    //    }
}
