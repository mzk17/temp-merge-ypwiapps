<?php

namespace SevenArts\Models\AssetInventory;

use SevenArts\Models\Base\YPWIAssetInventory;

class Barang extends YPWIAssetInventory
{
    const ENTITY_NAME = 'Barang';

    //memprotect tabel barang
    protected $table = 'barang';
    protected $fillable = ['id', 'nama_barang'];

    //  DEFINISI RELATIONSHIP:  
    public function alat_trans()
    {
        return $this->hasMany(AlatTransaksi::class);
    }
}
