<?php

namespace SevenArts\Models\AssetInventory;

use SevenArts\Models\Base\YPWIAssetInventory;

class Kategori extends YPWIAssetInventory
{
    const ENTITY_NAME = 'Kategori';
    protected $table = 'kategori';
    protected $fillable = ['id', 'kategori', 'sub_kategori'];

    //  DEFINISI RELATIONSHIP:  
    public function alat_transaksi()
    {
        return $this->hasMany(AlatTransaksi::class);
    }
}
