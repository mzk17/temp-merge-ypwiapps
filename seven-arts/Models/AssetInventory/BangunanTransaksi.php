<?php

namespace SevenArts\Models\AssetInventory;

use SevenArts\Models\Base\YPWIAssetInventory;
use SevenArts\Models\Prime\Sekolah;

class BangunanTransaksi extends YPWIAssetInventory
{
    const ENTITY_NAME = 'Data relasi bangunan';
    protected $table = 'bangunan_transaksi';
    protected $fillable = [
        'sekolah_id', 'kategori_id', 'ruangan_id', 'kode_bangunan', 'no_reg_bangunan',
        'kondisi_bangunan', 'konstruksi_bangunan_bertingkat', 'konstruksi_bangunan_beton',
        'luas_lantai', 'letak_lokasi', 'tanggal_dokumen', 'nomor_dokumen', 'luas_tanah', 'status_tanah',
        'nomor_kode_tanah', 'asal_usul', 'harga', 'sumber_dana', 'keterangan', 'foto_avatar'
    ];

    // DEFINISI RELATIONSHIP:
    public function ruangan()
    {
        return $this->belongsTo(Ruangan::class);
    }

    public function kategori()
    {
        return $this->belongsTo(Kategori::class);
    }

    public function sekolah()
    {
        return $this->belongsTo(Sekolah::class);
    }
}
