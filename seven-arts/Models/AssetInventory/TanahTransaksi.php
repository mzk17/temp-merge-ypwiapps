<?php

namespace SevenArts\Models\AssetInventory;

use SevenArts\Models\Base\YPWIAssetInventory;
use SevenArts\Models\Prime\Sekolah;

class TanahTransaksi extends YPWIAssetInventory
{
    const ENTITY_NAME = 'Tanah';
    protected $table = 'tanah_transaksi';
    protected $fillable = [
        'sekolah_id', 'kategori_id', 'jenis_tanah_id', 'kode_tanah', 'no_reg_tanah',
        'luas', 'tahun_pengadaan', 'letak_alamat', 'stts_tnh_hak', 'stts_tnh_tglsertifikat', 'stts_tnh_nmrsertifikat',
        'penggunaan', 'sumber_dana', 'harga', 'keterangan', 'foto_tnh'
    ];

    //DEFINISI RELATIONSHIP:  
    public function jenis()
    {
        return $this->belongsTo(JenisTanah::class, 'jenis_tanah_id', 'id');
    }

    public function kategori()
    {
        return $this->belongsTo(Kategori::class);
    }

    public function sekolah()
    {
        return $this->belongsTo(Sekolah::class);
    }
}
