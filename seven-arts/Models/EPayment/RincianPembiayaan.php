<?php

namespace SevenArts\Models\EPayment;

use SevenArts\Models\Base\YPWIEPayment;

class RincianPembiayaan extends YPWIEPayment
{
    const ENTITY_NAME = 'Rincian pembiayaan';

    protected $table = 'rincian_pembiayaan';

    public $incrementing = false;

    protected $keyType = 'string';

    protected $guarded = [];

    protected $casts = [
        'id' => 'string'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public function pembiayaan()
    {
        return $this->belongsTo('SevenArts\Models\EPayment\PembiayaanSekolah', 'id_pembiayaan_sekolah');
    }
}
