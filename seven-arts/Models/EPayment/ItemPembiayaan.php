<?php

namespace SevenArts\Models\EPayment;

use SevenArts\Models\Base\YPWIEPayment;

class ItemPembiayaan extends YPWIEPayment
{
    const ENTITY_NAME = "Item Pembiayaan";
    
    protected $table = "item_pembiayaan";

    protected $guarded = [];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public function pembiayaanSekolah()
    {
        return $this->hasMany('SevenArts\Models\EPayment\PembiayaanSekolah', 'id_item_pembiayaan');
    }
}
