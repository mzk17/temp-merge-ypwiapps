<?php

namespace SevenArts\Models\EPayment;

use Illuminate\Support\Facades\DB;
use SevenArts\Models\Base\YPWIEPayment;

class PembiayaanSiswa extends YPWIEPayment
{
    const ENTITY_NAME = 'Item pembiayaan peserta didik';

    protected $table = 'pembiayaan_siswa';

    public $incrementing = false;

    protected $keyType = 'string';

    protected $guarded = [];

    protected $casts = [
        'id' => 'string'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public function siswa()
    {
        return $this->belongsTo('\SevenArts\Models\Prime\PesertaDidik', 'id_peserta_didik', 'id');
    }

    public function pembiayaan()
    {
        return $this->belongsTo('SevenArts\Models\EPayment\PembiayaanSekolah', 'id_pembiayaan_sekolah');
    }

    public function subsidi()
    {
        return $this->hasMany('SevenArts\Models\EPayment\SubsidiPembiayaan', 'id_pembiayaan_siswa');
    }

    public function rincianTransaksi()
    {
        return $this->hasMany('SevenArts\Models\EPayment\RincianTransaksi', 'id_item_pembiayaan', 'id');
    }

    /**
     * Custom Scopes
     */
    public function scopeSudahLunas($query, $informasi_transaksi)
    {
        $pembiayaan = $query->where('id', $informasi_transaksi->id_item_pembiayaan)->first();

        return $informasi_transaksi->total_dibayar >= $sisa_biaya;
    }

    public function scopeRincianPembiayaan($query, $id_item_pembiayaan_siswa)
    {
        $daftar_transaksi = DB::connection('ypwi_epayment')->table('rincian_transaksi')
            ->addSelect('bayar')
            ->selectRaw(DB::raw('RIGHT(id_item_pembiayaan, 36) AS id_rincian_pembiayaan'))
            ->where('id_item_pembiayaan', 'like', $id_item_pembiayaan_siswa.'%')
            ->get();
        
        // Daftar ID rincian pembiayaan yang sudah dibayarkan sebagian
        $ids = $daftar_transaksi->pluck('id_rincian_pembiayaan')->all();

        /**
         * @var  array $xyz
         * Sebuah variabel array dengan struktur sebagai berikut
         * [ID_RINCIAN_PEMBIAYAAN => JUMLAH_YG_SUDAH_DIBAYAR]
         * 
         * Contoh:
         * [
         *  'b7078641-0b95-41e3-a23e-06d180fddd02' => 150000,
         *  'b00a0037-d880-4846-9936-6824687d03fd' => 5000000
         * ]
         */
        $xyz = $daftar_transaksi->pluck('bayar', 'id_rincian_pembiayaan')->all();

        $rincian = $this->pembiayaan->rincian;

        foreach ($rincian as $r) {
            $r->sisa = (in_array($r->id, $ids))
                ? $r->biaya - $xyz[$r->id] 
                : $r->biaya;
        }

        return $rincian;
    }

    public function scopeSisa()
    {
        $rincian_pembiayaan = $this->rincianPembiayaan($this->id);
        
        return (count($rincian_pembiayaan) > 1) 
            ? $this->biaya - $rincian_pembiayaan->sum('sisa') 
            : $this->biaya - collect($this->rincianTransaksi)->sum('bayar');
    }
}
