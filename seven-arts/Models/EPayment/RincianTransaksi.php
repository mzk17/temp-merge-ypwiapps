<?php

namespace SevenArts\Models\EPayment;

use SevenArts\Models\Base\YPWIEPayment;

class RincianTransaksi extends YPWIEPayment
{
    const ENTITY_NAME = 'Rincian transaksi';

    protected $table = 'rincian_transaksi';

    public $incrementing = false;

    protected $keyType = 'string';

    protected $guarded = [];

    protected $casts = [
        'id' => 'string'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /**
     * Menambahkan attribut tambahan secara default
     * ke model jika di akses.
     */
    protected $appends = [
        'nama_pembiayaan'
    ];

    public function transaksi()
    {
        return $this->belongsTo('SevenArts\Models\EPayment\Transaksi', 'id_transaksi', 'id');
    }

    public function pembiayaanSiswa()
    {
        return $this->belongsTo('SevenArts\Models\EPayment\PembiayaanSiswa', 'id_item_pembiayaan', 'id');
    }

    // public function scopeCekStatusPembiayaan()
    // {
    //     $id_pembiayaan_siswa
    // }

    public function getNamaPembiayaanAttribute()
    {
        $info_pembiayaan = explode(":", $this->id_item_pembiayaan);

        // Item Pembiayaan yg bersangkutan memiliki rincian
        if (count($info_pembiayaan) === 2) {
            $id_pembiayaan_siswa = $info_pembiayaan[0];
            $id_rincian = $info_pembiayaan[1];
            
            $pembiayaan_siswa = PembiayaanSiswa::find($id_pembiayaan_siswa);
            return $pembiayaan_siswa->pembiayaan
                ->rincian()->where('id', $id_rincian)
                ->first()
                ->nama_item;
        } else {
            return $this->makeHidden('pembiayaanSiswa')
                ->pembiayaanSiswa->makeHidden('pembiayaan')
                ->pembiayaan
                ->nama_pembiayaan;
        }
    }

    private function merupakanRincianPembiayaan()
    {
        $info_pembiayaan = explode(":", $this->id_item_pembiayaan);

        return count($info_pembiayaan) === 2;
    }

    /**
     * Custom Scopes
     */
    public function scopeSudahLunas()
    {
        if ($this->merupakanRincianPembiayaan()) {
            $info_pembiayaan  = explode(":", $this->id_item_pembiayaan);
            $pembiayaan_siswa = PembiayaanSiswa::find($info_pembiayaan[0]);
            $rincian          = $pembiayaan_siswa->pembiayaan->rincian->where('id', $info_pembiayaan[1])->first();

            return number_format($this->bayar) >= number_format($rincian->biaya);
        }
        
        return number_format($this->bayar) >= number_format($this->pembiayaanSiswa->biaya);
    }

    public function scopeIdPembiayaanSiswa()
    {
        return explode(":", $this->id_item_pembiayaan)[0];
    }
}
