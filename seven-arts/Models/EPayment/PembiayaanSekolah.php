<?php

namespace SevenArts\Models\EPayment;

use App\Events\EPayment\PembiayaanSekolahCreated;
use SevenArts\Models\Base\YPWIEPayment;

class PembiayaanSekolah extends YPWIEPayment
{
    const ENTITY_NAME = 'Pembiayaan Sekolah';

    protected $table = 'pembiayaan_sekolah';

    public $incrementing = false;

    protected $keyType = 'string';

    protected $guarded = [];

    protected $casts = [
        'id' => 'string'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    protected $dispatchesEvents = [
        'created' => PembiayaanSekolahCreated::class
    ];

    public function itemPembiayaan()
    {
        return $this->belongsTo('SevenArts\Models\EPayment\ItemPembiayaan', 'id_pembiayaan');
    }

    public function rincian()
    {
        return $this->hasMany('SevenArts\Models\EPayment\RincianPembiayaan', 'id_pembiayaan_sekolah');
    }

    public function pembiayaanSiswa()
    {
        return $this->hasMany('SevenArts\Models\EPayment\PembiayaanSiswa', 'id_pembiayaan_sekolah');
    }
}
