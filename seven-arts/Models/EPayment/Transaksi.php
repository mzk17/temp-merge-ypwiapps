<?php

namespace SevenArts\Models\EPayment;

use SevenArts\Models\Base\YPWIEPayment;

class Transaksi extends YPWIEPayment
{
    const ENTITY_NAME = 'Pembayaran';

    protected $table = 'transaksi';

    public $incrementing = false;

    protected $keyType = 'string';

    protected $guarded = [];

    protected $casts = [
        'id' => 'string'
    ];

    protected $hidden = [];

    protected $appends = [
        'item'
    ];

    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(\DateTimeInterface $date)
    {
        $tanggal = $date->format('l, d M Y. ');
        $waktu   = "Jam " . $date->format("h:i");

        return $tanggal . $waktu;
    }

    public function siswa()
    {
        return $this->belongsTo('SevenArts\Models\Prime\PesertaDidik', 'id_peserta_didik', 'id');
    }

    public function rincian()
    {
        return $this->hasMany('SevenArts\Models\EPayment\RincianTransaksi', 'id_transaksi', 'id');
    }

    public function getItemAttribute()
    {
        return count($this->rincian) === 1 ? null : $this->makeHidden('rincian')->rincian;
    }
}
