<?php

namespace SevenArts\Models\EPayment;

use SevenArts\Models\Base\YPWIEPayment;

class SubsidiPembiayaan extends YPWIEPayment
{
    const ENTITY_NAME = 'Subsidi pembiayaan';

    protected $table = 'subsidi_pembiayaan';

    protected $guarded = [];

    public function pembiayaanSiswa()
    {
        return $this->belongsTo('SevenArts\Models\EPayment\PembiayaanSiswa', 'id_pembiayaan_siswa');
    }
}
