<?php

namespace SevenArts\Models\Base;

use App\Helpers\Traits\ModelLogger;
use Illuminate\Database\Eloquent\Model;

class YPWIConstant extends Model
{
    use ModelLogger;
    /**
     * The connection name for the model
     *
     * @var string
     */
    protected $connection = 'constant';

    // public static function boot()
    // {
    //     self::cudLogger();
    // }
}
