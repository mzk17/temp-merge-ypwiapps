<?php

namespace SevenArts\Models\Base;

use App\Helpers\Traits\ModelLogger;
use Illuminate\Database\Eloquent\Model;

class YPWIEPayment extends Model
{
  use ModelLogger;
  /**
   * The connection name for the model
   * 
   * @var string
   */
  protected $connection = 'ypwi_epayment';

  public static function boot()
  {
    $classes_using_uuid = [
      'SevenArts\Models\EPayment\PembiayaanSekolah',
      'SevenArts\Models\EPayment\RincianPembiayaan',
      'SevenArts\Models\EPayment\PembiayaanSiswa',
      'SevenArts\Models\EPayment\Transaksi',
    ];

    parent::boot();
    if(in_array(get_called_class(), $classes_using_uuid)) {
      self::creating(function ($model) {
        $model->id = \Str::uuid();
      });
    }

    self::cudLogger();
  }
}