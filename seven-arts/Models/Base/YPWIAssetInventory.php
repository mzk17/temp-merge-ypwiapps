<?php

namespace SevenArts\Models\Base;

use App\Helpers\Traits\ModelLogger;
use Illuminate\Database\Eloquent\Model;

class YPWIAssetInventory extends Model
{
  use ModelLogger;
  /**
   * The connection name for the model
   * 
   * @var string
   */
  protected $connection = 'ypwi_asset_inventory';

  public static function boot()
  {
    self::cudLogger();
  }
}