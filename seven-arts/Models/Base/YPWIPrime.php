<?php

namespace SevenArts\Models\Base;

use App\Helpers\Traits\ModelLogger;
use Illuminate\Database\Eloquent\Model;

class YPWIPrime extends Model
{
  use ModelLogger;
  /**
   * The connection name for the model
   * 
   * @var string
   */
  protected $connection = 'ypwi_prime';

  public static function boot()
  {
    $classes_using_uuid = [
      'SevenArts\Models\Prime\Alamat',
      'SevenArts\Models\Prime\Sekolah',
      'SevenArts\Models\Prime\Lembaga',
      'SevenArts\Models\Prime\DataPribadi',
      'SevenArts\Models\Prime\Karyawan',
      'SevenArts\Models\Prime\TenagaPendidik',
      'SevenArts\Models\Prime\PesertaDidik',
    ];

    parent::boot();
    if(in_array(get_called_class(), $classes_using_uuid)) {
      self::creating(function ($model) {
        $model->id = \Str::uuid();
      });
    }

    self::cudLogger();
  }
}