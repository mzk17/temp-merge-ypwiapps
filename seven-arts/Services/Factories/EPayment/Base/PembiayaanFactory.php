<?php

namespace SevenArts\Services\Factories\EPayment\Base;

abstract class PembiayaanFactory
{
    abstract public function buatPembiayaanTunggal();
}