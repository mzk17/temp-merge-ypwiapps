<?php

namespace SevenArts\Services\Factories\EPayment;

use App\Repositories\Prime\SekolahRepository;
use SevenArts\Repositories\EPayment\PembiayaanSiswaRepository;
use SevenArts\Services\Factories\EPayment\Base\PembiayaanFactory;
use SevenArts\Support\Traits\EPayment\PayloadPembiayaanSiswa;

class PembiayaanSiswaFactory extends PembiayaanFactory
{
    use PayloadPembiayaanSiswa;

    public $repository;

    public $pembiyaan_sekolah;

    public function __construct($pembiyaan_sekolah = null)
    {
        $this->repository = new PembiayaanSiswaRepository();
        $this->pembiyaan_sekolah = $pembiyaan_sekolah;
    }

    public function buatPembiayaanTunggal()
    {
        // 1. Cek pembiayaan apakah khusus untuk kelas tertentu atau untuk semuanya
        // 2. Ambil data siswa yang ada pada sekolah
        // 3. Buat pembiayaan

        $sekolah = new SekolahRepository($this->pembiyaan_sekolah->id_sekolah);
        $daftar_siswa = $sekolah->daftarSiswa(
            (isset($this->pembiyaan_sekolah->kelas))
                ? $this->pembiyaan_sekolah->kelas
                : []
        );

        $pembiayaan_siswa = [];

        foreach ($daftar_siswa as $siswa) {
            array_push(
                $pembiayaan_siswa, 
                $this->payloadPembiayaanSiswa($siswa, $this->pembiyaan_sekolah)
            );
        }
    
        return $this->repository->simpan($pembiayaan_siswa);
    }

    public function buatPembiayaanRutin()
    {
        if($this->pembiyaan_sekolah instanceof \Illuminate\Database\Eloquent\Collection) {
            $ps = $this->pembiyaan_sekolah;
            foreach ($ps as $pembiayaan_sekolah) {
                $this->pembiyaan_sekolah = $pembiayaan_sekolah;
                $this->buatPembiayaanTunggal();
            }
        }
    }
}