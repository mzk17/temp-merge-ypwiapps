<?php

namespace SevenArts\Services\Factories\EPayment;

use App\Events\EPayment\PembiayaanSekolahCreated;
use Illuminate\Http\Request;
use SevenArts\Repositories\EPayment\PembiayaanSekolahRepository;
use SevenArts\Services\Factories\EPayment\Base\PembiayaanFactory;
use SevenArts\Support\Traits\EPayment\PayloadPembiayaanSekolah;

class PembiayaanSekolahFactory extends PembiayaanFactory
{
    use PayloadPembiayaanSekolah;

    public $repository;

    public function __construct()
    {
        $this->repository = new PembiayaanSekolahRepository();
    }

    public function buatPembiayaanTunggal()
    {
        $payload_pembiayaan_sekolah = $this->payloadPembiayaanSekolah(request());

        $rincian_pembiayaan = ['use' => request()->get('rincian_pembiayaan')];

        if (request()->get('rincian_pembiayaan'))
            $rincian_pembiayaan['item'] = request()->get('item_rincian_pembiayaan');

        return $this->repository->simpan(
            $payload_pembiayaan_sekolah,
            $rincian_pembiayaan
        );
    }

    public function buatPembiayaanRutin()
    {
        $payload_pembiayaan_sekolah = [];

        foreach(request()->get('nama_pembiayaan') as $key => $nama_pembiayaan) {
            array_push($payload_pembiayaan_sekolah, $this->payloadPembiayaanSekolah(request(), true, $key));
        }

        $pembiayaan_sekolah = $this->repository->simpanBanyak($payload_pembiayaan_sekolah);
        event(new PembiayaanSekolahCreated($pembiayaan_sekolah));

        return $pembiayaan_sekolah;
    }
}