<?php

namespace SevenArts\Services;

use App\Repositories\Prime\DataPribadiRepository;
use Illuminate\Support\Facades\Validator;
use SevenArts\Support\Handlers\ErrorHandler;
use SevenArts\Support\Traits\BasicCRUD\FieldsFilter;

class DataPribadiService
{
    use FieldsFilter;

    /**
     * Objek repositori utama dalam service ini
     * 
     * @var \App\Repositories\Prime\DataPribadiRepository
     */
    private $repository;

    /**
     * Daftar kolom dari model dalam service ini
     * 
     * @var array
     */
    private $fields;

    /**
     * Objek service alamat
     * 
     * @var \SevenArts\Services\AlamatService
     */
    private $alamat_service;

    /**
     * @param bool $alamat Jika pemrosesan data pribadi sekaligus dengan alamat
     */
    public function __construct($alamat = true)
    {
        $this->repository = new DataPribadiRepository();

        $this->fields = $this->getNecessaryFieldsFrom('\SevenArts\Models\Prime\DataPribadi', ['id']);
        
        if($alamat) {
            $this->alamat_service = new AlamatService();
        }
    }

    /**
     * Buat sebuah entitas data pribadi
     * 
     * @return array berisi status dan data yang baru saja dibuat
     */
    public function createPersonalData()
    {
        // Buat alamat terlebih dahulu
        if(isset($this->alamat_service)) {
            $alamat = $this->createAlamat();
            
            // Jika berhasil, set alamat ID
            if($alamat['status'] === 'success')
                request()->merge(['alamat_1' => $alamat['data']->id]);
        }

        $validator = $this->validation();

        if (!$validator->fails()) {
            $status = 'success';
            $data = request()->only($this->fields);
            $data = $this->repository->create($data);
        } else {
            $status = 'failed';
            // Simpan pesan error pada session
            ErrorHandler::multipleActionFormValidation($validator);
        }

        return [
            'status' => $status,
            'data' => $data ?? ''
        ];
    }

    public function updatePersonalData($id_data_pribadi, $id_alamat)
    {
        // Update alamat terlebih dahulu
        if(isset($this->alamat_service)) {
            $this->alamat_service->updateAlamat($id_alamat);
        }

        $validator = $this->validation();

        if (!$validator->fails()) {
            $status = 'success';
            $data = array_merge(
                [
                    'id' => $id_data_pribadi, 
                    'alamat_1' => $id_alamat
                ], 
                request()->only($this->fields)
            );
            $data = $this->repository->update($id_data_pribadi, $data);
            
        } else {
            $status = 'failed';
            // Simpan pesan error pada session
            ErrorHandler::multipleActionFormValidation($validator);
        }

        return [
            'status' => $status,
            'data' => $data ?? ''
        ];
    }

    /**
     * Jalankan validasi untuk pembuatan data pribadi
     * 
     * @return \Illuminate\Support\Facades\Validator
     */
    private function validation()
    {
        $data = request()->only($this->fields);

        $rules = [
            'nama' => 'required|string|min:3',
            'jenis_kelamin' => 'required|string|max:1',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'agama' => 'required',
        ];

        // Validasi alamat jika pembuatan data pribadi sekaligus alamat
        if(isset($this->alamat_service)) {
            $rules = array_merge($rules, ['alamat_1' => 'required']);
            $data = array_merge(['alamat_1' => request()->get('id_alamat')], $data);
        }
        
        return Validator::make($data, $rules);
    }

    /**
     * 
     */
    private function createAlamat()
    {
        return $this->alamat_service->createAlamat();
    }
}