<?php

namespace SevenArts\Services;

use App\Repositories\Prime\AlamatRepository;
use SevenArts\Support\Handlers\ErrorHandler;
use SevenArts\Support\Traits\BasicCRUD\FieldsFilter;
use SevenArts\Validators\AlamatValidator;

class AlamatService
{
    use FieldsFilter;

    /**
     * Objek repositori utama dalam service ini
     * 
     * @var \App\Repositories\Prime\DataPribadiRepository
     */
    private $repository;

    /**
     * Daftar kolom dari model dalam service ini
     * 
     * @var array
     */
    private $fields;

    public function __construct()
    {
        $this->repository = new AlamatRepository();

        $this->fields = $this->getNecessaryFieldsFrom('\SevenArts\Models\Prime\Alamat', ['id']);
    }

    /**
     * Buat sebuah entitas alamat
     * 
     * @return array berisi status dan data yang baru saja dibuat
     */
    public function createAlamat($in_method = null)
    {
        $validator = $this->validation();

        if (!$validator->fails()) {
            $status = 'success';
            $data = request()->only($this->fields);
            $data = $this->repository->create($data);
        } else {
            $status = 'failed';
            // Simpan pesan error pada session
            ErrorHandler::multipleActionFormValidation($validator);
        }

        return [
            'status' => $status,
            'data' => $data ?? ''
        ];
    }

    public function updateAlamat($id_alamat)
    {
        $validator = $this->validation();

        if (!$validator->fails()) {
            $status = 'success';
            $data = array_merge(['id' => $id_alamat], request()->only($this->fields));
            $data = $this->repository->update($id_alamat, $data);
        } else {
            $status = 'failed';
            // Simpan pesan error pada session
            ErrorHandler::multipleActionFormValidation($validator);
        }

        return [
            'status' => $status,
            'data' => $data ?? ''
        ];
    }

    /**
     * Jalankan validasi untuk pembuatan alamat
     * 
     * @return \Illuminate\Support\Facades\Validator
     */
    private function validation()
    {
        return AlamatValidator::validate(request()->only($this->fields));
    }
}