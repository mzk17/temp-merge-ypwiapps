<?php

namespace SevenArts\Validators;

use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

trait PersonalData
{
    // Pesan error pada validasi input
    private function errorValidationMessages()
    {
        $this->err_validation_messages = [
            'required' => ':attribute tidak boleh kosong',
            'string'   => ':attribute tidak boleh berisi angka',
            'date'     => ':attribute harus dengan format YYYY-MM-DD',
            'min'      => ':attribute minimal sebanyak :min huruf',
            'max'      => ':attribute maksimal sebanyak :min huruf',
            'alamat_1.required' => 'Anda harus mempunyai alamat utama'
        ];
    }

    // Ambil kolom yang ingin di-validasi
    private function onlyNeededFields(array $source, array $validatedFields)
    {
        $result  = [];
        $data    = $source;
        $vfcount = count($validatedFields);

        // Remove 'id' element
        unset($data['id']);
        
        foreach ($data as $field => $value) {
            if (array_key_exists($field, $validatedFields)) {
                $result = array_merge($result, [$field => $validatedFields[$field]]);
                unset($validatedFields[$field]);
            }
        }

        if ($vfcount > count($result)) {
            $remainingRequired = array_diff_key($validatedFields, $result);
            $result = array_merge($result, $remainingRequired);
        }

        return $result;
    }

    private function validateAction(array $data, array $allFields)
    {
        $validated = $this->onlyNeededFields($data, $allFields);
        return Validator::make(
            $data,
            $validated,
            $this->err_validation_messages
        );
    }
    
    protected function personalDataValidator(array $data)
    {
        $fields = [
            'nama_depan'    => ['required', 'string', 'min:3', 'max:255'],
            'tempat_lahir'  => ['required', 'string', 'min:3', 'max:255'],
            'tanggal_lahir' => ['required', 'date'],
            'alamat_1'      => ['required', 'integer', 'min:1'],
            'alamat_2'      => ['integer', 'min:1'],
            'alamat_3'      => ['integer', 'min:1']
        ];

        return $this->validateAction($data, $fields);
    }

    protected function addressValidator(array $data)
    {
        $fields = [
            'alamat'   => ['required', 'string', 'min: 5'],
            'nama_alamat'  => ['required', 'string', 'min: 6'],
            'provinsi'     => ['required'],
            'kota'         => ['required'],
            'alamat_utama' => ['required', 'boolean']
        ];

        return $this->validateAction($data, $fields);
    }
}
