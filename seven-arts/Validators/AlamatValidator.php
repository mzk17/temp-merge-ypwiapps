<?php

namespace SevenArts\Validators;

class AlamatValidator extends BaseValidator
{
    public static function validate(array $data, $error_validation_messages = [])
    {
        $fields = [
            'alamat'   => ['required', 'string', 'min: 5'],
            'nama_alamat'  => ['required', 'string', 'min: 6'],
            'alamat_utama' => ['required', 'boolean'],
            'provinsi'     => ['required'],
            'kota'         => ['required'],
        ];

        if(count($error_validation_messages) == 0) {
            $error_validation_messages = [
                'required' => ':attribute tidak boleh kosong',
                'min'      => ':attribute minimal sebanyak :min huruf',
                'alamat_utama.required' => 'Anda harus mempunyai alamat utama'
            ];
        }

        return self::validateAction($data, $fields, $error_validation_messages);
    }
}
