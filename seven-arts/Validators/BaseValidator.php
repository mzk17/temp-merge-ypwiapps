<?php

namespace SevenArts\Validators;

use Illuminate\Support\Facades\Validator;
use SevenArts\Contracts\ValidationHandler;

class BaseValidator implements ValidationHandler
{
    // Ambil kolom yang ingin di-validasi
    public static function onlyNeededFields(array $source, array $validatedFields)
    {
        $result  = [];
        $data    = $source;
        $vfcount = count($validatedFields);

        // Remove 'id' element
        unset($data['id']);
        
        foreach ($data as $field => $value) {
            if (array_key_exists($field, $validatedFields)) {
                $result = array_merge($result, [$field => $validatedFields[$field]]);
                unset($validatedFields[$field]);
            }
        }

        if ($vfcount > count($result)) {
            $remainingRequired = array_diff_key($validatedFields, $result);
            $result = array_merge($result, $remainingRequired);
        }

        return $result;
    }

    public static function validateAction(array $data, array $all_fields, $error_validation_messages)
    {
        $validated = self::onlyNeededFields($data, $all_fields);
        return Validator::make(
            $data,
            $validated,
            $error_validation_messages
        );
    }
}
