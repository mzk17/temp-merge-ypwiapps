<?php

use App\Constants\PrimeEventListeners;
use App\User;
use SevenArts\Models\Prime\Lembaga;
use Barryvdh\DomPDF\PDF;

// Directly to login page
Route::get('/', function () {
    return view('pages.prime.landing');
})->middleware('guest')->name('landing');

Auth::routes(['verify' => true]);
Route::get('/pre-register', 'Auth\PreRegisterController@showPreRegistrationForm')->name('pre-register');
Route::post('/pre-register', 'Auth\PreRegisterController@register')->name('post-pre-register');

// Prime app route
Route::prefix('prime')
    ->name('prime.')
    ->group(function () {
        Route::middleware('auth')->namespace('Auth')->group(function () {
            Route::get('/verify/resend', 'VerificationController@resentEmailVerification')->name('resend-verification');
            Route::get('/verify/eona', 'VerificationController@verificationNotice')->name('verification-notice');
            Route::get('/verify/eona/{token}/token', 'VerificationController@verificationAccount')->name('verification-action');
        });

        Route::namespace('Prime')->group(function () {
            Route::middleware(['auth', 'verified'])->group(function () {
                // Complete Registration
                Route::get('/complete-registration', 'UserController@showFormCompleteRegistration')->name('show-fcr');
                Route::post('/complete-registration', 'UserController@completeRegistration')->name('post-complete-registration');
            });

            Route::middleware(['auth', 'verified', 'verified.completed'])
                ->group(function () {
                    Route::get('home', 'MasterController@home')->name('main-home');
                    Route::get('a/id={id}&token={token}', 'MasterController@a')->name('a');

                    // Data Pribadi
                    Route::resource('/personal-data', 'DataPribadiController')
                        ->parameters(['personal-data' => 'data_pribadi'])
                        ->only(['index', 'edit', 'update']);

                    // Data Lembaga
                    Route::resource('/lembaga', 'LembagaController')->except(['show', 'create']);

                    // Sekolah
                    Route::resource('/sekolah', 'SekolahController');

                    // Karyawan
                    Route::resource('/karyawan', 'KaryawanController');
                    Route::post('/employee-dps', 'KaryawanController@beforeStore')->name('dps');

                    // Tenaga Pendidik
                    Route::resource('/tenaga-pendidik', 'TenagaPendidikController')->except(['show', 'create']);

                    // Peserta Didik
                    Route::resource('/peserta-didik', 'PesertaDidikController')->except(['show', 'create']);

                    // Send mail
                    Route::get('/kirim-email', 'UserController@sendEmail');
                });
        });
});

/**
 * **************************************** 
 * Asset & Inventory routes
 * ****************************************
 */
Route::prefix('asset-inventory')
    ->namespace('AssetInventory')
    ->name('asset-inventory.')
    ->middleware(['auth', 'verified', 'verified.completed'])
    ->group(function () {
        Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

        // Transaksi
        Route::prefix('transaksi')
            ->namespace('Transaksi')
            ->name('transaksi.')
            ->group(function () {
                // Alat
                Route::resource('alat', 'AlatController')->except(['show', 'create']);
                Route::prefix('alat')->name('alat.')->group(function () {
                    Route::get('/export', 'AlatController@export')->name('export');
                });
                // Bangunan
                Route::resource('bangunan', 'BangunanController')->except(['show', 'create']);
                // Tanah
                Route::resource('tanah', 'TanahController');
            });
        // Barang
        Route::resource('barang', 'BarangController')->except(['show', 'create']);

        // Jenis Tanah
        Route::prefix('jenis-tanah')->name('jenis-tanah.')->group(function () {
            Route::get('/', 'JenisTanahController@index')->name('index');
            Route::post('/create', 'JenisTanahController@create')->name('create');
            Route::get('/{id}/edit', 'JenisTanahController@edit')->name('edit');
            Route::post('/{id}/update', 'JenisTanahController@update')->name('update');
            Route::get('/{id}/delete', 'JenisTanahController@delete')->name('delete');
        });
        // Ruangan
        Route::resource('ruangan', 'RuanganController')->except(['show', 'create']);
        // Kategori
        Route::resource('kategori', 'KategoriController')->except(['show', 'create']);
        // Supplier
        Route::resource('supplier', 'SupplierController')->except(['show', 'create']);

        // Route::get('/karyawan', 'KaryawanController@index')->name('karyawan-index');
        // Route::post('/karyawan/create', 'KaryawanController@create');
        // Route::get('/karyawan/{id}/edit', 'KaryawanController@edit');
        // Route::post('/karyawan/{id}/update', 'KaryawanController@update');
        // Route::get('/karyawan/{id}/delete', 'KaryawanController@delete');

        // Route::get('/inventaris', 'InventarisController@index')->name('inventaris-index');
        // Route::post('/inventaris/create', 'InventarisController@create');
        // Route::get('/inventaris/{id}/edit', 'InventarisController@edit');
        // Route::post('/inventaris/{id}/update', 'InventarisController@update');
        // Route::get('/inventaris/{id}/delete', 'InventarisController@delete');

        // Route::get('/sekolah', 'SekolahController@index')->name('sekolah-index');
        // Route::post('/sekolah/create', 'SekolahController@create');
        // Route::get('/sekolah/{id}/edit', 'SekolahController@edit');
        // Route::post('/sekolah/{id}/update', 'SekolahController@update');
        // Route::get('/sekolah/{id}/delete', 'SekolahController@delete');
        // Route::get('/sekolah/{id}/profile', 'SekolahController@profile');
});

// E-Payment route
Route::prefix('epayment')
    ->namespace('EPayment')
    ->name('epayment.')
    ->middleware(['auth', 'verified', 'verified.completed'])
    ->group(function () {
        Route::get('/', 'DashboardController@index')->name('dashboard');
});

// Experiment routes
/**
 * ****************************************
 * Experiment routes
 * ****************************************
 * 
 * Any functions you wanna try, smash it here
 * before putted into real one!
 */
Route::prefix('test')->name('test.')->group(function () {
    Route::get('/coba-redirect', 'ExperimentController@cobaRedirect');
    Route::get('/hasil-redirect', function () {
        return "YAY!";
    })->name('test.hasil-redirect');

    Route::get('/change-reference-data', 'ExperimentController@changeReferenceData');
    // Route::resource('/foundation', 'LembagaController')->except(['create', 'edit']);
    Route::resource('/school', 'Prime\SekolahController')->except(['create', 'edit']);
    Route::get('/zero-resource-method/using-sp/{id}', 'ExperimentController@show');

    Route::get('/use-mailable', function () {
        $user = new stdClass();
        $user->name = 'Saiko Mizuki';
        $user->username = 'mzk_ky';
        $user->email = 'mzk@me.com';
        $user->token = sha1(time());

        try {
            \Mail::to('izzuddin.alfikri@icloud.com')->send(new App\Mail\VerifyUserRegistered($user));
            return response()->json("Yea");
        } catch (\Throwable $th) {
            throw $th;
        }
    });
    Route::get('/url-params/{token}', 'Auth\VerificationController@verificationAccount');
    Route::get('/expiration', function () {
        return view('auth.verification-expired');
    });
    // Route::get('/auto-increment-static-id', 'LembagaController@newID');

    // Route::get('/parent-inherit', 'InheritedExperimentController@someAction');

    Route::get('/get-model-info/{id}', function ($id) {
        $model = Lembaga::find($id);
        return response()->json(['model' => $model]);
    })->name('get-model-info');

    // Route::prefix('components')->group(function () {
    //     Route::get('list', 'ComponentController@list');
    // });

    Route::get('generate-pdf', function () {
        $data = ['title' => 'This is generated by DomPDF'];
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('mypdf', $data);

        return $pdf->download('generated_pdf.pdf');
    });

    Route::post('eksperimen-validasi', 'ExperimentController@cobaValidasi');
    Route::get('halaman-eksperimen-validasi', 'ExperimentController@halamanValidasi')->name('halaman-eksperimen-validasi');

    Route::resource('prime/school', 'Prime\SekolahController');
    Route::get('prime/school-with-relations/{school}', 'Prime\SekolahController@lihat');

    Route::get('array-merge', function () {
        dd(array_merge([
            'App\Events\Prime\SekolahDeleted' => [
                'App\Listeners\Prime\RemoveAlamatSekolah',
            ],
        ], PrimeEventListeners::HAPUS_ALAMAT));
    });
});

/**
 * ****************************************
 * Dummy routes
 * ****************************************
 * 
 * Any values you wanna know or expose, put it here!
 */
Route::prefix('dummy')->group(function() {
    Route::get('/current-ip', function () {
        return request()->ip();
    });
    Route::get('/check-database', function () {
        return User::all();
    });
});