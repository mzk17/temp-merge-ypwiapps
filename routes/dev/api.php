<?php

use App\Repositories\Prime\SekolahRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use SevenArts\Models\EPayment\PembiayaanSekolah;
use SevenArts\Models\EPayment\PembiayaanSiswa;
use SevenArts\Models\EPayment\RincianTransaksi;
use SevenArts\Support\Facades\EPayment\SubsidiHandler;
use Symfony\Component\HttpKernel\Event\RequestEvent;

/**
 * **********************************************************
 * Prime
 * **********************************************************
 */
Route::namespace('Prime')->prefix('adm-wilayah')->group(function () {
    Route::get('daftar-provinsi', 'AlamatController@apiShowProvinceList');
    Route::get('daftar-kota/{previous_id}', 'AlamatController@apiShowDescendantsRegionList');
    Route::get('daftar-kecamatan/{previous_id}', 'AlamatController@apiShowDescendantsRegionList');
    Route::get('daftar-kelurahan/{previous_id}', 'AlamatController@apiShowDescendantsRegionList');
});

/**
 * **********************************************************
 * E-Payment
 * **********************************************************
 */
Route::namespace('EPayment')->prefix('e-payment')->group(function () {
    // Pembiayaan Sekolah
    Route::prefix('pembiayaan-sekolah')
        ->name('epayment.')
        ->group(function () {
            Route::post('buat-pembiayaan', 'PembiayaanSekolahController@buatPembiayaan')->name('buat-pembiayaan-sekolah');
        });
});

// Route::get('auto-query-alamat', 'Prime\AlamatController@autoQuery');
Route::prefix('constant')->group(function () {
    Route::get('daftar-agama', 'Constant\AgamaController@index')->name('daftar-agama');
});

/**
 * **********************************************************
 * Test / Experiment / Development
 * **********************************************************
 */
Route::prefix('test')->group(function () {
    Route::get('method-variable', 'ExperimentController@actionName');
    Route::get('data-pribadi', 'ExperimentController@repositoryDataPribadi');
    Route::post('upload', 'ExperimentController@import');
    Route::get('{id_sekolah}/daftar-tendik', 'ExperimentController@daftarTendik');

    // Experiments
    Route::get('destructuring-array', function () {
        $array = [
            "Nama, Alamat, Gender",
            "Jalan, Kecamatan, Kelurahan"
        ];
        [$data_diri, $alamat] = $array;

        // Jika jumlah elemen kurang, maka ngga bisa "Undefined offset: x"

        return response()->json([
            'data_diri' => $data_diri,
            'alamat' => $alamat
        ]);
    });
    Route::get('id-generator', function () {
        return Str::uuid();
    });
    Route::get('fungsi-statis-dinamis', 'Prime\SekolahController@cobaFungsiRepository');
    Route::get('before-action-callback/{id}', 'Prime\SekolahController@destroy');
    Route::get('properti-tambahan', 'ExperimentController@commonJson');
    Route::resource('sekolah', 'Prime\SekolahController');

    Route::post('konversi-json-string', function () {
        $pembiayaan_sekolah = request()->only([
            'id_pembiayaan',           // Value ini sudah pre-defined pada view
            'id_sekolah',              // Value ini sudah pre-defined pada view
            'tahun_ajaran',            // Value ini sudah pre-defined pada view
            'nama_pembiayaan',
            'frekuensi',
            'biaya_dasar',
            'suffix',
            'sasaran_peserta_didik'
        ]);

        dd($pembiayaan_sekolah);
    });
    Route::post('buat-pembiayaan-sekolah/{frek_biaya}', 'ExperimentController@buatPembiayaanSekolah');
    Route::get('semua-kelas', function () {
        $id_sekolah = '7cc9c68c-a056-4507-961d-3e4def4d54d6';
        $pd = new SekolahRepository($id_sekolah);
        dd($pd->semuaKelas());
    });
    Route::get('ambil-siswa-kelas-tertentu', function () {
        $id_sekolah = '7cc9c68c-a056-4507-961d-3e4def4d54d6';
        $pd = new SekolahRepository($id_sekolah);
        dd($pd->daftarSiswa([21]));
    });
    Route::get('abc', function () {
        $ps = PembiayaanSiswa::whereIn('id', ['36aac310-399c-471f-8305-9e839d3d4b21', '469e3bab-a824-4388-bd04-4f4bd564c645'])
            ->get();
        dd($ps[0]->nisn);
    });
    Route::post('test-request', function (Request $request) {
        dd($request, request());
    });
    Route::post('bayar-pembiayaan', 'ExperimentController@bayarPembiayaanSiswa');
    Route::get('lihat-pembayaran/{id}', 'ExperimentController@lihatPembayaran');
    Route::get('update-status-pembiayaan-siswa/{id_transaksi}', 'ExperimentController@updateStatusPembiayaanSiswa');
    Route::get('db-raw-expression/{id_transaksi}', function (Request $request) {
        try {
            $sub = DB::table('ypwi_epayment.rincian_transaksi')
                ->select(DB::raw('left(id_item_pembiayaan, 36) as id_item_pembiayaan, bayar'))
                ->where('id_transaksi', $request->id_transaksi);

            $hasil = DB::table(DB::raw("({$sub->toSql()}) as sub"))
                ->mergeBindings($sub)
                ->select(DB::raw('id_item_pembiayaan, SUM(bayar) as total_dibayar'))
                ->groupBy('id_item_pembiayaan')
                ->get();
    
            return response()->json($hasil);
        } catch (\Throwable $th) {
            throw $th;
        }
    });

    Route::get('lihat-pembiayaan-yang-tersedia/{id_peserta_didik}', 'ExperimentController@daftarPembiayaanYangTersedia');
    Route::get('lihat-daftar-siswa-sekolah/{id_sekolah}', function (Request $request) {
        return response()->json(SubsidiHandler::penerimaSubsidiTahunLalu($request->id_sekolah));
    });

    Route::post('setup-kurikulum-selesai/subsidi-siswa', 'EPayment\SubsidiSiswaController@earlyPhase');
});
