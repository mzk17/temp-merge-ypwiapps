<?php

// Redirect to login page
Route::get('/', function () {
    return view('prime.pages.login');
})->middleware('guest')->name('prime-login');

// Auth::routes();

Route::get('/login', 'AuthController@login')->name('login');
Route::post('/postlogin', 'AuthController@postlogin');
Route::get('/logout', 'AuthController@logout');

/**
 * PRIME ROUTES
 */
Route::middleware('auth')
    ->prefix('prime')
    ->name('prime.')
    ->namespace('Prime')
    ->group(function () {
        Route::get('home', 'MasterController@home')->name('main-home');

        // Complete Registration
        Route::get('/complete-registration', 'UserController@showFormCompleteRegistration')->name('show-fcr');
        Route::post('/complete-registration', 'UserController@completeRegistration')->name('post-complete-registration');
    });

/**
 * ASSET & INVENTORY ROUTES
 */
Route::group(['middleware' => 'auth'], function () {
    Route::get('/dashboard', 'DashboardController@index');

    Route::get('/alat_trans', 'AlatTransaksiController@index');
    Route::post('/alat_trans/create', 'AlatTransaksiController@create');
    Route::get('/alat_trans/{id}/edit', 'AlatTransaksiController@edit');
    Route::post('/alat_trans/{id}/update', 'AlatTransaksiController@update');
    Route::get('/alat_trans/{id}/delete', 'AlatTransaksiController@delete');
    Route::get('/alat_trans/export', 'AlatTransaksiController@export');

    Route::get('/bangunan_trans', 'BangunanTransaksiController@index');
    Route::post('/bangunan_trans/create', 'BangunanTransaksiController@create');
    Route::get('/bangunan_trans/{id}/edit', 'BangunanTransaksiController@edit');
    Route::post('/bangunan_trans/{id}/update', 'BangunanTransaksiController@update');
    Route::get('/bangunan_trans/{id}/delete', 'BangunanTransaksiController@delete');

    Route::get('/barang', 'BarangController@index');
    Route::post('/barang/create', 'BarangController@create');
    Route::get('/barang/{id}/edit', 'BarangController@edit');
    Route::post('/barang/{id}/update', 'BarangController@update');
    Route::get('/barang/{id}/delete', 'BarangController@delete');
    
    Route::get('/jenis', 'JenisController@index');
    Route::post('/jenis/create', 'JenisController@create');
    Route::get('/jenis/{id}/edit', 'JenisController@edit');
    Route::post('/jenis/{id}/update', 'JenisController@update');
    Route::get('/jenis/{id}/delete', 'JenisController@delete');

    Route::get('/karyawan', 'KaryawanController@index');
    Route::post('/karyawan/create', 'KaryawanController@create');
    Route::get('/karyawan/{id}/edit', 'KaryawanController@edit');
    Route::post('/karyawan/{id}/update', 'KaryawanController@update');
    Route::get('/karyawan/{id}/delete', 'KaryawanController@delete');

    Route::get('/kategori', 'KategoriController@index');
    Route::post('/kategori/create', 'KategoriController@create');
    Route::get('/kategori/{id}/edit', 'KategoriController@edit');
    Route::post('/kategori/{id}/update', 'KategoriController@update');
    Route::get('/kategori/{id}/delete', 'KategoriController@delete');

    Route::get('/inventaris', 'InventarisController@index');
    Route::post('/inventaris/create', 'InventarisController@create');
    Route::get('/inventaris/{id}/edit', 'InventarisController@edit');
    Route::post('/inventaris/{id}/update', 'InventarisController@update');
    Route::get('/inventaris/{id}/delete', 'InventarisController@delete');

    Route::get('/ruangan', 'RuanganController@index');
    Route::post('/ruangan/create', 'RuanganController@create');
    Route::get('/ruangan/{id}/edit', 'RuanganController@edit');
    Route::post('/ruangan/{id}/update', 'RuanganController@update');
    Route::get('/ruangan/{id}/delete', 'RuanganController@delete');

    Route::get('/sekolah', 'SekolahController@index');
    Route::post('/sekolah/create', 'SekolahController@create');
    Route::get('/sekolah/{id}/edit', 'SekolahController@edit');
    Route::post('/sekolah/{id}/update', 'SekolahController@update');
    Route::get('/sekolah/{id}/delete', 'SekolahController@delete');
    Route::get('/sekolah/{id}/profile', 'SekolahController@profile');

    Route::get('/supplier', 'SupplierController@index');
    Route::post('/supplier/create', 'SupplierController@create');
    Route::get('/supplier/{id}/edit', 'SupplierController@edit');
    Route::post('/supplier/{id}/update', 'SupplierController@update');
    Route::get('/supplier/{id}/delete', 'SupplierController@delete');

    Route::get('/tanah_trans', 'TanahTransaksiController@index');
    Route::post('/tanah_trans/create', 'TanahTransaksiController@create');
    Route::get('/tanah_trans/{id}/edit', 'TanahTransaksiController@edit');
    Route::post('/tanah_trans/{id}/update', 'TanahTransaksiController@update');
    Route::get('/tanah_trans/{id}/delete', 'TanahTransaksiController@delete');
});
