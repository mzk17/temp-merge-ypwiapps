<?php

use Illuminate\Http\Request;

Route::prefix('adm-wilayah')->group(function () {
    Route::get('daftar-provinsi', 'AlamatController@apiShowProvinceList');
    Route::get('daftar-kota/{previous_id}', 'AlamatController@apiShowDescendantsRegionList');
    Route::get('daftar-kecamatan/{previous_id}', 'AlamatController@apiShowDescendantsRegionList');
    Route::get('daftar-kelurahan/{previous_id}', 'AlamatController@apiShowDescendantsRegionList');
});
