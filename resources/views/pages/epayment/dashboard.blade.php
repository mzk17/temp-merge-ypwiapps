@extends('layouts.epayment.wrapper')
@section('head-style')
@section('app-content')
<div id="epayment-content-wrapper" class="flex-grow overflow-auto flex flex-col items-center">
    <!-- Grafik total pembayaran -->
    <div class="app-section w-3/4">
        <div class="app-card">
            <h1>Dashboard Content</h1>
        </div>
    </div>

    <!-- Riwayat pembayaran -->
    <div class="app-section w-3/4">
        <h1 class="app-card--outer-title">Riwayat Pembayaran</h1>
        <div class="app-card">
            <div class="app-card--table">
                <div class="table-header">
                    <div class="search-input flex-grow">
                        <font-awesome-icon icon="check" />
                    </div>
                    <div class="create-button flex-shrink">
                        Tambah
                    </div>
                </div>
                <div class="table-body my-4">
                    Heyho
                </div>
            </div>
        </div>
    </div>
</div>
@endsection