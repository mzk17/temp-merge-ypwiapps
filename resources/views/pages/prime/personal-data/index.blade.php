@extends('layouts.prime.wrapper')

@section('app-content')
<div class="flex flex-col">
    <h1>Data Personal</h1>
    <div class="w-4/5">
        <div class="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
            <div class="align-middle inline-block min-w-full shadow overflow-hidden sm:rounded-lg border-b border-gray-200">
                <table class="min-w-full">
                    <thead>
                        <tr>
                            <th class="px-6 py-3 border-b border-gray-200 bg-gray-100 text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                Nama
                            </th>
                            <th class="px-6 py-3 border-b border-gray-200 bg-gray-100 text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                JK
                            </th>
                            <th class="px-6 py-3 border-b border-gray-200 bg-gray-100 text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                Alamat
                            </th>
                            <th class="px-6 py-3 border-b border-gray-200 bg-gray-100 text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                Email
                            </th>
                            <th class="px-6 py-3 border-b border-gray-200 bg-gray-100"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($daftar_data_pribadi as $data_pribadi)
                            <tr>
                                <th class="px-6 py-4 whitespace-no-wrap border-b border-gray-200">
                                    <div class="text-sm leading-5 font-medium text-gray-900">
                                        {{ $data_pribadi->nama }}
                                    </div>
                                </th>
                                <th class="px-6 py-4 whitespace-no-wrap border-b border-gray-200">
                                    <div class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full {{ ($data_pribadi->jenis_kelamin == 'L') ? 'bg-green-100 text-green-800' : 'bg-blue-100 text-blue-800' }}">
                                        {{ $data_pribadi->jenis_kelamin }}
                                    </div>
                                </th>
                                <th class="px-6 py-4 whitespace-no-wrap border-b border-gray-200">
                                    <div class="text-sm leading-5 font-medium text-gray-900">
                                        {{ $data_pribadi->daftar_alamat['alamat_utama']->alamat }}
                                    </div>
                                </th>
                                <th class="px-6 py-4 whitespace-no-wrap border-b border-gray-200">
                                    <div class="text-sm leading-5 text-gray-500">
                                        {{ $data_pribadi->email }}
                                    </div>
                                </th>
                                <th class="px-6 py-4 whitespace-no-wrap text-right border-b border-gray-200 text-sm leading-5 font-medium">
                                    <a href="{{ route('prime.personal-data.edit', ['data_pribadi' => $data_pribadi->id]) }}" class="text-indigo-600 hover:text-indigo-900 focus:outline-none focus:underline">
                                        Edit
                                    </a>
                                </th>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <a href="{{ route('prime.main-home') }}" class="button">Dashboard</a>
    </div>
</div>
@endsection