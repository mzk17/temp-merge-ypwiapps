@extends('layouts.prime.wrapper')
@section('app-content')
    <form action="{{ route('prime.tenaga-pendidik.update', ['tenaga_pendidik' => $tenaga_pendidik->id]) }}" method="POST">
        @method('PATCH')
        {{ csrf_field() }}
        @include('components.prime.tenaga-pendidik.tenaga-pendidik-form')
        <hr>
        @include('components.form.data-pribadi', ['mode' => 'update', 'data_pribadi' => $tenaga_pendidik->dataPribadi])
        <hr>
        @include('components.form.alamat', ['alamat' => $tenaga_pendidik->dataPribadi->alamatUtama])
        <hr>
        <button type="submit">Ubah</button>
    </form>
    <a href="{{ route('prime.tenaga-pendidik.index') }}" class="button">Dashboard</a>
@endsection