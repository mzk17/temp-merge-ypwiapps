@extends('layouts.prime.wrapper')

@section('app-content')
<div class="flex flex-col">
    <h1>Data Tenaga Pendidik</h1>
    <div class="w-4/5">
        <div class="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
            <div class="align-middle inline-block min-w-full shadow overflow-hidden sm:rounded-lg border-b border-gray-200">
                <table class="min-w-full">
                    <thead>
                        <tr>
                            <th class="px-6 py-3 border-b border-gray-200 bg-gray-100 text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                Nama
                            </th>
                            <th class="px-6 py-3 border-b border-gray-200 bg-gray-100 text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                JK
                            </th>
                            <th class="px-6 py-3 border-b border-gray-200 bg-gray-100 text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                NIPD
                            </th>
                            <th class="px-6 py-3 border-b border-gray-200 bg-gray-100"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($daftar_tenaga_pendidik as $tenaga_pendidik)
                            <tr>
                                <th class="px-6 py-4 whitespace-no-wrap border-b border-gray-200">
                                    <div class="text-sm leading-5 font-medium text-gray-900">
                                        {{ $tenaga_pendidik->dataPribadi->nama }}
                                    </div>
                                </th>
                                <th class="px-6 py-4 whitespace-no-wrap border-b border-gray-200">
                                    <div class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full {{ ($tenaga_pendidik->jenis_kelamin == 'L') ? 'bg-green-100 text-green-800' : 'bg-blue-100 text-blue-800' }}">
                                        {{ $tenaga_pendidik->dataPribadi->jenis_kelamin }}
                                    </div>
                                </th>
                                <th class="px-6 py-4 whitespace-no-wrap border-b border-gray-200">
                                    <div class="text-sm leading-5 font-medium text-gray-900">
                                        {{ $tenaga_pendidik->nip }}
                                    </div>
                                </th>
                                <th class="px-6 py-4 whitespace-no-wrap text-right border-b border-gray-200 text-sm leading-5 font-medium">
                                    <a href="{{ route('prime.tenaga-pendidik.edit', ['tenaga_pendidik' => $tenaga_pendidik->id]) }}" class="text-indigo-600 hover:text-indigo-900 focus:outline-none focus:underline">
                                        Edit
                                    </a>
                                    <form action="{{ route('prime.tenaga-pendidik.destroy', ['tenaga_pendidik' => $tenaga_pendidik->id]) }}" method="POST">
                                        @method('DELETE')
                                        {{ csrf_field() }}
                                        <button class="button is-danger" onclick="return confirm('Yakin mau dihapus?')" type="submit">Hapus</button>
                                    </form>
                                </th>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <a href="{{ route('prime.main-home') }}" class="button">Dashboard</a>
    </div>
    <form class="w-4/5 flex flex-col" action="{{ route('prime.tenaga-pendidik.store') }}" method="POST">
        {{ csrf_field() }}
        <h1 class="is-size-4">Data Tenaga Pendidik</h1>
        <div>@include('components.prime.tenaga-pendidik.tenaga-pendidik-form')</div>

        <h1 class="is-size-4">Data Pribadi</h1>
        <div>@include('components.form.data-pribadi')</div>

        <h1 class="is-size-4">Data Alamat</h1>
        <div>@include('components.form.alamat')</div>
        <button type="submit">Tambah</button>
    </form>
</div>
@endsection