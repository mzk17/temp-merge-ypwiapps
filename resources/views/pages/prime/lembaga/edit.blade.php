@extends('layouts.prime.wrapper')

@section('app-content')
<h1 class="is-size-4">
    Edit Data Lembaga
</h1>
<!-- Form penambahan Yayasan -->
<form-pembuatan-lembaga
    :post-url="'{{ route('prime.lembaga.update', ['lembaga' => $lembaga->id]) }}'"
    :entity="{{ json_encode($lembaga) }}"
    @if($errors->any())
    :errors="{{ $errors->toJson() }}"
    :old="{
        nama_yayasan: '{{ old('nama_yayasan') }}'
    }"
    @endif>
        {{ csrf_field() }}
        @method('PATCH')
        <template v-slot:address-form>
            @include('components.form.alamat', ['alamat' => $lembaga->alamat])
        </template>
</form-pembuatan-lembaga>
<a class="button" href="{{ route('prime.main-home') }}">Home</a>
@endsection