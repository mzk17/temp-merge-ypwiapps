@extends('layouts.prime.wrapper')

@section('app-content')
<div class="grid-x align-center">
    <div class="cell large-6">
        @error('gagal')
            <b-notification
            type="is-danger"
            aria-close-label="Close notification"
            role="alert">
            {{ $message }}
            </b-notification>
        @enderror
        <div>
            <ol>
                @foreach($daftar_lembaga as $lembaga)
                    <li>{{ $lembaga->nama_yayasan }} - <a href="{{ route('prime.lembaga.edit', ['lembaga' => $lembaga->id]) }}">Edit</a>
                        <form action="{{ route('prime.lembaga.destroy', ['lembaga' => $lembaga->id]) }}" method="POST">
                            @method('DELETE')
                            {{ csrf_field() }}
                            <button class="button is-danger" onclick="return confirm('Yakin mau dihapus?')" type="submit">Hapus</button>
                        </form>
                    </li>
                @endforeach
            </ol>
        </div>

        <!-- Form penambahan Yayasan -->
        <form-pembuatan-lembaga
            :post-url="'{{ route('prime.lembaga.store') }}'"
            @if($errors->any())
                :errors="{{ $errors->toJson() }}"
                :old="{
                    nama_yayasan: '{{ old('nama_yayasan') }}'
                }"
            @endif>
                {{ csrf_field() }}
                <template v-slot:address-form>
                    @include('components.form.alamat')
                </template>
        </form-pembuatan-lembaga>
        <a class="button" href="{{ route('prime.main-home') }}">Home</a>
    </div>
</div>
@endsection