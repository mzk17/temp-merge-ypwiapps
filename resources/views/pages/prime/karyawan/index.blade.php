@extends('layouts.prime.wrapper')

@section('app-content')
    <div class="flex flex-wrap content-center justify-center">
        <div class="flex-col w-1/2">
            <div>
                <ol>
                    @foreach($daftar_karyawan as $karyawan)
                        <li>
                            {{ $karyawan->dataPribadi->nama }} -
                            <a href="{{ route('prime.karyawan.edit', ['karyawan' => $karyawan->id]) }}">Edit</a> -
                            <form action="{{ route('prime.karyawan.destroy', ['karyawan' => $karyawan->id]) }}" method="POST">
                                @method('DELETE')
                                {{ csrf_field() }}
                                <button class="button is-danger" onclick="return confirm('Yakin mau dihapus?')" type="submit">Hapus</button>
                            </form>
                        </li>
                    @endforeach
                </ol>
            </div>
            <form action="{{ route('prime.karyawan.store') }}" method="POST">
                {{ csrf_field() }}
                <h1>Informasi Pribadi</h1>
                @include('components.form.data-pribadi')
                <br>
                @include('components.form.alamat')
                <br>
                <h1>Informasi Kepegawaian</h1>
                @include('components.prime.karyawan.form-kepegawaian', ['karyawan' => null])
                <button class="button" type="submit">Tambah</button>
            </form>
        </div>
    </div>
@endsection