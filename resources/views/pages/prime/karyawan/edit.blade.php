@extends('layouts.prime.wrapper')

@section('app-content')
<h1 class="is-size-4">
    Edit Data Karyawan
</h1>
<form action="{{ route('prime.karyawan.update', ['karyawan' => $karyawan->id]) }}" method="POST">
    @method('PATCH')
    {{ csrf_field() }}
    <h1>Informasi Pribadi</h1>
    @include('components.form.data-pribadi', ['data_pribadi' => $karyawan->dataPribadi, 'mode' => 'update'])
    <br>
    @include('components.form.alamat', ['alamat' => $karyawan->dataPribadi->alamatUtama])
    <br>
    <h1>Informasi Kepegawaian</h1>
    @include('components.prime.karyawan.form-kepegawaian', ['karyawan' => $karyawan])
    <button class="button" type="submit">Simpan</button>
</form>
@endsection