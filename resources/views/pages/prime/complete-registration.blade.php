@extends('layouts.prime.wrapper')

@section('app-content')
<div class="grid-x align-center">
    <div class="grid-y cell small-10 medium-6 large-4">
        <div class="cell text-center margin-top-3 margin-bottom-2">
            <h2>Penyelesaian Pendaftaran</h2>
        </div>
        <div class="cell card margin-bottom-3">
            <complete-registration-form
                :adm-reg="{{ $admReg }}"
                :route-url="'{{ route('prime.post-complete-registration') }}'">
                @csrf
            </complete-registration-form>
        </div>
    </div>
</div>
@endsection