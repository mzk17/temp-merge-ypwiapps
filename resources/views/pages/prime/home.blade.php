@extends('layouts.prime.wrapper')

@section('app-content')
<div class="flex flex-col">
  <div class="flex flex-row px-8">
    <!-- Left -->
    <div class="flex-shrink">
      <h1>YPWI Apps</h1>
    </div>
    <!-- Mid - Vue Component -->
    <div class="flex-grow text-center">
      <h3>02 jam 45 menit waktu Sholat Maghrib</h3>
    </div>
    <!-- Right - Vue Component -->
    <div class="flex-shrink">
      <form action="{{ route('logout') }}" method="POST">
        @csrf
        <button type="submit" class="button is-danger">Logout</button>
      </form>
    </div>
  </div>
  <div class="px-8">
    <a href="{{ route('asset-inventory.dashboard') }}">Aset dan Inventaris</a>
    |
    <a href="{{ route('prime.sekolah.index') }}">Modul Sekolah</a>
    |
    <a href="{{ route('prime.lembaga.index') }}">Modul Lembaga / Yayasan</a>
    |
    <a href="{{ route('prime.karyawan.index') }}">Modul Karyawan</a>
    |
    <a href="{{ route('prime.tenaga-pendidik.index') }}">Modul Tenaga Pendidik (TENDIK)</a>
    |
    <a href="{{ route('prime.peserta-didik.index') }}">Peserta Didik</a>
    <!-- crg = Complete Registration -->
    @if(session()->has('crg_message'))
    <strong>{{ session()->get('crg_message') }}</strong>
    @endif
  </div>
</div>
@endsection