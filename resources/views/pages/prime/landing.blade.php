@extends('layouts.prime.wrapper')

@section('app-content')
<div class="flex content-center flex-wrap justify-center h-screen">
  <div class="flex flex-col text-center">
    <h1 class="login-title">YPWI Apps</h1>
    <div class="mt-8">
      <a class="button bg-blue-400 rounded py-3 px-4 mx-2" href="{{ route('login') }}">Login</a>
      <a class="button bg-blue-400 rounded py-3 px-4 mx-2" href="{{ route('pre-register') }}">Register</a>
    </div>
  </div>
</div>
    
@endsection