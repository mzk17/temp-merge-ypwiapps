@extends('layouts.prime.wrapper')

@section('app-content')
<div class="grid-x align-center">
    <div class="grid-y cell large-6">
        <!-- Notifikasi -->
        <div class="cell">
            @error('gagal')
            <b-notification type="is-danger" aria-close-label="Close notification" role="alert">
                {{ $message }}
            </b-notification>
            @enderror

            @if (session('berhasil'))
            <b-notification type="is-success" aria-close-label="Close notification" role="alert">
                {{ session('berhasil') }}
            </b-notification>
            @endif
        </div>

        <!-- Daftar Sekolah -->
        <div class="cell">
            <h3>Daftar Sekolah</h3>
            <table>
                @foreach($daftar_sekolah as $sekolah)
                <tr>
                    <td>{{ $sekolah->nama_sekolah }}</td>
                    <td>
                        <a class="button" href="{{ route('prime.sekolah.show', ['sekolah' => $sekolah->id]) }}">Lihat</a>
                    </td>
                    <td>
                        <a class="button" href="{{ route('prime.sekolah.edit', ['sekolah' => $sekolah->id]) }}">Edit</a>
                    </td>
                    <td>
                        <form action="{{ route('prime.sekolah.destroy', ['sekolah' => $sekolah->id]) }}" method="POST">
                            @method('DELETE')
                            {{ csrf_field() }}
                            <button class="button is-danger" onclick="return confirm('Yakin mau dihapus?')" type="submit">Hapus</button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </table>
        </div>

        <!-- Form penambahan Sekolah -->
        <div class="cell">
            <creation-form
                mode="create"
                :post-url="'{{ route('prime.sekolah.store') }}'"
                :school-level="{{ json_encode($tingkatan_sekolah) }}"
                @if($errors->any())
                    :errors="{{ $errors->toJson() }}"
                    :old="{
                        nama_sekolah: '{{ old('nama_sekolah') }}',
                        id_tingkat_sekolah: '{{ old('id_tingkat_sekolah') }}'
                    }"
                @endif>
                {{ csrf_field() }}
                <template v-slot:address-form>
                    @include('components.form.alamat')
                </template>
            </creation-form>
        </div>
        <a href="{{ route('prime.main-home') }}">Home</a>
    </div>
</div>
@endsection