@extends('layouts.prime.wrapper')

@section('app-content')
<h1>Edit Data Sekolah</h1>
<h3>{{ $sekolah->nama_sekolah }}</h3>
<creation-form
    mode="update"
    post-url="{{ route('prime.sekolah.update', ['sekolah' => $sekolah->id]) }}"
    :entity="{{ json_encode($sekolah) }}"
    :school-level="{{ json_encode($tingkatan_sekolah) }}"
    @if($errors->any())
        :errors="{{ $errors->toJson() }}"
        :old="{
            nama_sekolah: '{{ old('nama_sekolah') }}',
            id_tingkat_sekolah: '{{ old('id_tingkat_sekolah') }}'
        }"
    @endif>
    {{ csrf_field() }}
    <template v-slot:address-form>
        @include('components.form.alamat', ['alamat' => $sekolah->alamat])
    </template>
</creation-form>
@endsection