@extends('layouts.prime.wrapper')
@section('app-content')
    <form action="{{ route('prime.peserta-didik.update', ['peserta_didik' => $peserta_didik->id]) }}" method="POST">
        @method('PATCH')
        {{ csrf_field() }}
        @include('components.prime.peserta-didik.peserta-didik-form')
        <hr>
        @include('components.form.data-pribadi', ['mode' => 'update', 'data_pribadi' => $peserta_didik->dataPribadi])
        <hr>
        @include('components.form.alamat', ['alamat' => $peserta_didik->dataPribadi->alamatUtama])
        <hr>
        <button type="submit">Ubah</button>
    </form>
    <a href="{{ route('prime.peserta-didik.index') }}" class="button">Dashboard</a>
@endsection