@extends('layouts.master')

@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Edit Data Jenis Tanah</h3>
                        </div>
                        <div class="panel-body">
                            <form action="{{ route('asset-inventory.jenis-tanah.update', ['id' => $jenis->id]) }}" method="POST">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nama Barang</label>
                                    <input name="jenis_tanah" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Jenis Tanah" value="{{$jenis->jenis_tanah}}" required>
                                </div>
                                <button type="submit" class="btn btn-warning">Update Data</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop