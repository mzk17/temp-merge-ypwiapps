@extends('layouts.master')
@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Data Jenis Tanah</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <table id="example1" class="table table-bordered table-striped">
            <div class="btn btn-sm right">
                <button type="button" class="btn btn-sm btn-block btn-outline-primary" data-toggle="modal" data-target="#exampleModal">Tambah Data Jenis Tanah
                </button>
            </div>
            <thead>
                <tr>
                    <th>Jenis Tanah</th>
                    <th width="5%">Update</th>
                    <th width="10%">Delete</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data_jenis as $jenis)
                <tr>
                    <td>{{$jenis->jenis_tanah}}</td>
                    <td width="5%"><a href="{{ route('asset-inventory.jenis-tanah.edit', ['id' => $jenis->id]) }}" class="btn btn-warning btn-sm">Edit</a></td>
                    <td width="10%"><a href="{{ route('asset-inventory.jenis-tanah.delete', ['id' => $jenis->id]) }}" class="btn btn-danger btn-sm" onclick="return confirm('Yakin mau dihapus?')">Hapus</a></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">TAMBAH DATA JENIS TANAH</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('asset-inventory.jenis-tanah.create') }}" method="POST">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="exampleInputEmail1">Jenis Tanah</label>
                        <input name="jenis_tanah" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Isikan jenis tanah" required>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>

                </form>
            </div>
        </div>
    </div>
    @stop