@extends('layouts.master')
@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Edit Data Bangunan</h3>
                        </div>
                        <div class="panel-body">
                            <form action="{{ route('asset-inventory.transaksi.bangunan.update', ['bangunan' => $bangunan_trans->id]) }}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                @method('PATCH')
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Kategori</label>
                                    <input name="kategori_id" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$bangunan_trans->kategori->sub_kategori}}" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Aset Alat</label>
                                    <input name="barang_id" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$bangunan_trans->ruangan->nama_ruangan}}" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Sekolah / Yayasan</label>
                                    <input name="sekolah_id" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$bangunan_trans->sekolah->nama_sekolah}}" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Kode Bangunan</label>
                                    <input name="kode_bangunan" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$bangunan_trans->kode_alat}}">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nomor Registrasi Bangunan</label>
                                    <input name="no_reg_bangunan" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$bangunan_trans->no_reg_bangunan}}">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Kondisi Bangunan</label>
                                    <input name="kondisi_bangunan" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$bangunan_trans->kondisi_bangunan}}" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Konstruksi Bangunan Bertingkat</label>
                                    <input name="konstruksi_bangunan_bertingkat" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$bangunan_trans->konstruksi_bangunan_bertingkat}}">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Konstruksi Bangunan Beton</label>
                                    <input name="konstruksi_bangunan_beton" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$bangunan_trans->konstruksi_bangunan_beton}}">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"> Luas tanah </label>
                                    <input name="luas_tanah" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$bangunan_trans->luas_tanah}}" >
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"> Letak / Lokasi </label>
                                    <input name="letak_lokasi" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$bangunan_trans->letak_lokasi}}" >
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"> Tanggal Dokumen </label>
                                    <input name="tanggal_dokumen" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$bangunan_trans->tanggal_dokumen}}" >
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"> Nomor Dokumen </label>
                                    <input name="nomor_dokumen" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$bangunan_trans->nomor_dokumen}}" >
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"> Luas Lantai </label>
                                    <input name="luas_lantai" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$bangunan_trans->luas_lantai}}">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"> Status Tanah </label>
                                    <input name="status_tanah" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$bangunan_trans->status_tanah}}" >
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"> Nomor Kode Tanah </label>
                                    <input name="nomor_kode_tanah" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$bangunan_trans->nomor_kode_tanah}}" >
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"> Asal Usul </label>
                                    <input name="asal_usul" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$bangunan_trans->asal_usul}}" >
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"> Harga </label>
                                    <input name="harga" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$bangunan_trans->harga}}" >
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"> Sumber Dana </label>
                                    <input name="sumber_dana" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$bangunan_trans->sumber_dana}}" disabled>
                                </div>
                                <div class="form-group">
                                    <label name="keterangan" for="exampleFormControlTextarea1" >Keterangan</label>
                                    <textarea name="keterangan" class="form-control" id="exampleFormControlTextarea1" rows="3" >{{$bangunan_trans->keterangan}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlFile1">Masukkan Foto Nota/Bangunan (bila ada)</label>
                                    <input name="foto_avatar" type="file" value="{{$bangunan_trans->foto_avatar}}" class="form-control-file" id="exampleFormControlFile1">
                                </div>
                                <button type="submit" class="btn btn-warning">Update Data</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop