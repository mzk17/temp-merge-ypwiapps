@extends('layouts.master')

@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Data Aset Bangunan</h3>
    </div>                
    
    <div class="card-body ">
        <a href="/bangunan_trans/export" class="btn btn-sm btn-success right">Export Data</a>
        <table id="example1" class="table table-bordered table-striped ">
            <div class="btn btn-sm right">
                <button type="button" class="btn btn-block btn-sm btn-outline-primary" data-toggle="modal" data-target="#exampleModal">Tambah Data Bangunan                             
                </button>
            </div>
                <thead>
                <tr>
                    <th>Nama Bangunan </th>
                    <th>Luas Bangunan</th>
                    <th>Letak/Lokasi</th>
                    <th>Tanggal Dokumen</th>
                    <!--Kondisi</!--th-->                                                
                    <th width="14%">Harga</th>
                    <th>Foto Ruangan/Bangunan</th>
                    <th>Keterangan</th>
                    <th>Update</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data_bangunan_trans as $bangunan_trans)                                            
                <tr>                                                
                    <td>{{$bangunan_trans->ruangan->nama_ruangan}}</td>
                    <td>{{formatVolume($bangunan_trans->luas_tanah)}}</td>
                    <td>{{$bangunan_trans->letak_lokasi}}</td>
                    <td>{{$bangunan_trans->tanggal_dokumen}}</td>
                    <!--td--$alat_trans->kondisi<td-->            
                    <td width="14%">{{formatRupiah($bangunan_trans->harga)}}</td>
                    <td><img height="96" src="{{asset('images/'.$bangunan_trans->foto_avatar)}}"></td>
                    <td>{{$bangunan_trans->keterangan}}</td>
                    <td><a href="{{ route('asset-inventory.transaksi.bangunan.edit', ['bangunan' => $bangunan_trans->id]) }}" class="btn btn-warning btn-sm">Edit</a></td>
                    <td>
                        <form action="{{ route('asset-inventory.transaksi.bangunan.destroy', ['bangunan' => $bangunan_trans->id]) }}" method="POST">
                            {{ csrf_field() }}
                            @method('DELETE')
                            <button class="btn btn-danger btn-sm" onclick="return confirm('Yakin mau dihapus?')">Hapus</button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">TAMBAH DATA BANGUNAN</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('asset-inventory.transaksi.bangunan.store') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">SUB KATEGORI</label>
                        <select name="kategori_id" class="form-control" id="exampleFormControlSelect1">
                        <option disabled selected> Pilih kategori </option>
                        @foreach($c_kategori as $kat)
                            <option value="{{$kat->id}}">{{$kat->sub_kategori}}</option>
                        @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">NAMA SEKOLAH</label>
                        <select name="sekolah_id" class="form-control" id="exampleFormControlSelect1">
                        <option disabled selected> Pilih nama sekolah / Yayasan </option>
                        @foreach($c_sekolah as $skl)
                            <option value="{{$skl->id}}">{{$skl->nama_sekolah}}</option>
                        @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">NAMA BANGUNAN / RUANGAN</label>
                        <select name="ruangan_id" class="form-control" id="exampleFormControlSelect1">
                        <option disabled selected> Pilih nama Bangunan/ Ruangan </option>
                        @foreach($c_ruangan as $rng)
                            <option value="{{$rng->id}}">{{@$rng->nama_ruangan}}</option>
                        @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">LUAS BANGUNAN / RUANGAN  </label>
                        <input name="luas_tanah" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Cukup tuliskan angka saja, misal 100" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">LETAK LOKASI</label>
                        <input name="letak_lokasi" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Isikan lokasi, misal Makassar">
                    </div>
                    <div class="form-group">
                        <label>Tanggal Dokumen:</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                            </div>
                            <input name="tanggal_dokumen" type="text" class="form-control" id="user1" >
                        </div>
                    </div> 
                    <div class="form-group">
                        <label for="exampleInputEmail1">HARGA  </label>
                        <input name="harga" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Cukup tuliskan angka saja, misal 100000" required>
                    </div>                        
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">SUMBER DANA</label>
                        <select name="sumber_dana" class="form-control" id="exampleFormControlSelect1">
                        <option disabled selected> Pilih Sumber Dana</option>
                            <option value="Dana Yayasan">Dana Yayasan</option>
                            <option value="Dana BOS">Dana BOS Sekolah</option>
                            <option value="Dana Pribadi">Dana Pribadi</option>
                            <option value="Pinjaman Bank">Pinjaman Bank</option>
                            <option value="Sumbangan Masyarakat">Sumbangan Masyarakat</option>
                            <option value="Wakaf">Wakaf</option>                                
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">KONDISI</label>
                        <select name="kondisi_bangunan" class="form-control" id="exampleFormControlSelect1">
                        <option disabled selected> Pilih kondisi Bangunan/Ruangan </option>
                            <option value="Baik">Baik</option>
                            <option value="Kurang Baik">Kurang Baik</option>
                            <option value="Rusak Berat">Rusak Berat</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">KETERANGAN</label>
                        <textarea name="keterangan" class="form-control" id="exampleFormControlTextarea1" rows="3" required></textarea>
                    </div>                        
                    <!--   <div class="form-group">
                        <label for="exampleFormControlFile1">Masukkan Foto/Nota Ruangan / Bangunan (bila ada)</label>
                        <input name="foto_avatar" type="file" class="form-control-file" id="exampleFormControlFile1">
                    </div> -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    
                </form>
            </div>
        </div>
    </div>
</div>
@stop