@extends('layouts.master')
@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Edit Data Aset Tanah</h3>
                        </div>
                        <div class="panel-body">
                            <form action="{{ route('asset-inventory.transaksi.tanah.update', ['tanah' => $tanah_trans->id]) }}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                @method('PATCH')
                                <div class="form-group">
                                    <label for="kategori">Kategori</label>
                                    <input name="kategori_id" type="text" class="form-control" id="kategori" 
                                    aria-describedby="emailHelp" value="{{$tanah_trans->kategori->sub_kat}}" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="aset_tanah">Aset Tanah</label>
                                    <input name="jenis_id" type="text" class="form-control" id="aset_tanah" 
                                    aria-describedby="emailHelp" value="{{$tanah_trans->jenis->jenis_tanah}}" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="sekolah-yayasan">Sekolah / Yayasan</label>
                                    <input name="sekolah_id" type="text" class="form-control" id="sekolah-yayasan" 
                                    aria-describedby="emailHelp" value="{{$tanah_trans->sekolah->nama_sekolah}}" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="kode-tanah">Kode Tanah</label>
                                    <input name="kode_tanah" type="text" class="form-control" id="kode-tanah" 
                                    aria-describedby="emailHelp" value="{{$tanah_trans->kode_tanah}}">
                                </div>
                                <div class="form-group">
                                    <label for="no-reg-tanah">Nomor Registrasi Tanah</label>
                                    <input name="no_reg_tanah" type="text" class="form-control" id="no-reg-tanah" 
                                    aria-describedby="emailHelp" value="{{$tanah_trans->no_reg_tanah}}">
                                </div>
                                <div class="form-group">
                                    <label for="luas-tanah">Luas Tanah</label>
                                    <input name="luas" type="text" class="form-control" id="luas-tanah" 
                                    aria-describedby="emailHelp" value="{{$tanah_trans->luas}}" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="tahun-pengadaan">Tahun Pengadaan</label>
                                    <input name="tahun_pengadaan" type="text" class="form-control" id="tahun-pengadaan" 
                                    aria-describedby="emailHelp" value="{{$tanah_trans->tahun_pengadaan}}">
                                </div>
                                <div class="form-group">
                                    <label for="letak-alamat-tanah">Letak Alamat Tanah</label>
                                    <input name="letak_alamat" type="text" class="form-control" id="letak-alamat-tanah" 
                                    aria-describedby="emailHelp" value="{{$tanah_trans->letak_alamat}}">
                                </div>
                                <div class="form-group">
                                    <label for="status-hak-tanah"> Status Hak Tanah </label>
                                    <input name="stts_tanah_hak" type="text" class="form-control" id="status-hak-tanah" 
                                    aria-describedby="emailHelp" value="{{$tanah_trans->stts_tanah_hak}}" >
                                </div>
                                <div class="form-group">
                                    <label for="status-tanggal-sertifikat-tanah"> Status Tanggal Sertifikat Tanah </label>
                                    <input name="stts_tanah_tglsertifikat" type="text" class="form-control" id="status-tanggal-sertifikat-tanah" 
                                    aria-describedby="emailHelp" value="{{$tanah_trans->stts_tanah_tglsertifikat}}" >
                                </div>
                                <div class="form-group">
                                    <label for="status-nomor-sertifikat-tanah"> Status Nomor Sertifikat Tanah </label>
                                    <input name="stts_tanah_nmrsertifikat" type="text" class="form-control" id="status-nomor-sertifikat-tanah" 
                                    aria-describedby="emailHelp" value="{{$tanah_trans->stts_tanah_nmrsertifikat}}" >
                                </div>
                                <div class="form-group">
                                    <label for="penggunaan"> Penggunaan </label>
                                    <input name="penggunaan" type="text" class="form-control" id="penggunaan" 
                                    aria-describedby="emailHelp" value="{{$tanah_trans->penggunaan}}" >
                                </div>
                                <div class="form-group">
                                    <label for="sumber-dana"> Sumber Dana </label>
                                    <input name="sumber_dana" type="text" class="form-control" id="sumber-dana" 
                                    aria-describedby="emailHelp" value="{{$tanah_trans->sumber_dana}}" disabled>
                                </div>                                    
                                <div class="form-group">
                                    <label for="harga"> Harga </label>
                                    <input name="harga" type="text" class="form-control" id="harga" 
                                    aria-describedby="emailHelp" value="{{$tanah_trans->harga}}" >
                                </div>
                                <div class="form-group">
                                    <label name="keterangan" for="exampleFormControlTextarea1" >Keterangan</label>
                                    <textarea name="keterangan" class="form-control" id="exampleFormControlTextarea1" 
                                    rows="3" >{{$tanah_trans->keterangan}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="foto-tanah">Masukkan Foto Nota/Tanah (bila ada)</label>
                                    <input name="foto_tnh" type="file" value="{{$tanah_trans->foto_tnh}}" 
                                    class="form-control-file" id="foto-tanah">
                                </div>
                                <button type="submit" class="btn btn-warning">Update Data</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop