@extends('layouts.master')

@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Data Aset Tanah</h3>
    </div>                
    <!-- /.card-header -->
    <div class="card-body ">
        <a href="/tanah_trans/export" class="btn btn-sm btn-success right"> Export Data </a>
        <table id="example1" class="table table-bordered table-striped ">
            <div class="btn btn-sm right">
                <button type="button" class="btn btn-block btn-sm btn-outline-primary" data-toggle="modal" data-target="#exampleModal">
                    Tambah Data Tanah
                </button>
            </div>
            <thead>
                <tr>
                    <th>Aset Tanah </th>
                    <th>Luas Tanah</th>
                    <th>Letak/Lokasi</th>
                    <th>Tahun Pengadaan</th>
                    <!--Kondisi</!--th-->                                                
                    <th width="14%">Harga</th>
                    <th>Foto </th>
                    <th>Keterangan</th>
                    <th>Update</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data_tanah_trans as $tanah_trans)                                            
                <tr>                                                
                    <td>{{ $tanah_trans->jenis->jenis_tanah }}</td>
                    <td>{{ formatVolume($tanah_trans->luas) }}</td>
                    <td>{{ $tanah_trans->letak_alamat }}</td>
                    <td>{{ $tanah_trans->tahun_pengadaan }}</td>
                    <!--td--$tanah_trans->kondisi<td-->            
                    <td width="14%">{{formatRupiah($tanah_trans->harga)}}</td>
                    <td><img height="96" src="{{asset('images/'.$tanah_trans->foto_tnh)}}"></td>
                    <td>{{ $tanah_trans->keterangan }}</td>
                    <td><a href="{{ route('asset-inventory.transaksi.tanah.edit', ['tanah' => $tanah_trans->id]) }}" class="btn btn-warning btn-sm">Edit</a></td>
                    <td>
                        <form action="{{ route('asset-inventory.transaksi.tanah.destroy', ['tanah' => $tanah_trans->id]) }}" method="POST">
                            {{ csrf_field() }}
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Yakin mau dihapus?')">Hapus</button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">TAMBAH DATA TANAH</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <div class="modal-body">
            <form action="{{ route('asset-inventory.transaksi.tanah.store') }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="exampleFormControlSelect1">SUB KATEGORI</label>
                    <select name="kategori_id" class="form-control" id="exampleFormControlSelect1">
                        <option disabled selected> Pilih kategori </option>
                        @foreach($c_kategori as $kat)
                            <option value="{{ $kat->id }}">{{ $kat->sub_kategori }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect1">NAMA SEKOLAH</label>
                    <select name="sekolah_id" class="form-control" id="exampleFormControlSelect1">
                        <option disabled selected> Pilih nama sekolah / Yayasan </option>
                        @foreach($c_sekolah as $skl)
                            <option value="{{ $skl->id }}">{{ $skl->nama_sekolah }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect1">JENIS ASET TANAH</label>
                    <select name="jenis_tanah_id" class="form-control" id="exampleFormControlSelect1">
                        <option disabled selected> Pilih jenis aset tanah </option>
                        @foreach($c_jenis as $jns)
                            <option value="{{ $jns->id }}">{{@$jns->jenis_tanah}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">LUAS TANAH </label>
                    <input name="luas" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Cukup tuliskan angka saja, misal 100" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">LETAK TANAH</label>
                    <input name="letak_alamat" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Isikan lokasi, misal Makassar">
                </div>
                <div class="form-group">
                    <label>Tahun Pengadaan:</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                        </div>
                        <input name="tahun_pengadaan" type="text" class="form-control" id="exampleInputEmail1" placeholder="Cukup tuliskan tahun angka, misal 2020" required >
                    </div>
                </div> 
                <div class="form-group">
                    <label for="exampleInputEmail1">HARGA  </label>
                    <input name="harga" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Cukup tuliskan angka saja, misal 100000" required>
                </div>                        
                <div class="form-group">
                    <label for="exampleFormControlSelect1">SUMBER DANA</label>
                    <select name="sumber_dana" class="form-control" id="exampleFormControlSelect1">
                    <option disabled selected> Pilih Sumber Dana</option>
                        <option value="Dana Yayasan">Dana Yayasan</option>
                        <option value="Dana BOS">Dana BOS Sekolah</option>
                        <option value="Dana Pribadi">Dana Pribadi</option>
                        <option value="Pinjaman Bank">Pinjaman Bank</option>
                        <option value="Sumbangan Masyarakat">Sumbangan Masyarakat</option>
                        <option value="Wakaf">Wakaf</option>                                
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">KETERANGAN</label>
                    <textarea name="keterangan" class="form-control" id="exampleFormControlTextarea1" rows="3" required></textarea>
                </div>                        
                <!--   <div class="form-group">
                    <label for="exampleFormControlFile1">Masukkan Foto/Nota Ruangan / Bangunan (bila ada)</label>
                    <input name="foto_avatar" type="file" class="form-control-file" id="exampleFormControlFile1">
                </div> -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>
@stop