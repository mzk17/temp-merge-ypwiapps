@extends('layouts.master')

@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Edit Data Kategori</h3>
                        </div>
                        <div class="panel-body">
                            <form action="{{ route('asset-inventory.kategori.update', ['kategori' => $kategori->id]) }}" method="POST">
                                {{ csrf_field() }}
                                @method('PATCH')
                                <div class="form-group">
                                    <label for="nama_kategori">NAMA KATEGORI</label>
                                    <input name="kategori" type="text" class="form-control" id="nama_kategori" aria-describedby="emailHelp" placeholder="Nama Kategori" value="{{ $kategori->kategori }}" required>
                                </div>
                                <div class="form-group">
                                    <label for="sub_kategori">SUB KATEGORI</label>
                                    <input name="sub_kategori" type="text" class="form-control" id="sub_kategori" aria-describedby="emailHelp" placeholder="Sub Kategori" value="{{ $kategori->sub_kategori }}" required>
                                </div>
                                <button type="submit" class="btn btn-warning">Update Data</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop