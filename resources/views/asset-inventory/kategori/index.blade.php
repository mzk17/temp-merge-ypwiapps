@extends('layouts.master')
@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Data Kategori</h3>
    </div>                

    <div class="card-body ">                    
        <table id="example1" class="table table-bordered table-striped ">
            <div class="btn btn-sm right">
                <button type="button" class="btn btn-sm btn-block btn-outline-primary" data-toggle="modal" data-target="#tambah_kategori_modal">
                    Tambah Data Kategori
                </button>
            </div>
            <thead>
                <tr>
                    <th>Kode Kategori</th>
                    <th>Nama Kategori</th>
                    <th width="5%">AKSI</th>
                    <th width="10%"></th>
                </tr>
            </thead>
            <tbody>
                @foreach($data_kategori as $kategori)
                <tr>
                    <td> {{ $kategori->kategori }} </td>
                    <td> {{ $kategori->sub_kategori }} </td>
                    <td width="5%"><a href="{{ route('asset-inventory.kategori.edit', ['kategori' => $kategori->id]) }}" class="btn btn-warning btn-sm">Edit</a></td>
                    <td width="10%">
                        <form action="{{ route('asset-inventory.kategori.destroy', ['kategori' => $kategori->id]) }}" method="POST">
                            {{ csrf_field() }}
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Yakin mau dihapus?')">Hapus</button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="tambah_kategori_modal" tabindex="-1" role="dialog" aria-labelledby="tambah_kategori_modal_label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="tambah_kategori_modal_label">TAMBAH DATA KATEGORI</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form action="{{ route('asset-inventory.kategori.store') }}" method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="kategori">NAMA KATEGORI</label>
                    <input name="kategori" type="text" class="form-control" id="kategori" placeholder="Isikan nama kategori" required>
                </div>
                <div class="form-group">
                    <label for="sub_kategori">SUB KATEGORI</label>
                    <input name="sub_kategori" type="text" class="form-control" id="sub_kategori" placeholder="Isikan nama sub kategori" required>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>
@stop