@extends('layouts.master')
@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Data Ruangan/ Bangunan</h3>
    </div>
    
    <div class="card-body">
        <table id="example1" class="table table-bordered table-striped">
            <div class="btn btn-sm right">
                <button type="button" class="btn btn-block btn-sm btn-outline-primary" data-toggle="modal" data-target="#tambah_ruangan">
                    Tambah Data Bangunan / Ruangan
                </button>
            </div>
            <thead>
                <tr>
                    <th>Nama Bangunan / Ruangan</th>
                    <th width="5%">Update</th>
                    <th width="10%">Delete</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data_ruangan as $ruangan)
                <tr>
                    <td>{{$ruangan->nama_ruangan}}</td>
                    <td width="5%"><a href="{{ route('asset-inventory.ruangan.edit', ['ruangan' => $ruangan->id]) }}" class="btn btn-warning btn-sm">Edit</a></td>
                    <td width="10%">
                        <form action="{{ route('asset-inventory.ruangan.destroy', ['ruangan' => $ruangan->id]) }}" method="POST">
                            {{ csrf_field() }}
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Yakin mau dihapus?')">Hapus</a>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="tambah_ruangan" tabindex="-1" role="dialog" aria-labelledby="Tambah Ruangan" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="Tambah Ruangan">TAMBAH DATA BANGUNAN / RUANGAN</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form action="{{ route('asset-inventory.ruangan.store') }}" method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="exampleInputEmail1">Nama Bangunan / Ruangan</label>
                    <input name="nama_ruangan" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Isikan nama bangunan / ruangan" required>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>
@stop