@extends('layouts.master')

@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Edit Data Bangunan / Ruangan</h3>
                        </div>
                        <div class="panel-body">
                            <form action="{{ route('asset-inventory.ruangan.update', ['ruangan' => $ruangan->id]) }}" method="POST">
                                {{csrf_field()}}
                                @method('PATCH')
                                <div class="form-group">
                                    <label for="nama_ruangan">Nama Ruangan</label>
                                    <input name="nama_ruangan" type="text" class="form-control" id="nama_ruangan" placeholder="Nama bangunan / ruangan" value="{{ $ruangan->nama_ruangan }}" required>
                                </div>
                                <button type="submit" class="btn btn-warning">Update</button>
                                <a href="{{ route('asset-inventory.ruangan.index') }}" class="btn">Kembali</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop