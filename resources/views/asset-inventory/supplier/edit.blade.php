@extends('layouts.master')

@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Edit Data Supplier</h3>
                        </div>
                        <div class="panel-body">
                            <form action="{{ route('asset-inventory.supplier.update', ['supplier' => $supplier->id]) }}" method="POST">
                                {{ csrf_field() }}
                                @method('PATCH')
                                <div class="form-group">
                                    <label for="nama_supplier">NAMA SUPPLIER</label>
                                    <input name="nama_supplier" type="text" class="form-control" id="nama_supplier" placeholder="Nama Supplier" value="{{$supplier->nama_supplier}}" required>
                                </div>
                                <div class="form-group">
                                    <label name="alamat" for="alamat_supplier">ALAMAT</label>
                                    <textarea name="alamat_supplier" class="form-control" id="alamat_supplier" rows="3" required>{{ $supplier->alamat_supplier }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="telepon_supplier">TELEPON</label>
                                    <input name="telepon_supplier" type="text" class="form-control" id="telepon_supplier" placeholder="Masukkan nomor telepon" value="{{ ($supplier->telepon_supplier) }}" required>
                                </div>
                                <div class="form-group">
                                    <label for="email_supplier">EMAIL</label>
                                    <input name="email_supplier" type="text" class="form-control" id="email_supplier" placeholder="Masukkan alamat email" value="{{( $supplier->email_supplier) }}" required>
                                </div>                                    
                                <!--div-- class="form-group">
                                    <label for="exampleFormControlFile1">Masukkan Foto/Nota Supplier (bila ada)</label>
                                    <input name="foto_supplier" type="file" value="{{$supplier->foto_supplier}}" class="form-control-file" id="exampleFormControlFile1">
                                </!--div-->
                                <button type="submit" class="btn btn-warning">Update</button>
                                <a class="btn" href="{{ route('asset-inventory.supplier.index') }}">Kembali</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop