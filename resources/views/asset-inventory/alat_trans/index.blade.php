@extends('layouts.master')

@section('content')

<!-- Main content -->
<section class="card">
    <div class="row">
        <div class="col-12">
            <div class="card-header">
                <h3 class="card-title">Data Aset Alat dan Mesin</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body ">
                <a href="{{ route('asset-inventory.transaksi.alat.export') }}" class="btn btn-sm btn-success right"> Export Data </a>
                <table id="example1" class="table table-bordered table-striped ">
                    <div class="btn btn-sm right">
                        <button type="button" class="btn btn-block btn-sm btn-outline-primary" data-toggle="modal" data-target="#exampleModal">Tambah Data Aset
                        </button>
                    </div>
                    <thead>
                        <tr>
                            <th>Nama Aset/ Barang </th>
                            <th>Merk/Tipe</th>
                            <th>Spesifikasi</th>
                            <th>Tahun Pembelian</th>
                            <!--Kondisi</!--th-->
                            <th width="14%">Harga</th>
                            <th>Foto</th>
                            <th>Keterangan</th>
                            <th>Update</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data_alat_trans as $alat_trans)
                        <tr>
                            <td>{{$alat_trans->barang->nama_barang}}</td>
                            <td>{{$alat_trans->merek_tipe}}</td>
                            <td>{{$alat_trans->ukuran_cc}}</td>
                            <td>{{$alat_trans->thn_pembelian}}</td>
                            <!--td--$alat_trans->kondisi<td-->
                            <!--td>formatVolume$alat_trans->luas</!td-->
                            <td width="14%">{{formatRupiah($alat_trans->harga)}}</td>
                            <td><img height="96" src="{{asset('images/'.$alat_trans->fotoavatar)}}"></td>
                            <td>{{$alat_trans->keterangan}}</td>
                            <td><a href="{{ route('asset-inventory.transaksi.alat.edit', ['alat' => $alat_trans->id]) }}" class="btn btn-warning btn-sm">Edit</a></td>
                            <td>
                                <form action="{{ route('asset-inventory.transaksi.alat.destroy', ['alat' => $alat_trans->id]) }}" method="POST">
                                    {{ csrf_field() }}
                                    @method('DELETE')
                                    <button class="btn btn-danger btn-sm" onclick="return confirm('Yakin mau dihapus?')">Hapus</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">TAMBAH DATA ASET ALAT/MESIN</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('asset-inventory.transaksi.alat.store') }}" method="POST" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">SUB KATEGORI</label>
                                <select name="kategori_id" class="form-control" id="exampleFormControlSelect1">
                                    <option disabled selected> Pilih kategori </option>
                                    @foreach($c_kategori as $kat)
                                    <option value="{{$kat->id}}">{{$kat->sub_kategori}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">NAMA SEKOLAH</label>
                                <select name="sekolah_id" class="form-control" id="exampleFormControlSelect1">
                                    <option disabled selected> Pilih nama sekolah / Yayasan </option>
                                    @foreach($c_sekolah as $skl)
                                    <option value="{{$skl->id}}">{{$skl->nama_sekolah}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">NAMA BARANG</label>
                                <select name="barang_id" class="form-control" id="exampleFormControlSelect1">
                                    <option disabled selected> Pilih nama barang </option>
                                    @foreach($c_barang as $brg)
                                    <option value="{{$brg->id}}">{{@$brg->nama_barang}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="merk-tipe">MEREK/TIPE </label>
                                <input name="merek_tipe" type="text" class="form-control" id="merk-tipe" aria-describedby="emailHelp" placeholder="Tuliskan Merek/Tipe, misal ASUS Transformer A001" required>
                            </div>
                            <div class="form-group">
                                <label for="spesifikasi">SPESIFIKASI</label>
                                <input name="spesifikasi_ukuran_cc" type="text" class="form-control" id="spesifikasi" aria-describedby="emailHelp" placeholder="Isikan spesifikasi, misal Prosesor Intel Core i3 3500 Mhz">
                            </div>
                            <div class="form-group">
                                <label for="tahun-pembelian">TAHUN PEMBELIAN </label>
                                <input name="thn_pembelian" type="text" class="form-control" id="tahun-pembelian" aria-describedby="emailHelp" placeholder="Isikan angka tahun lengkap, misalnya 2020" required>
                            </div>
                            <div class="form-group">
                                <label for="harga">HARGA </label>
                                <input name="harga" type="text" class="form-control" id="harga" aria-describedby="emailHelp" placeholder="masukkan harga, tuliskan angkanya saja, misal 100000" required>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">SUMBER DANA</label>
                                <select name="sumber_dana" class="form-control" id="exampleFormControlSelect1">
                                    <option disabled selected> Pilih Sumber Dana</option>
                                    <option value="Dana Yayasan">Dana Yayasan</option>
                                    <option value="Dana BOS">Dana BOS Sekolah</option>
                                    <option value="Dana Pribadi">Dana Pribadi</option>
                                    <option value="Pinjaman Bank">Pinjaman Bank</option>
                                    <option value="Sumbangan Masyarakat">Sumbangan Masyarakat</option>
                                    <option value="Wakaf">Wakaf</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">KONDISI</label>
                                <select name="kondisi" class="form-control" id="exampleFormControlSelect1">
                                    <option disabled selected> Pilih kondisi Baru / Bekas </option>
                                    <option value="Baru">Baru</option>
                                    <option value="Bekas">Bekas</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="keterangan">KETERANGAN</label>
                                <textarea name="keterangan" class="form-control" id="keterangan" rows="3"></textarea>
                            </div>
                            <!--  <div class="form-group">
                            <label for="exampleFormControlFile1">Masukkan Foto/Nota Alat (bila ada)</label>
                            <input name="fotoavatar" type="file" class="form-control-file" id="exampleFormControlFile1">
                        </div> -->
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
</section>
@stop