@extends('layouts.master')

@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Edit Data Aset Alat</h3>
                        </div>
                        <div class="panel-body">
                            <form action="{{ route('asset-inventory.transaksi.alat.update', ['alat' => $alat_trans->id]) }}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                @method('PATCH')
                                <div class="form-group">
                                    <label for="kategori">Kategori</label>
                                    <input name="kategori_id" type="text" class="form-control" id="kategori" aria-describedby="emailHelp" value="{{$alat_trans->kategori->sub_kat}}" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="barang">Aset Alat</label>
                                    <input name="barang_id" type="text" class="form-control" id="barang" aria-describedby="emailHelp" value="{{$alat_trans->barang->nama_barang}}" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="sekolah-yayasan">Sekolah / Yayasan</label>
                                    <input name="sekolah_id" type="text" class="form-control" id="sekolah-yayasan" aria-describedby="emailHelp" value="{{$alat_trans->sekolah->nama_sekolah}}" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="kode-alat">Kode Alat</label>
                                    <input name="kode_alat" type="text" class="form-control" id="kode-alat" aria-describedby="emailHelp" value="{{$alat_trans->kode_alat}}">
                                </div>
                                <div class="form-group">
                                    <label for="nomor-registrasi-alat">Nomor Registrasi Alat</label>
                                    <input name="no_reg_alat" type="text" class="form-control" id="nomor-registrasi-alat" aria-describedby="emailHelp" value="{{$alat_trans->no_reg_alat}}">
                                </div>
                                <div class="form-group">
                                    <label for="merk-tipe">Merek Tipe</label>
                                    <input name="merek_tipe" type="text" class="form-control" id="merk-tipe" aria-describedby="emailHelp" value="{{$alat_trans->merek_tipe}}">
                                </div>
                                <div class="form-group">
                                    <label for="spesifikasi">Spesifikasi</label>
                                    <input name="ukuran_cc" type="text" class="form-control" id="spesifikasi" aria-describedby="emailHelp" value="{{$alat_trans->ukuran_cc}}">
                                </div>
                                <div class="form-group">
                                    <label for="bahan"> Bahan </label>
                                    <input name="bahan" type="text" class="form-control" id="bahan" aria-describedby="emailHelp" value="{{$alat_trans->bahan}}">
                                </div>
                                <div class="form-group">
                                    <label for="tahun-pembelian"> Tahun Pembelian </label>
                                    <input name="thn_pembelian" type="text" class="form-control" aria-describedby="tahun-pembelian" aria-describedby="emailHelp" value="{{$alat_trans->thn_pembelian}}" >
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"> Nomor Pabrik </label>
                                    <input name="no_pabrik" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$alat_trans->no_pabrik}}" >
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"> Nomor Rangka </label>
                                    <input name="no_rangka" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$alat_trans->no_rangka}}" >
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"> Nomor Mesin </label>
                                    <input name="no_mesin" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$alat_trans->no_mesin}}" >
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"> Nomor Polisi </label>
                                    <input name="no_polisi" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$alat_trans->no_polisi}}" >
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"> Nomor BPKB </label>
                                    <input name="no_bpkb" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$alat_trans->no_bpkb}}" >
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"> Asal Usul </label>
                                    <input name="asal_usul" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$alat_trans->asal_usul}}" >
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"> Harga </label>
                                    <input name="harga" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$alat_trans->harga}}" >
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"> Sumber Dana </label>
                                    <input name="sumber_dana" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$alat_trans->sumber_dana}}" disabled>
                                </div>
                                <div class="form-group">
                                    <label name="keterangan" for="exampleFormControlTextarea1" >Keterangan</label>
                                    <textarea name="keterangan" class="form-control" id="exampleFormControlTextarea1" rows="3" >{{$alat_trans->keterangan}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlFile1">Masukkan Foto Nota/Alat/Barang (bila ada)</label>
                                    <input name="fotoavatar" type="file" value="{{$alat_trans->fotoavatar}}" class="form-control-file" id="exampleFormControlFile1">
                                </div>
                                <button type="submit" class="btn btn-warning">Update Data</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop