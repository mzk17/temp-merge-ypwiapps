@extends('layouts.master')

@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                <div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Edit Data Sekolah / Yayasan</h3>
								</div>
								<div class="panel-body">
                                            <form action="/sekolah/{{$sekolah->id}}/update" method="POST">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Nama Sekolah / Yayasan</label>
                                        <input name="nama_sekolah" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama Sekolah" value="{{$sekolah->nama_sekolah}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label name="alamat" for="exampleFormControlTextarea1" >Alamat</label>
                                        <textarea name="alamat" class="form-control" id="exampleFormControlTextarea1" rows="3" required>{{$sekolah->alamat}}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Telepon</label>
                                        <input name="telepon" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan nomor telepon" required value="{{($sekolah->telepon)}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Email</label>
                                        <input name="email" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan alamat email" required value="{{($sekolah->email)}}" >
                                    </div>                                    
                                    <!--div-- class="form-group">
                                        <label for="exampleFormControlFile1">Masukkan Foto/Nota Sekolah (bila ada)</label>
                                        <input name="foto_sekolah" type="file" value="{{$sekolah->foto_sekolah}}" class="form-control-file" id="exampleFormControlFile1">
                                    </!--div-->
                                    <button type="submit" class="btn btn-warning">Update Data</button>
                                    </form>
								</div>
							</div>
                </div>
            </div>  
        </div>
    </div>
</div>

@stop