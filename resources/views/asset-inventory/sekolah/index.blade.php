@extends('layouts.master')
@section('content')
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Data Sekolah</h3>
                </div>                
                    <!-- /.card-header -->
                    <div class="card-body ">       
                    <a href="/sekolah/export" class="btn btn-sm btn-success right"> Export Data </a>             
                                    <table id="example1" class="table table-bordered table-striped ">
                                        <div class="btn btn-sm right">
                                         <button type="button" class="btn btn-sm btn-block btn-outline-primary" data-toggle="modal" data-target="#exampleModal">Tambah Data Sekolah                             
                                         </button>
                                        </div>
                                         <thead>
											<tr>
                                                <th width="30%">NAMA SEKOLAH</th>
                                                <th width="35%">ALAMAT</th>
                                                <th>TELEPON </th>
                                                <th width="15%">EMAIL</th>
                                                <th width="5%">AKSI</th>
                                                <th width="5%"></th>
											</tr>
										</thead>
										<tbody>
                                            @foreach($data_sekolah as $sekolah)
                                            <tr>
                                                <td width="30%"><a href="/sekolah/{{$sekolah->id}}/profile">{{$sekolah->nama_sekolah}}</a></td>
                                                <td width="35%">{{$sekolah->alamat}}</td>
                                                <td>{{$sekolah->telepon}}</td>
                                                <td width="15%">{{$sekolah->email}}</td>
                                                <td width="5%"><a href="/sekolah/{{$sekolah->id}}/edit" class="btn btn-warning btn-sm">Edit</a></td>
                                                <td width="5%"><a href="/sekolah/{{$sekolah->id}}/delete" class="btn btn-danger btn-sm" onclick="return confirm('Yakin mau dihapus?')">Hapus</a></td>                
                                            </tr>
                                            @endforeach
										</tbody>
									</table>
								</div>
							</div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--tambah data-->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" >TAMBAH DATA SEKOLAH / YAYASAN</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/sekolah/create" method="POST">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="exampleInputEmail1">NAMA SEKOLAH</label>
                            <input name="nama_sekolah" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama Sekolah" required>
                        </div>

                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">ALAMAT</label>
                            <textarea name="alamat" class="form-control" id="exampleFormControlTextarea1" rows="3" required></textarea>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">TELEPON</label>
                            <input name="telepon" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan nomor telepon" required>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">EMAIL</label>
                            <input name="email" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan alamat email" required>
                        </div>
                        <!--div class="form-group">
                            <label for="exampleFormControlFile1">Masukkan Foto Sekolah (bila ada)</label>
                            <input name="foto_sekolah" type="file" class="form-control-file" id="exampleFormControlFile1">
                        </!--div>-->                   
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        
                    </form>
                </div>
                </div>
            </div>
@stop