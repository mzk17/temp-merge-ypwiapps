@extends('layouts.master')

@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Edit Data Barang</h3>
                    </div>
                    <div class="panel-body">
                        <form action="{{ route('asset-inventory.barang.update', ['barang' => $barang->id]) }}" method="POST">
                            {{ csrf_field() }}
                            @method('PATCH')
                            <div class="form-group">
                                <label for="nama_barang">Nama Barang</label>
                                <input name="nama_barang" type="text" class="form-control" id="nama_barang" placeholder="Nama Barang" value="{{ $barang->nama_barang }}" required>
                            </div>
                            <button type="submit" class="btn btn-warning">Update Data</button>
                        </form>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop