@extends('layouts.master')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Data Barang</h3>
            </div>            
            <!-- /.card-header -->
            <div class="card-body">            
                <table id="example1" class="table table-bordered table-striped">
                    <div class="btn btn-sm right">
                        <button type="button" class="btn btn-block btn-sm btn-outline-primary" data-toggle="modal" data-target="#exampleModal">
                            Tambah Data Barang                             
                        </button>
                    </div>
                    <thead>
                        <tr>
                            <th>Nama Barang</th>
                            <th width="5%">Update</th>
                            <th width="10%">Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data_barang as $barang)
                        <tr>
                            <td>{{$barang->nama_barang}}</td>
                            <td width="5%"><a href="{{ route('asset-inventory.barang.edit', ['barang' => $barang->id]) }}" class="btn btn-warning btn-sm">Edit</a></td>
                            <td width="10%">
                                <form action="{{ route('asset-inventory.barang.destroy', ['barang' => $barang->id]) }}" method="POST">
                                    {{ csrf_field() }}
                                    @method('DELETE')
                                    <button class="btn btn-danger btn-sm" onclick="return confirm('Yakin mau dihapus?')">Hapus</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">TAMBAH DATA BARANG</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('asset-inventory.barang.store') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="nama_barang">Nama Barang</label>
                        <input name="nama_barang" type="text" class="form-control" id="nama_barang" placeholder="Isikan nama barang" required>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop