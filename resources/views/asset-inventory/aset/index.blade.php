@extends('layouts.master')

@section('content')
<div class="card">
                <div class="card-header">
                    <h3 class="card-title">Data Aset</h3>
                </div>                
                    <!-- /.card-header -->
                    <div class="card-body ">                    
                                    <table id="example1" class="table table-bordered table-striped ">
                                        <div class="btn btn-sm right">
                                         <button type="button" class="btn btn-block btn-outline-primary" data-toggle="modal" data-target="#exampleModal">Tambah Data Aset                             
                                         </button>
                                        </div>
                                         <thead>
											<tr>
                                                <th>NAMA ASET</th>
                                                <th width="5%">AKSI</th>
                                                <th width="10%"></th>
											</tr>
										</thead>
										<tbody>
                                            @foreach($data_aset as $aset)
                                            <tr>
                                                <td>{{$aset->nama_aset}}</td>
                                                <td width="5%"><a href="/aset/{{$aset->id}}/edit" class="btn btn-warning btn-sm">Edit</a></td>
                                                <td width="10%"><a href="/aset/{{$aset->id}}/delete" class="btn btn-danger btn-sm" onclick="return confirm('Yakin mau dihapus?')">Hapus</a></td>                
                                            </tr>
                                            @endforeach
										</tbody>
									</table>
								</div>
							</div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">TAMBAH DATA ASET</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/aset/create" method="POST">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="exampleInputEmail1">NAMA ASET</label>
                            <input name="nama_aset" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama Aset" required>
                        </div>
                </div>
                    
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        
                    </form>
                </div>
                </div>
            </div>
@stop