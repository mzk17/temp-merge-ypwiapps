@extends('layouts.master')

@section('content')

<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
            <!--form-- class="form-inline my-2 my-lg-0 col-md-1" method="GET" action="/aset">
                <input name="cari" class="form-control mr-sm-2 col-md-1" type="search" placeholder="Cari berdasarkan aset" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Cari !!</button>
                </!--form-->
                <form class="navbar-form navbar-right" method="GET" action="/aset">
                    <input name="cari" class="form-control" type="search" placeholder="Cari berdasarkan aset" aria-label="Search">
                    <button class="btn btn-outline-success btn-primary right" type="submit">Cari !!</button>
                    </form>
                <div class="col-md-12">
                <div class="panel">
                <div class="input-group">
                    
                </div>
								<div class="panel-heading">
                                    <h2 class="panel-title text-bold">Data Aset</h2>
                                    <div class=" btn btn-primary right">
                                        <button type="button" class="btn-primary" data-toggle="modal" data-target="#exampleModal">Tambah Data Aset                              
                                        </button>
                                    </div>
								</div>
								<div class="panel-body">
                               
									<table class="table table-hover">
										<thead>
											<tr>
                                                <th>NAMA ASET</th>
                                                <th>FOTO </th>
                                                <th>LOKASI </th>
                                                <th>KATEGORI</th>
                                                <th>LUAS</th>
                                                <th>HARGA</th>
                                                <th>TANGGAL</th>
                                                <!--th>BULAN</!th>
                                                <th>TAHUN</th>-->
                                                <th>KETERANGAN</th>
                                                <th>AKSI</th>
                                                <th></th>
											</tr>
										</thead>
										<tbody>
                                            @foreach($data_aset as $aset)
                                            <tr>
                                                <td>{{$aset->nama_aset}}</td>
                                                <td>{{$aset->foto_aset}}</td>
                                                <td>{{$aset->id_lokasi}}</td>
                                                <td>{{$aset->kategori}}</td>
                                                <td>{{formatVolume($aset->luas)}}</td>                
                                                <td>{{formatRupiah($aset->harga)}}</td>
                                                <td>{{$aset->tanggal}}-{{$aset->bulan}}-{{$aset->tahun}}</td>
                                                <td>{{$aset->keterangan}}</td>
                                                <td><a href="/aset/{{$aset->id}}/edit" class="btn btn-warning btn-sm">Edit</a></td>
                                                <td><a href="/aset/{{$aset->id}}/delete" class="btn btn-danger btn-sm" onclick="return confirm('Yakin mau dihapus?')">Hapus</a></td>                
                                            </tr>
                                            @endforeach
										</tbody>
									</table>
								</div>
							</div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">TAMBAH DATA ASET</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/aset/create" method="POST">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="exampleInputEmail1">NAMA ASET</label>
                            <input name="nama_aset" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama Aset" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">LOKASI</label>
                            <select name="id_lokasi" class="form-control" id="exampleFormControlSelect1">
                                <option value="1">YPWI PUSAT MAKASSAR</option>
                                <option value="2">SD WU ABDESIR</option>
                                <option value="3">SD WI 01 ANTANG</option>
                                <option value="4">SD WI 02 HERTASNING</option>
                                <option value="5">SD WI 03 BTP</option>
                                <option value="6">SMP IT WI PUTRA</option>
                                <option value="7">SMP IT WI PUTRI</option>
                                <option value="8">SMA IT WI PUTRA</option>
                                <option value="9">SMA IT WI PUTRI</option>
                                <option value="10">TK IT WU</option>
                                <option value="11">TK IT WI 01</option>
                                <option value="12">PONPES TAHFIDZ WI - BILAYYA</option> 
                                <option value="13">PONPES TAHFIDZ WI - DAYA</option>
                                <option value="14">SMP-SMA AL-QUR'AN WI CIBINONG BOGOR</option>
                                <option value="15">SMP-SMA AL-QUR'AN WI ANABANUA WAJO
                                <option value="16">SAWANGAN DEPOK</option>
                                <option value="17">TANJUNG PINANG RIAU</option>
                                <option value="18">JAMBI</option>
                                <option value="19">BELOPA</option>
                                <option value="20">GORONTALO</option>
                                <option value="21">PALOPO</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">KATEGORI</label>
                            <select name="kategori" class="form-control" id="exampleFormControlSelect1">
                                <option value="TANAH">TANAH</option>
                                <option value="BANGUNAN">BANGUNAN</option>
                            </select>
                        </div>

                        <!-- //kalau mau pake radio

                        <div class="form-check">
                        <input class="form-check-input" type="radio" name="kategori" id="exampleRadios1" value="TANAH">
                        <label class="form-check-label" for="exampleRadios1">
                            TANAH
                        </label>
                        </div>
                        <div class="form-check">
                        <input class="form-check-input" type="radio" name="kategori" id="exampleRadios2" value="BANGUNAN">
                        <label class="form-check-label" for="exampleRadios2">
                            BANGUNAN
                        </label>
                        </div>

                        -->
                        <div class="form-group">
                            <label for="exampleInputEmail1">LUAS (m2)</label>
                            <input name="luas" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Cukup tuliskan dalam angka" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">HARGA (Rp)</label>
                            <input name="harga" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Cukup tuliskan dalam angka" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">TANGGAL</label>
                            <select name="tanggal" class="form-control" id="exampleFormControlSelect1">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                                <option value="21">21</option>
                                <option value="22">22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                                <option value="26">26</option>
                                <option value="27">27</option>
                                <option value="28">28</option>
                                <option value="29">29</option>
                                <option value="30">30</option>
                                <option value="31">31</option>
                            </select>
                            <!--label for="exampleInputEmail1">TANGGAL</!--label>
                            <input type="text" name="tanggal" class="datepicker-here form-control" data-language="en" id="exampleInputEmail1"/-->
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">BULAN</label>
                            <select name="bulan" class="form-control" id="exampleFormControlSelect1">
                                <option value="1">JANUARI</option>
                                <option value="2">FEBRUARI</option>
                                <option value="3">MARET</option>
                                <option value="4">APRIL</option>
                                <option value="5">MEI</option>
                                <option value="6">JUNI</option>
                                <option value="7">JULI</option>
                                <option value="8">AGUSTUS</option>
                                <option value="9">SEPTEMBER</option>
                                <option value="10">OKTOBER</option>
                                <option value="11">NOVEMBER</option>
                                <option value="12">DESEMBER</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">TAHUN</label>
                            <select name="tahun" class="form-control" id="exampleFormControlSelect1">
                                <option value="2020">2020</option>
                                <option value="2019">2019</option>
                                <option value="2018">2018</option>
                                <option value="2017">2017</option>
                                <option value="2016">2016</option>
                                <option value="2015">2015</option>
                                <option value="2014">2014</option>
                                <option value="2013">2013</option>
                                <option value="2012">2012</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">KETERANGAN</label>
                            <textarea name="keterangan" class="form-control" id="exampleFormControlTextarea1" rows="3" required></textarea>
                        </div>
                        
                        <div class="form-group">
                            <label for="exampleFormControlFile1">Masukkan Foto/Nota Aset (bila ada)</label>
                            <input name="foto_aset" type="file" class="form-control-file" id="exampleFormControlFile1">
                        </div>
                </div>
                    
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        
                    </form>
                </div>
                </div>
            </div>
@stop