@extends('layouts.master')

@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                <div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Edit Data Karyawan</h3>
								</div>
								<div class="panel-body">
                                <span class="label label-info label-col-lg-6">DATA DIRI</span>
                                    </br>
                                    <form action="/karyawan/{{$karyawan->id}}/update" method="POST" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Nama Lengkap</label>
                                        <input type="text" name="nama" class="form-control col-md-12" 
                                        id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$karyawan->nama_pegawai}}" >
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">NIK</label>
                                        <input type="text" name="nik" class="form-control col-md-12" 
                                        id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$karyawan->nik}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Tempat Lahir</label>
                                        <input type="text" name="tempat_lahir" class="form-control col-md-12" 
                                        id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$karyawan->tempat_lahir}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Tanggal Lahir</label>
                                        <input name="tanggal_lahir" type="text" class="form-control col-md-12" 
                                        id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$karyawan->tanggal_lahir}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Jenis Kelamin</label>
                                        <select name="jk" class="form-control" 
                                        id="exampleFormControlSelect1">
                                        <option value="Laki-laki" @if($karyawan->jk == 'Laki-laki') selected @endif> Laki-laki </option>
                                        <option value="Perempuan" @if($karyawan->jk == 'Perempuan') selected @endif> Perempuan </option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">Alamat</label>
                                        <textarea name="alamat_jalan" class="form-control" 
                                        id="exampleFormControlTextarea1" rows="3">{{$karyawan->alamat_jalan}}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Nomor HP</label>
                                        <input type="text" name="hp" class="form-control col-md-12" 
                                        id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$karyawan->hp}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">Foto</label>
                                        <input type="file" name="avatar" class="form-control">
                                    </div>
                                    </br>
                                    </br>
                                    <span class="label label-info label-col-lg-6">DATA PENGHASILAN</span>
                                    </br>
                                    
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Nama Wajib Pajak</label>
                                        <input type="text" name="nama_wajib_pajak" class="form-control col-md-6" 
                                        id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$karyawan->nama_wajib_pajak}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Nomor NPWP</label>
                                        <input type="text" name="npwp" class="form-control col-md-6" 
                                        id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$karyawan->npwp}}">
                                    </div>
                                    </br>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Bank</label>
                                        <input type="text" name="bank" class="form-control col-md-6" 
                                        id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$karyawan->bank}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Nomor Rekening Bank</label>
                                        <input type="text" name="nomor_rekening_bank" class="form-control col-md-6" 
                                        id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$karyawan->nomor_rekening_bank}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Nama Rekening Bank atas nama</label>
                                        <input type="text" name="rekening_atas_nama" class="form-control col-md-6" 
                                        id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$karyawan->rekening_atas_nama}}">
                                    </div>
                                    </br>
                                    </br>
                                <button type="submit" class="btn btn-warning">Update Data</button>   
                            </form>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop