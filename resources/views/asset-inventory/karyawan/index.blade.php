@extends('layouts.master')

@section('content')
<div class="card">
                <div class="card-header">
                    <h3 class="card-title">Data Pegawai/Karyawan/Guru</h3>
                </div>                
                    <!-- /.card-header -->
                    <div class="card-body ">       
                    <a href="/karyawan/export" class="btn btn-sm btn-success right"> Export Data </a>             
                                    <table id="example1" class="table table-bordered table-striped ">
                                        <div class="btn btn-sm right">
                                         <button type="button" class="btn btn-sm btn-block btn-outline-primary" data-toggle="modal" data-target="#exampleModal">Tambah Data Karyawan                             
                                         </button>
                                        </div>
                                         <thead>
											<tr>
                                                <th>NAMA KARYAWAN</th>
                                                <th>JENIS KELAMIN</th>
                                                <th>TEMPAT, TANGGAL LAHIR</th>
                                                <th>ALAMAT</th>
                                                <th>JABATAN</th>
                                                <th>UNIT KERJA</th>
                                                <th>NOMOR HANDPHONE</th>
                                                <th>AKSI</th>
                                                <th></th>
											</tr>
										</thead>
										<tbody>
                                            @foreach($data_karyawan as $karyawan)
                                            <tr>
                                                <td>{{$karyawan->nama_pegawai}}</td>
                                                <td>{{$karyawan->jk}}</td>
                                                <td>{{$karyawan->tempat_lahir}},{{$karyawan->tanggal_lahir}}</td>
                                                <td>{{$karyawan->alamat_jalan}}</td>
                                                <td>{{$karyawan->tugas_tambahan}}</td>   
                                                <td>{{$karyawan->unit_kerja}}</td>                   
                                                <td>{{$karyawan->hp}}</td>
                                                <td><a href="/karyawan/{{$karyawan->id}}/edit" class="btn btn-warning btn-sm">Edit</a></td>
                                                <td><a href="/karyawan/{{$karyawan->id}}/delete" class="btn btn-danger btn-sm" onclick="return confirm('Yakin mau dihapus?')">Hapus</a></td>                
                                            </tr>
                                            @endforeach
										</tbody>
									</table>
								</div>
							</div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">TAMBAH DATA KARYAWAN</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/karyawan/create" method="POST">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="exampleInputEmail1">NAMA KARYAWAN</label>
                            <input name="nama_pegawai" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama Lengkap" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">JENIS KELAMIN</label>
                            <select name="jk" class="form-control" id="exampleFormControlSelect1">
                                <option value="" disabled>Pilih Jenis Kelamin</option>
                                <option value="Laki-laki">Laki-laki</option>
                                <option value="Perempuan">Perempuan</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">TEMPAT LAHIR</label>
                            <input name="tempat_lahir" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Tempat Lahir" required>
                        </div>
                        <!--div class="form-group">
                            <label for="exampleFormControlSelect1">MERK</label>
                            <select name="merk" class="form-control" id="exampleFormControlSelect1">
                                <option value="TANAH">TANAH</option>
                                <option value="BANGUNAN">BANGUNAN</option>
                            </select>
                        </!--div>-->

                        <!-- //kalau mau pake radio

                        <div class="form-check">
                        <input class="form-check-input" type="radio" name="kategori" id="exampleRadios1" value="TANAH">
                        <label class="form-check-label" for="exampleRadios1">
                            TANAH
                        </label>
                        </div>
                        <div class="form-check">
                        <input class="form-check-input" type="radio" name="kategori" id="exampleRadios2" value="BANGUNAN">
                        <label class="form-check-label" for="exampleRadios2">
                            BANGUNAN
                        </label>
                        </div>

                        -->
                        <div class="form-group">
                            <label for="exampleInputEmail1">TANGGAL LAHIR</label>
                            <input name="tanggal_lahir" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Format yyyy/mm/dd, misal 2020/01/31" required>
                        </div>                        
                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">ALAMAT</label>
                            <textarea name="alamat_jalan" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">UNIT KERJA</label>
                            <input name="unit_kerja" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Tuliskan unit kerja, misal YPWI Pusat" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">JABATAN</label>
                            <input name="tugas_tambahan" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Tuliskan unit kerja, misal YPWI Pusat" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">NOMOR HANDPHONE</label>
                            <input name="hp" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Cukup tuliskan dalam angka, misal 04119119000" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlFile1">Masukkan Foto (bila ada)</label>
                            <input name="avatar" type="file" class="form-control-file" id="exampleFormControlFile1">
                        </div>
                </div>
                    
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        
                    </form>
                </div>
                </div>
            </div>
@stop