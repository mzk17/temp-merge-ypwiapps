@extends('layouts.app')

@section('content')
<div class="grid-x align-center margin-top-3">
    <div class="cell small-4 grid-y">
        <div class="cell text-center">
            <img src="/images/vc_logo@256.png" width="96px" height="96px" alt="ypwi_logo"/>
            <h1 class="auth-title is-size-4">YPWI Apps - Pendaftaran</h1>
        </div>
        <div class="cell margin-vertical-2">
            <form method="POST" action="{{ route('register') }}" class="card">
                @csrf

                <div class="card-content grid-y grid-margin-y">
                    <b-field class="cell" label="No. Identitas" :message="['NIP untuk pegawai / NIS untuk siswa', 'Nomor KTP untuk umum']">
                        <input
                            id="id"
                            type="text"
                            class="input @error('id') is-danger @enderror"
                            name="id"
                            required
                            value="{{ old('id') }}"
                            autofocus>
                    </b-field>

                <registration-user-role-form :jenis-akun="{{ $jenisAkun }}"></registration-user-role-form>

                    <div class="cell">
                        <b-field label="{{ __('Username') }}">
                            <input
                                id="username"
                                type="text"
                                class="input @error('username') is-danger @enderror"
                                name="username"
                                aria-label="username"
                                value="{{ old('username') }}"
                                required
                                autocomplete="username"
                                autofocus>
                        </b-field>
                        @error('username')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    
                    <div class="cell">
                        <b-field label="{{ __('Alamat E-Mail') }}">
                                <input
                                    id="email"
                                    type="email"
                                    class="input @error('email') is-danger @enderror"
                                    name="email"
                                    aria-label="alamat email"
                                    value="{{ old('email') }}"
                                    required
                                    autocomplete="email">
                        </b-field>
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="cell">
                        <b-field label="{{ __('Password') }}">
                                <input
                                    id="password"
                                    type="password"
                                    class="input @error('password') is-danger @enderror"
                                    name="password"
                                    aria-label="password"
                                    required
                                    autocomplete="new-password">
                        </b-field>
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
    
                    <div class="cell">
                        <b-field label="{{ __('Konfirmasi Password') }}">
                                <input
                                    id="password-confirm"
                                    type="password"
                                    class="input"
                                    name="password_confirmation"
                                    aria-label="konfirmasi password"
                                    required
                                    autocomplete="new-password">
                        </b-field>
                    </div>
    
                    <div class="cell">
                        <button type="submit" class="button is-primary">
                            {{ __('Daftar') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
