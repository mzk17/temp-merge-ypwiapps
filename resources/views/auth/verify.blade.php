@extends('layouts.prime.wrapper')

@section('app-content')
<div class="grid-x align-middle align-center" style="height: 100vh">
    <div class="cell small-12 medium-8 large-6 grid-y grid-margin-y text-center">
        @if (session('resent'))
            <div class="cell" role="alert">
                <p>Email verifikasi yang baru telah dikirimkan ke alamat email anda.</p>
            </div>
        @endif

        <div class="cell">
            <img src="{{ asset('/images/vc_logo@256.png') }}" width="96px" height="96px" alt="ypwi_logo"/>
            <h1 class="auth-title is-size-4">YPWI Apps</h1>
        </div>

        <div class="cell card">
            <div class="card-content">
                <p><strong>Mohon verifikasi alamat email anda terlebih dahulu</strong></p>
                <hr>
                <p>Mohon tunggu 1-3 menit dan cek folder <strong>Spam / Junk</strong> terlebih dahulu.</p>
                <p>Jika masih belum mendapatkan email verifikasi, silahkan klik tombol dibawah untuk dikirmkan verifikasi ulang pada alamat email anda.</p>
            </div>
        </div>

        <div class="cell">
            <a href="{{ route('prime.resend-verification') }}" class="button is-primary">Kirim Ulang</a>
            <form action="{{ route('logout') }}" method="POST" style="display: inline-block">
                @csrf
                <button type="submit" class="button is-danger">Logout</button>
            </form>
        </div>
    </div>
</div>
@endsection
