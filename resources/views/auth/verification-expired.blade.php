@extends('layouts.prime.wrapper')

@section('app-content')
<div class="grid-x align-middle align-center" style="height: 100vh">
    <div class="cell small-12 medium-8 large-4 grid-y grid-margin-y text-center">
        <div class="cell">
            <img src="{{ asset('images/vc_logo@256.png') }}" height="96px" width="96px" alt="ypwi_logo">
        </div>
        <div class="cell card">
            <div class="card-content">
                <h1 class="auth-title is-size-4">419 | Verifikasi telah kadaluarsa</h1>
                <hr>
                <p>Silahkan klik tombol dibawah untuk dikirmkan verifikasi ulang pada alamat email anda</p>
            </div>
        </div>
        <div class="cell">
            <a href="{{ route('prime.resend-verification') }}" class="button is-primary">Kirim Ulang</a>
        </div>
    </div>
</div>
@endsection