@extends('layouts.prime.wrapper')

{{-- layouts.app extensions --}}
@section('meta')
    <meta name="percobaan" content="Synchronicity">
@endsection
{{-- end of layouts.app extensions --}}

{{-- PHP Script --}}
@php
    $username_error_class = ($errors->has('username')) ? 'is-danger' : '';
    $password_error_class = ($errors->has('password')) ? 'is-danger' : '';
@endphp
{{-- End of PHP Script --}}

@section('app-content')
    <div class="flex flex-wrap content-center justify-center h-screen">
        <div class="flex flex-col w-1/3">
            @if(count($errors) > 0)
                <div class="cell margin-bottom-1">
                    <b-notification
                    type="is-danger"
                    aria-close-label="Tutup notifikasi"
                    role="alert">
                        <ul>
                            @foreach ($errors->all() as $message)
                                <li>{{ $message }}</li>
                            @endforeach
                        </ul>
                    </b-notification>
                </div>
            @endif
            <div class="flex flex-col items-center mb-1 w-100">
                <img src="/images/vc_logo@256.png" width="96px" height="96px" alt="ypwi_logo"/>
                <h1 class="auth-title is-size-1">YPWI Apps</h1>
            </div>
            <form id="ypwi-login-form" action="{{ route('login') }}" method="POST" class="card">
                @csrf
                <div class="card-content grid-y">
                    <div class="cell margin-bottom-1 has-text-left">
                        <b-field label="Username">
                            <input class="input {{ $username_error_class }}" type="text" name="username" placeholder="Tuliskan Username anda">
                        </b-field>
                    </div>
                    <div class="cell has-text-left">
                        <b-field label="Password">
                            <input class="input {{ $password_error_class }}" type="password" name="password" placeholder="Password anda">
                        </b-field>
                    </div>
                </div>
                <footer class="card-footer">
                    <a href="#" onclick="loginCreds()" class="card-footer-item">Login</a>    
                </footer>
            </form>
        </div>
	</div>
@endsection

{{-- layouts.app extensions - JavaScript  --}}
@section('body-script')
<script type="text/javascript">
    function loginCreds() {
        document.getElementById("ypwi-login-form").submit();
    }
</script>
@stop
{{-- end of layouts.app extensions - JavaScript --}}