@extends('layouts.app')

@section('content')
<div class="tack-center">
    <div class="w-1/3 flex flex-col">
        <div class="flex flex-col">
            <img src="/images/vc_logo@256.png" width="96px" height="96px" alt="ypwi_logo"/>
            <h1 class="auth-title is-size-4">YPWI Apps - Pendaftaran</h1>
        </div>
        <div class="cell">
          {{-- @foreach ($sekolah as $se)
              {{ $se->nama_tingkatan }}
          @endforeach --}}
        </div>
        <div class="cell margin-vertical-2">
            <form method="POST" action="{{ route('post-pre-register') }}" class="card">
                @csrf

                <div class="card-content grid-y grid-margin-y">
                    <input type="hidden" name="id" value="{{ $new_id }}" />
                    <input type="hidden" name="id_jenis_akun" value="2" />
                    <!-- Sekolah, Identitas, & email -->
                    <pre-registration-sekolah-form :sekolah="{{ $sekolah }}"></pre-registration-sekolah-form>

                    <div class="cell">
                        <b-field label="{{ __('Username') }}">
                            <input
                                id="username"
                                type="text"
                                class="input @error('username') is-danger @enderror"
                                name="username"
                                aria-label="username"
                                value="{{ old('username') }}"
                                required
                                autocomplete="username"
                                placeholder="Tuliskan username yang anda inginkan"
                                autofocus>
                        </b-field>
                        @error('username')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="cell">
                        <b-field label="{{ __('Password') }}">
                                <input
                                    id="password"
                                    type="password"
                                    class="input @error('password') is-danger @enderror"
                                    name="password"
                                    aria-label="password"
                                    required
                                    autocomplete="new-password">
                        </b-field>
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
    
                    <div class="cell">
                        <b-field label="{{ __('Konfirmasi Password') }}">
                                <input
                                    id="password-confirm"
                                    type="password"
                                    class="input"
                                    name="password_confirmation"
                                    aria-label="konfirmasi password"
                                    required
                                    autocomplete="new-password">
                        </b-field>
                    </div>
    
                    <div class="cell">
                        <button type="submit" class="button is-primary">
                            {{ __('Daftar') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
