<address-form
    @if(isset($alamat))
        :entity="{{ json_encode($alamat) }}"
    @endif
    :adm-reg="{{ $adm_reg }}"
    @if($errors->any())
        :errors="{{ $errors->toJson() }}"
        :old="{
            nama_alamat: '{{ old('nama_alamat') }}',
            alamat: '{{ old('alamat') }}',
            provinsi: '{{ old('provinsi') }}',
            kota: '{{ old('kota') }}',
            kecamatan: '{{ old('kecamatan') }}',
            kelurahan: '{{ old('kelurahan') }}',
        }"
    @endif>
    <h3 class="is-size-4">Data Alamat</h3>
    <template v-slot:alamat_utama>
        <input type="hidden" name="alamat_utama" value="{{ isset($alamat_utama) ? $alamat_utama : '1' }}" />
    </template>
</address-form>