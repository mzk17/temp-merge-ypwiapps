@if(isset($modul) && $modul === 'data_pribadi')
    <form class="flex flex-wrap flex-col" action="{{ route('prime.personal-data.update', ['data_pribadi' => $data_pribadi->id]) }}" method="POST">
        {{ csrf_field() }}
        @method('PATCH')
        <personal-information-form
            :entity="{{ json_encode($data_pribadi) }}"
            @if($errors->any())
                :errors="{{ $errors->toJson() }}"
                :old="{
                    nama: '{{ old('nama') }}',
                    tempat_lahir: '{{ old('tempat_lahir') }}',
                    tanggal_lahir: '{{ old('tanggal_lahir') }}',
                    agama: '{{ old('agama') }}',
                    email: '{{ old('email') }}',
                    telepon: '{{ old('telepon') }}',
                    hp: '{{ old('hp') }}',
                    nik: '{{ old('nik') }}',
                    no_kk: '{{ old('no_kk') }}',
                }"
            @endif>
        </personal-information-form>
        <div>
            <button type="submit" class="button">Simpan</button>
        </div>
        <div>
            <a href="{{ route('prime.personal-data.index') }}" class="button">Kembali</a>
        </div>
    </form>
@elseif (isset($data_pribadi))
    <personal-information-form
        :entity="{{ json_encode($data_pribadi) }}"
        @if($errors->any())
            :errors="{{ $errors->toJson() }}"
            :old="{
                nama: '{{ old('nama') }}',
                tempat_lahir: '{{ old('tempat_lahir') }}',
                tanggal_lahir: '{{ old('tanggal_lahir') }}',
                agama: '{{ old('agama') }}',
                email: '{{ old('email') }}',
                telepon: '{{ old('telepon') }}',
                hp: '{{ old('hp') }}',
                nik: '{{ old('nik') }}',
                no_kk: '{{ old('no_kk') }}',
            }"
        @endif>
    </personal-information-form>
@else
    <personal-information-form
        @if($errors->any())
            :errors="{{ $errors->toJson() }}"
            :old="{
                nama: '{{ old('nama') }}',
                tempat_lahir: '{{ old('tempat_lahir') }}',
                tanggal_lahir: '{{ old('tanggal_lahir') }}',
                agama: '{{ old('agama') }}',
                email: '{{ old('email') }}',
                telepon: '{{ old('telepon') }}',
                hp: '{{ old('hp') }}',
                nik: '{{ old('nik') }}',
                no_kk: '{{ old('no_kk') }}',
            }"
        @endif>
    </personal-information-form>
@endif