@extends('layouts.app')
@section('content')

<div class="flex flex-row overflow-hidden" style="max-height: 100vh;">

    <!-- Side Main Menu -->
    @include('components.epayment.side-main-menu')

    <!-- App Content -->
    @yield('app-content')
</div>

@endsection