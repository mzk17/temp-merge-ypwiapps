<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('prime.main-home') }}" class="brand-link">
      <img src="/adminlte/dist/img/favicon.png"
           alt="YPWI Logo"
           class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">YPWI Applications</span>
    </a>

<!-- Sidebar -->
<div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="/adminlte/dist/img/avatar5.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><span>{{auth()->user()->name}}</span></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
          with font-awesome or any other icon font library -->

          @foreach ($sidebar_item as $main_key => $main_item)
            <li class="nav-item has-treeview">
              <a href="{{ $main_item['link'] }}" class="nav-link">
                <i class="{{ $main_item['icon'] }}"></i>
                <p>
                  {{ $main_item['name'] }}
                  @if($main_key === 'absensi')
                    <span class="badge badge-warning right">Penting!!</span>
                  @elseif($main_key === 'setting')
                    <i class="fas fa-angle-left right"></i>
                  @endif
                </p>
                @if(($main_key === 'master_data' || $main_key === 'transaksi') && (auth()->user()->jabatan === \App\Constants\Jabatan::ADMIN || auth()->user()->jabatan === \App\Constants\Jabatan::KETUA))
                  <span class="badge badge-info right">{{ count($main_item['children']) }} items</span>
                @endif
              </a>
              @if(($main_key === 'master_data' || $main_key === 'transaksi') && (auth()->user()->jabatan === \App\Constants\Jabatan::ADMIN || auth()->user()->jabatan === \App\Constants\Jabatan::KETUA))
                <ul class="nav nav-treeview">
                  @foreach ($main_item['children'] as $child_key => $child_item)
                    <li class="nav-item">
                      <a href="{{ $child_item['link'] }}" class="nav-link">
                        <i class="{{ $child_item['icon'] }}"></i>
                        <p>{{ $child_item['name'] }}</p>
                      </a>
                    </li>
                  @endforeach
                </ul>
              @endif
            </li>
          @endforeach

          <li class="nav-header">PROFIL PENGGUNA</li>
          @foreach ($sidebar_user_item as $user_item_key => $user_item)
            <li class="nav-item has-treeview">
              <a href="{{ $user_item['link'] }}" class="nav-link">
                <i class="{{ $user_item['icon'] }}"></i>
                <p>
                  {{ $user_item['name'] }}
                  @if($user_item_key === 'absensi')
                    <span class="badge badge-warning right">Penting!!</span>
                  @elseif($user_item_key === 'setting')
                    <i class="fas fa-angle-left right"></i>
                  @endif
                </p>
              </a>
              @if(array_key_exists('children', $user_item))
                <ul class="nav nav-treeview">
                  @foreach ($user_item['children'] as $child_key => $child_item)
                    <li class="nav-item">
                      <a href="{{ $child_item['link'] }}" class="nav-link">
                        <i class="{{ $child_item['icon'] }}"></i>
                        <p>{{ $child_item['name'] }}</p>
                      </a>
                    </li>
                  @endforeach
                </ul>
              @endif
            </li>
          @endforeach

          <!--li-- class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Grafik Laporan
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="../charts/chartjs.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>ChartJS</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../charts/flot.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Flot</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../charts/inline.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Inline</p>
                </a>
              </li>
            </ul>
          </!--li-->

          <!--li-- class="nav-item">
            <a href="../gallery.html" class="nav-link">
              <i class="nav-icon far fa-image"></i>
              <p>
                Dana Pensiun
              </p>
            </a>
          </!--li-->
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>