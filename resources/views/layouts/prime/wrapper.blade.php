@extends('layouts.app')
@section('content')

  <!-- Header -->
  @yield('header')

  <!-- App Content -->
  @yield('app-content')

@endsection