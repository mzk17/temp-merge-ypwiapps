@extends('layouts.master')

@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                <div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Edit Data Kategori</h3>
								</div>
								<div class="panel-body">
                                            <form action="/kategori/{{$kategori->id}}/update" method="POST">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">NAMA KATEGORI</label>
                                        <input name="nama_kategori" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama Kategori" value="{{$kategori->nama_kategori}}" required>
                                    </div>
                                    <button type="submit" class="btn btn-warning">Update Data</button>
                                    </form>
								</div>
							</div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop