@extends('layouts.master')

@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
            <form class="navbar-form navbar-right" method="GET" action="/kategori">
                    <input name="cari" class="form-control" type="search" placeholder="Cari berdasarkan kategori" aria-label="Search">
                    <button class="btn btn-outline-success btn-primary right" type="submit">Cari !!</button>
                    </form>
                <div class="col-md-12">
                <div class="panel">
								<div class="panel-heading">
                                    <h2 class="panel-title text-bold">Data Kategori</h2>
                                    <div class=" btn btn-primary right">
                                        <button type="button" class="btn-primary" data-toggle="modal" data-target="#exampleModal">Tambah Data Kategori                              
                                        </button>
                                    </div>
								</div>
								<div class="panel-body">
									<table class="table table-hover">
										<thead>
											<tr>
                                                <th>NAMA KATEGORI</th>
                                                <th width="5%">AKSI</th>
                                                <th width="10%"></th>
											</tr>
										</thead>
										<tbody>
                                            @foreach($data_kategori as $kategori)
                                            <tr>
                                                <td>{{$kategori->nama_kategori}}</td>
                                                <td width="5%"><a href="/kategori/{{$kategori->id}}/edit" class="btn btn-warning btn-sm">Edit</a></td>
                                                <td width="10%"><a href="/kategori/{{$kategori->id}}/delete" class="btn btn-danger btn-sm" onclick="return confirm('Yakin mau dihapus?')">Hapus</a></td>                
                                            </tr>
                                            @endforeach
										</tbody>
									</table>
								</div>
							</div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">TAMBAH DATA KATEGORI</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/kategori/create" method="POST">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="exampleInputEmail1">NAMA KATEGORI</label>
                            <input name="nama_kategori" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama Kategori" required>
                        </div>
                </div>
                    
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        
                    </form>
                </div>
                </div>
            </div>
@stop