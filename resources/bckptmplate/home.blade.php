<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<!--link href="{{ asset('admin/assets/lain/css/custom/cover.css') }}" rel="stylesheet"--> <!--asset ''-->
<link href="{{ asset('admin/assets/lain/vendor/font-awesome/css/all.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin/assets/lain/css/bootstrap.css') }}" rel="stylesheet">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>YPWI APPS FRONT</title>


    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <h1>
        <a href="" center class="typewrite" data-period="2000" data-type='[ "Selamat Datang", 
    "di aplikasi Yayasan Pesantren Wahdah Islamiyah (YPWI)" ]'>
            <span class="wrap"></span>
        </a>
    </h1>
    <br>
    
    

    <!--Style-->
    <style>
        body {
            background-color: #03a1a7;
            text-align: center;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            color: #fff;
            padding-top: 10em;
        }

        * {
            color: #fff;
            text-decoration: none;
        }
    </style>
</head>

<body>
    <p>
            <a href="{{ url('/login') }}" class="btn btn-lg btn-success"><span class="fa fa-sign-in-alt"></span> Masuk</a>

    </p>

    <script>
        var TxtType = function (el, toRotate, period) {
            this.toRotate = toRotate;
            this.el = el;
            this.loopNum = 0;
            this.period = parseInt(period, 10) || 2000;
            this.txt = '';
            this.tick();
            this.isDeleting = false;
        };

        TxtType.prototype.tick = function () {
            var i = this.loopNum % this.toRotate.length;
            var fullTxt = this.toRotate[i];

            if (this.isDeleting) {
                this.txt = fullTxt.substring(0, this.txt.length - 1);
            } else {
                this.txt = fullTxt.substring(0, this.txt.length + 1);
            }

            this.el.innerHTML = '<span class="wrap">' + this.txt + '</span>';

            var that = this;
            var delta = 200 - Math.random() * 100;

            if (this.isDeleting) {
                delta /= 2;
            }

            if (!this.isDeleting && this.txt === fullTxt) {
                delta = this.period;
                this.isDeleting = true;
            } else if (this.isDeleting && this.txt === '') {
                this.isDeleting = false;
                this.loopNum++;
                delta = 500;
            }

            setTimeout(function () {
                that.tick();
            }, delta);
        };

        window.onload = function () {
            var elements = document.getElementsByClassName('typewrite');
            for (var i = 0; i < elements.length; i++) {
                var toRotate = elements[i].getAttribute('data-type');
                var period = elements[i].getAttribute('data-period');
                if (toRotate) {
                    new TxtType(elements[i], JSON.parse(toRotate), period);
                }
            }
            // INJECT CSS
            var css = document.createElement("style");
            css.type = "text/css";
            css.innerHTML = ".typewrite > .wrap { border-right: 0.08em solid #fff}";
            document.body.appendChild(css);
        };
    </script>

</body>

</html>