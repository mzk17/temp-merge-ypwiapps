@extends('layouts.master')

@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                <div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Edit Data Supplier</h3>
								</div>
								<div class="panel-body">
                                            <form action="/supplier/{{$supplier->id}}/update" method="POST">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">NAMA SUPPLIER</label>
                                        <input name="nama_supplier" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama Supplier" value="{{$supplier->nama_supplier}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label name="alamat" for="exampleFormControlTextarea1" >ALAMAT</label>
                                        <textarea name="alamat_supplier" class="form-control" id="exampleFormControlTextarea1" rows="3" required>{{$supplier->alamat_supplier}}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">TELEPON</label>
                                        <input name="telepon_supplier" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan nomor telepon" required value="{{($supplier->telepon_supplier)}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">EMAIL</label>
                                        <input name="email_supplier" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan alamat email" required value="{{($supplier->email_supplier)}}" >
                                    </div>                                    
                                    <!--div-- class="form-group">
                                        <label for="exampleFormControlFile1">Masukkan Foto/Nota Supplier (bila ada)</label>
                                        <input name="foto_supplier" type="file" value="{{$supplier->foto_supplier}}" class="form-control-file" id="exampleFormControlFile1">
                                    </!--div-->
                                    <button type="submit" class="btn btn-warning">Update Data</button>
                                    </form>
								</div>
							</div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop