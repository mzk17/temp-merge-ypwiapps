@extends('layouts.master')

@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                <div class="panel">
								<div class="panel-heading">
                                    <h2 class="panel-title text-bold">Data Supplier</h2>
                                    <div class=" btn btn-primary right">
                                        <button type="button" class="btn-primary" data-toggle="modal" data-target="#exampleModal">Tambah Data Supplier                              
                                        </button>
                                    </div>
								</div>
								<div class="panel-body">
									<table class="table table-hover">
										<thead>
											<tr>
                                                <th width="30%">NAMA SUPPLIER</th>
                                                <th width="35%">ALAMAT</th>
                                                <th>TELEPON </th>
                                                <th width="15%">EMAIL</th>
                                                <th width="5%">AKSI</th>
                                                <th width="5%"></th>
											</tr>
										</thead>
										<tbody>
                                            @foreach($data_supplier as $supplier)
                                            <tr>
                                                <td width="30%">{{$supplier->nama_supplier}}</td>
                                                <td width="35%">{{$supplier->alamat_supplier}}</td>
                                                <td>{{$supplier->telepon_supplier}}</td>
                                                <td width="15%">{{$supplier->email_supplier}}</td>
                                                <td width="5%"><a href="/supplier/{{$supplier->id}}/edit" class="btn btn-warning btn-sm">Edit</a></td>
                                                <td width="5%"><a href="/supplier/{{$supplier->id}}/delete" class="btn btn-danger btn-sm" onclick="return confirm('Yakin mau dihapus?')">Hapus</a></td>                
                                            </tr>
                                            @endforeach
										</tbody>
									</table>
								</div>
							</div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">TAMBAH DATA SUPPLIER</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/supplier/create" method="POST">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="exampleInputEmail1">NAMA SUPPLIER</label>
                            <input name="nama_supplier" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama Supplier" required>
                        </div>

                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">ALAMAT</label>
                            <textarea name="alamat_supplier" class="form-control" id="exampleFormControlTextarea1" rows="3" required></textarea>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">TELEPON</label>
                            <input name="telepon_supplier" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan nomor telepon" required>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">EMAIL</label>
                            <input name="email_supplier" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan alamat email" required>
                        </div>
                        <!--div class="form-group">
                            <label for="exampleFormControlFile1">Masukkan Foto Sekolah (bila ada)</label>
                            <input name="foto_sekolah" type="file" class="form-control-file" id="exampleFormControlFile1">
                        </!--div>-->
                </div>
                    
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        
                    </form>
                </div>
                </div>
            </div>
@stop