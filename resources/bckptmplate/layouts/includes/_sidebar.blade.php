<div id="sidebar-nav" class="sidebar">
			<div class="sidebar-scroll">
				<nav>
					<ul class="nav">
						<li><a href="/aset" class="active"><i class="lnr lnr-home"><!--i class="fa fa-home"--></i> <span>Dashboard</span></a></li>
						<li>
							<a href="#subPages" data-toggle="collapse" class="collapsed"><i class="lnr lnr-file-empty"></i> <span>Data Master</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="subPages" class="collapsed">
								<ul class="nav">
									<li><a href="/barang" class=""><i class="lnr lnr-cart"></i>Barang</a></li><!--/barang-->
									<li><a href="/kategori" class=""><i class="fa fa-database"></i>Kategori</a></li><!--/kategori-->
									<li><a href="/supplier" class=""><i class="lnr lnr-location"></i>Supplier</a></li><!--/supplier-->
									<li><a href="/sekolah" class=""><i class="lnr lnr-map-marker"></i>Sekolah/ Yayasan</a></li><!--/sekolah_yayasan-->
									
								</ul>
							</div>
						</li>
						<li><a href="/aset" class=""><i class="lnr lnr-dice"></i> <span>Aset</span></a></li>
						<li><a href="/inventaris" class=""><i class="lnr lnr-inbox"></i> <span>Inventaris</span></a></li>
						<li><a href="#" class=""><i class="lnr lnr-map"></i> <span>Report/Laporan</span></a></li>
					</ul>
				</nav>
			</div>
		</div>