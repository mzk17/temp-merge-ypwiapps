@extends('layouts.master')

@section('content')
<div class="card">
                <div class="card-header">
                    <h3 class="card-title">Data Aset Alat dan Mesin</h3>
                </div>                
                    <!-- /.card-header -->
                    <div class="card-body ">                    
                                    <table id="example1" class="table table-bordered table-striped ">
                                        <div class="btn btn-sm right">
                                         <button type="button" class="btn btn-block btn-outline-primary" data-toggle="modal" data-target="#exampleModal">Tambah Data Aset                             
                                         </button>
                                        </div>
                                         <thead>
											<tr>
                                                <th>Nama Aset/ Barang </th>
                                                <th>Merk/Tipe</th>
                                                <th>Spesifikasi</th>
                                                <th>Tahun Pembelian</th>
                                                <!--Kondisi</!--th-->                                                
                                                <th width="14%">Harga</th>
                                                <th>Sumber Dana</th>
                                                <th>Keterangan</th>
                                                <th>Update</th>
                                                <th>Delete</th>
											</tr>
										</thead>
										<tbody>
                                            @foreach($data_alat_trans as $alat_trans)                                            
                                            <tr>                                                
                                                <td>{{$alat_trans->barang->nama_barang}}</td>
                                                <td>{{$alat_trans->merek_tipe}}</td>
                                                <td>{{$alat_trans->ukuran_cc}}</td>
                                                <td>{{$alat_trans->thn_pembelian}}</td>
                                                <!--td--$alat_trans->kondisi<td-->
                                                <!--td>formatVolume$alat_trans->luas</!td-->                
                                                <td width="14%">{{formatRupiah($alat_trans->harga)}}</td>
                                                <td>{{$alat_trans->sumber_dana}}</td>
                                                <td>{{$alat_trans->keterangan}}</td>
                                                <td><a href="/alat_trans/{{$alat_trans->id}}/edit" class="btn btn-warning btn-sm">Edit</a></td>
                                                <td><a href="/alat_trans/{{$alat_trans->id}}/delete" class="btn btn-danger btn-sm" onclick="return confirm('Yakin mau dihapus?')">Hapus</a></td>                
                                            </tr>
                                            @endforeach
										</tbody>
									</table>
								</div>
							</div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">TAMBAH DATA ASET ALAT/MESIN</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/alat_trans/create" method="POST">
                        {{csrf_field()}}
                        <div class="form-group">
                        <label for="exampleFormControlSelect1">SUB KATEGORI</label>
                            <select name="kategori_id" class="form-control" id="exampleFormControlSelect1">
                            <option disabled selected> Pilih kategori </option>
                            @foreach($c_kategori as $kat)
                                <option value="{{$kat->id}}">{{$kat->sub_kat}}</option>
                            @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                        <label for="exampleFormControlSelect1">NAMA SEKOLAH</label>
                            <select name="sekolah_id" class="form-control" id="exampleFormControlSelect1">
                            <option disabled selected> Pilih nama sekolah / Yayasan </option>
                            @foreach($c_sekolah as $skl)
                                <option value="{{$skl->id}}">{{$skl->nama_sekolah}}</option>
                            @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                        <label for="exampleFormControlSelect1">NAMA BARANG</label>
                            <select name="barang_id" class="form-control" id="exampleFormControlSelect1">
                            <option disabled selected> Pilih nama barang </option>
                            @foreach($c_barang as $brg)
                                <option value="{{$brg->id}}">{{@$brg->nama_barang}}</option>
                            @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">MEREK/TIPE  </label>
                            <input name="merek_tipe" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Tuliskan Merek/Tipe, misal ASUS Transformer A001" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">SPESIFIKASI</label>
                            <input name="ukuran_cc" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Isikan spesifikasi, misal Prosesor Intel Core i3 3500 Mhz">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">TAHUN PEMBELIAN  </label>
                            <input name="thn_pembelian" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Isikan angka tahun lengkap, misalnya 2020" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">HARGA  </label>
                            <input name="harga" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="masukkan harga, tuliskan angkanya saja, misal 100000" required>
                        </div>                        
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">SUMBER DANA</label>
                            <select name="sumber_dana" class="form-control" id="exampleFormControlSelect1">
                            <option disabled selected> Pilih Sumber Dana</option>
                                <option value="Dana Yayasan">Dana Yayasan</option>
                                <option value="Dana BOS">Dana BOS Sekolah</option>
                                <option value="Dana Pribadi">Dana Pribadi</option>
                                <option value="Pinjaman Bank">Pinjaman Bank</option>
                                <option value="Sumbangan Masyarakat">Sumbangan Masyarakat</option>
                                <option value="Wakaf">Wakaf</option>                                
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">KONDISI</label>
                            <select name="kondisi" class="form-control" id="exampleFormControlSelect1">
                            <option disabled selected> Pilih kondisi Baru / Bekas </option>
                                <option value="Baru">Baru</option>
                                <option value="Bekas">Bekas</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">KETERANGAN</label>
                            <textarea name="keterangan" class="form-control" id="exampleFormControlTextarea1" rows="3" required></textarea>
                        </div>
                        
                        <div class="form-group">
                            <label for="exampleFormControlFile1">Masukkan Foto/Nota Alat (bila ada)</label>
                            <input name="fotoavatar" type="file" class="form-control-file" id="exampleFormControlFile1">
                        </div>
                </div>
                    
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        
                    </form>
                </div>
                </div>
            </div>
@stop