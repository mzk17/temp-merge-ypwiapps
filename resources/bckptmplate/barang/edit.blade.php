@extends('layouts.master')

@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                <div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Edit Data Barang</h3>
								</div>
								<div class="panel-body">
                                            <form action="/barang/{{$barang->id}}/update" method="POST">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">NAMA BARANG</label>
                                        <input name="nama_barang" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama Barang" value="{{$barang->nama_barang}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">KETERANGAN</label>
                                        <input name="spesifikasi" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan keterangan" value="{{$barang->spesifikasi}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">LOKASI</label>
                                        <select name="lokasi" class="form-control" id="exampleFormControlSelect1">
                                            <option value="1"@if($barang->lokasi=='1') selected @endif> YPWI PUSAT MAKASSAR</option>
                                            <option value="2"@if($barang->lokasi=='2') selected @endif> SD WU ABDESIR</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">KATEGORI</label>
                                        <select name="kategori" class="form-control" id="exampleFormControlSelect1">
                                            <option value="TANAH"@if($barang->kategori=='TANAH') selected @endif> TANAH</option>
                                            <option value="BANGUNAN"@if($barang->kategori=='BANGUNAN') selected @endif>BANGUNAN</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">HARGA (Rp)</label>
                                        <input name="harga" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Cukup tuliskan dalam angka" value="{{($barang->harga)}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">KONDISI</label>
                                        <select name="kondisi" class="form-control" id="exampleFormControlSelect1" value="{{$barang->kondisi}}">
                                            <option value="Baru" @if($barang->kondisi=='Baru') selected @endif> Baru </option>
                                            <option value="Bekas" @if($barang->kondisi=='Bekas') selected @endif> Bekas </option>
                                        </select>
                                    </div>                                    
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">SUMBER DANA</label>
                                        <select name="sumber_dana" class="form-control" id="exampleFormControlSelect1" value="{{$barang->sumber_dana}}">
                                            <option value="DANA YAYASAN" @if($barang->sumber_dana=='DANA YAYASAN') selected @endif> DANA YAYASAN </option>
                                            <option value="DANA BOS" @if($barang->sumber_dana=='DANA BOS') selected @endif> DANA BOS </option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlFile1">Masukkan Foto/Nota Barang (bila ada)</label>
                                        <input name="foto" type="file" value="{{$barang->foto}}" class="form-control-file" id="exampleFormControlFile1">
                                    </div>
                                    <button type="submit" class="btn btn-warning">Update Data</button>
                                    </form>
								</div>
							</div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop