@extends('layouts.master')

@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
            <form class="navbar-form navbar-right" method="GET" action="/barang">
                    <input name="cari" class="form-control" type="search" placeholder="Cari berdasarkan barang" aria-label="Search">
                    <button class="btn btn-outline-success btn-primary right" type="submit">Cari !!</button>
                    </form>
                <div class="col-md-12">
                <div class="panel">
								<div class="panel-heading">
                                    <h2 class="panel-title text-bold">Data Barang</h2>
                                    <div class=" btn btn-primary right">
                                        <button type="button" class="btn-primary" data-toggle="modal" data-target="#exampleModal">Tambah Data Barang                              
                                        </button>
                                    </div>
								</div>
								<div class="panel-body">
									<table class="table table-hover">
										<thead>
											<tr>
                                                <th>NAMA BARANG</th>
                                                <th>SUPPLIER </th>
                                                <th>KATEGORI </th>
                                                <th>KONDISI </th>
                                                <th>SPESIFIKASI </th>
                                                <th>HARGA </th>
                                                <th>SUMBER DANA</th>
                                                <th>FOTO</th>
                                                <th>AKSI</th>
                                                <th></th>
											</tr>
										</thead>
										<tbody>
                                            @foreach($data_barang as $barang)
                                            <tr>
                                                <td>{{$barang->nama_barang}}</td>
                                                <td>{{$barang->supplier_id}}</td>
                                                <td>{{$barang->kategori_id}}</td>
                                                <td>{{$barang->kondisi}}</td>
                                                <td>{{$barang->spesifikasi}}</td>               
                                                <td>{{formatRupiah($barang->harga)}}</td>
                                                <td>{{$barang->kondisi}}</td>
                                                <td>{{$barang->sumber_dana}}</td>
                                                <td><a href="/barang/{{$barang->id}}/edit" class="btn btn-warning btn-sm">Edit</a></td>
                                                <td><a href="/barang/{{$barang->id}}/delete" class="btn btn-danger btn-sm" onclick="return confirm('Yakin mau dihapus?')">Hapus</a></td>                
                                            </tr>
                                            @endforeach
										</tbody>
									</table>
								</div>
							</div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">TAMBAH DATA BARANG</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/barang/create" method="POST">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="exampleInputEmail1">NAMA BARANG</label>
                            <input name="nama_barang" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama Barang" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">KETERANGAN</label>
                            <input name="spesifikasi" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan keterangan" required>
                        </div>                          
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">LOKASI</label>
                            <select name="lokasi" class="form-control" id="exampleFormControlSelect1">
                                <option value="1">YPWI PUSAT MAKASSAR</option>
                                <option value="2">SD WU ABDESIR</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">KATEGORI</label>
                            <select name="kategori" class="form-control" id="exampleFormControlSelect1">
                                <option value="TANAH">TANAH</option>
                                <option value="BANGUNAN">BANGUNAN</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">HARGA (Rp)</label>
                            <input name="harga" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Cukup tuliskan dalam angka" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">KONDISI</label>
                            <select name="kondisi" class="form-control" id="exampleFormControlSelect1">
                                <option value="Baru">Baru</option>
                                <option value="Bekas">Bekas</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">SUMBER DANA</label>
                            <select name="sumber_dana" class="form-control" id="exampleFormControlSelect1">
                                <option value="DANA YAYASAN">DANA YAYASAN</option>
                                <option value="DANA BOS">DANA BOS</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlFile1">Masukkan Foto/Nota Barang (bila ada)</label>
                            <input name="foto" type="file" class="form-control-file" id="exampleFormControlFile1">
                        </div>
                </div>
                    
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        
                    </form>
                </div>
                </div>
            </div>
@stop