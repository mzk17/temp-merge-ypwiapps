@extends('layouts.master')

@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                <div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Edit Data Inventaris</h3>
								</div>
								<div class="panel-body">
                                            <form action="/inventaris/{{$inventaris->id}}/update" method="POST">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">NAMA INVENTARIS</label>
                                        <input name="nama_inventaris" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama inventaris" value="{{$inventaris->nama_inventaris}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">LOKASI</label>
                                        <select name="id_lokasi" class="form-control" id="exampleFormControlSelect1">
                                            <option value="1"@if($inventaris->id_lokasi=='1') selected @endif> YPWI PUSAT MAKASSAR</option>
                                            <option value="2"@if($inventaris->id_lokasi=='2') selected @endif> SD WU ABDESIR</option>
                                            <option value="3"@if($inventaris->id_lokasi=='3') selected @endif> SD WI 01 ANTANG</option>
                                            <option value="4"@if($inventaris->id_lokasi=='4') selected @endif> SD WI 02 HERTASNING</option>
                                            <option value="5"@if($inventaris->id_lokasi=='5') selected @endif> SD WI 03 BTP</option>
                                            <option value="6"@if($inventaris->id_lokasi=='6') selected @endif> SMP IT WI PUTRA</option>
                                            <option value="7"@if($inventaris->id_lokasi=='7') selected @endif> SMP IT WI PUTRI</option>
                                            <option value="8"@if($inventaris->id_lokasi=='8') selected @endif> SMA IT WI PUTRA</option>
                                            <option value="9"@if($inventaris->id_lokasi=='9') selected @endif> SMA IT WI PUTRI</option>
                                            <option value="10"@if($inventaris->id_lokasi=='10') selected @endif> TK IT WU</option>
                                            <option value="11"@if($inventaris->id_lokasi=='11') selected @endif> TK IT WI 01</option>
                                            <option value="12"@if($inventaris->id_lokasi=='12') selected @endif> PONPES TAHFIDZ WI - BILAYYA</option> 
                                            <option value="13"@if($inventaris->id_lokasi=='13') selected @endif> PONPES TAHFIDZ WI - DAYA</option>
                                            <option value="14"@if($inventaris->id_lokasi=='14') selected @endif> SMP-SMA AL-QUR'AN WI CIBINONG BOGOR</option>
                                            <option value="15"@if($inventaris->id_lokasi=='15') selected @endif> SMP-SMA AL-QUR'AN WI ANABANUA WAJO
                                            <option value="16"@if($inventaris->id_lokasi=='16') selected @endif> SAWANGAN DEPOK</option>
                                            <option value="17"@if($inventaris->id_lokasi=='17') selected @endif> TANJUNG PINANG RIAU</option>
                                            <option value="18"@if($inventaris->id_lokasi=='18') selected @endif> JAMBI</option>
                                            <option value="19"@if($inventaris->id_lokasi=='19') selected @endif> BELOPA</option>
                                            <option value="20"@if($inventaris->id_lokasi=='20') selected @endif> GORONTALO</option>
                                            <option value="21"@if($inventaris->id_lokasi=='21') selected @endif> PALOPO</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">KATEGORI</label>
                                        <select name="kategori" class="form-control" id="exampleFormControlSelect1">
                                            <option value="TANAH"@if($inventaris->kategori=='TANAH') selected @endif> TANAH</option>
                                            <option value="BANGUNAN"@if($inventaris->kategori=='BANGUNAN') selected @endif>BANGUNAN</option>
                                        </select>
                                    </div>

                                    <!-- //kalau mau pake radio

                                    <div class="form-check">
                                    <input class="form-check-input" type="radio" name="kategori" id="exampleRadios1" value="TANAH">
                                    <label class="form-check-label" for="exampleRadios1">
                                        TANAH
                                    </label>
                                    </div>
                                    <div class="form-check">
                                    <input class="form-check-input" type="radio" name="kategori" id="exampleRadios2" value="BANGUNAN">
                                    <label class="form-check-label" for="exampleRadios2">
                                        BANGUNAN
                                    </label>
                                    </div>

                                    -->
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">JUMLAH BARANG</label>
                                        <input name="jumlah" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Cukup tuliskan dalam angka" value="{{$inventaris->jumlah}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">HARGA (Rp)</label>
                                        <input name="harga" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Cukup tuliskan dalam angka" value="{{($inventaris->harga)}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">TANGGAL</label>
                                        <select name="tanggal" class="form-control" id="exampleFormControlSelect1">
                                            <option value="1" @if($inventaris->tanggal=='1') selected @endif> 1 </option>
                                            <option value="2" @if($inventaris->tanggal=='2') selected @endif> 2 </option>
                                            <option value="3" @if($inventaris->tanggal=='3') selected @endif> 3 </option>
                                            <option value="4" @if($inventaris->tanggal=='4') selected @endif> 4 </option>
                                            <option value="5"@if($inventaris->tanggal=='5') selected @endif> 5 </option>
                                            <option value="6"@if($inventaris->tanggal=='6') selected @endif> 6 </option>
                                            <option value="7"@if($inventaris->tanggal=='7') selected @endif> 7 </option>
                                            <option value="8"@if($inventaris->tanggal=='8') selected @endif> 8 </option>
                                            <option value="9"@if($inventaris->tanggal=='9') selected @endif> 9 </option>
                                            <option value="10"@if($inventaris->tanggal=='10') selected @endif> 10 </option>
                                            <option value="11"@if($inventaris->tanggal=='11') selected @endif> 11 </option>
                                            <option value="12"@if($inventaris->tanggal=='12') selected @endif> 12 </option>
                                            <option value="13"@if($inventaris->tanggal=='13') selected @endif> 13 </option>
                                            <option value="14"@if($inventaris->tanggal=='14') selected @endif> 14 </option>
                                            <option value="15"@if($inventaris->tanggal=='15') selected @endif> 15 </option>
                                            <option value="16"@if($inventaris->tanggal=='16') selected @endif> 16 </option>
                                            <option value="17"@if($inventaris->tanggal=='17') selected @endif> 17 </option>
                                            <option value="18"@if($inventaris->tanggal=='18') selected @endif> 18 </option>
                                            <option value="19"@if($inventaris->tanggal=='19') selected @endif> 19 </option>
                                            <option value="20" @if($inventaris->tanggal=='20') selected @endif> 20 </option>
                                            <option value="21"@if($inventaris->tanggal=='21') selected @endif> 21 </option>
                                            <option value="22"@if($inventaris->tanggal=='22') selected @endif> 22 </option>
                                            <option value="23"@if($inventaris->tanggal=='23') selected @endif> 23 </option>
                                            <option value="24"@if($inventaris->tanggal=='24') selected @endif> 24 </option>
                                            <option value="25"@if($inventaris->tanggal=='25') selected @endif> 25 </option>
                                            <option value="26"@if($inventaris->tanggal=='26') selected @endif> 26 </option>
                                            <option value="27"@if($inventaris->tanggal=='27') selected @endif> 27 </option>
                                            <option value="28"@if($inventaris->tanggal=='28') selected @endif> 28 </option>
                                            <option value="29"@if($inventaris->tanggal=='29') selected @endif> 29 </option>
                                            <option value="30"@if($inventaris->tanggal=='30') selected @endif> 30 </option>
                                            <option value="31"@if($inventaris->tanggal=='31') selected @endif> 31 </option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">BULAN</label>
                                        <select name="bulan" class="form-control" id="exampleFormControlSelect1" value="{{$inventaris->bulan}}">
                                            <option value="1"@if($inventaris->bulan=='1') selected @endif>JANUARI</option>
                                            <option value="2"@if($inventaris->bulan=='2') selected @endif>FEBRUARI</option>
                                            <option value="3"@if($inventaris->bulan=='3') selected @endif>MARET</option>
                                            <option value="4"@if($inventaris->bulan=='4') selected @endif>APRIL</option>
                                            <option value="5"@if($inventaris->bulan=='5') selected @endif>MEI</option>
                                            <option value="6"@if($inventaris->bulan=='6') selected @endif>JUNI</option>
                                            <option value="7"@if($inventaris->bulan=='7') selected @endif>JULI</option>
                                            <option value="8"@if($inventaris->bulan=='8') selected @endif>AGUSTUS</option>
                                            <option value="9"@if($inventaris->bulan=='9') selected @endif>SEPTEMBER</option>
                                            <option value="10"@if($inventaris->bulan=='10') selected @endif>OKTOBER</option>
                                            <option value="11"@if($inventaris->bulan=='11') selected @endif>NOVEMBER</option>
                                            <option value="12"@if($inventaris->bulan=='12') selected @endif>DESEMBER</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">TAHUN</label>
                                        <select name="tahun" class="form-control" id="exampleFormControlSelect1" value="{{$inventaris->tahun}}">
                                            <option value="2020"@if($inventaris->tahun=='2020') selected @endif>2020</option>
                                            <option value="2019"@if($inventaris->tahun=='2019') selected @endif>2019</option>
                                            <option value="2018"@if($inventaris->tahun=='2018') selected @endif>2018</option>
                                            <option value="2017"@if($inventaris->tahun=='2017') selected @endif>2017</option>
                                            <option value="2016"@if($inventaris->tahun=='2016') selected @endif>2016</option>
                                            <option value="2015"@if($inventaris->tahun=='2015') selected @endif>2015</option>
                                            <option value="2014"@if($inventaris->tahun=='2014') selected @endif>2014</option>
                                            <option value="2013"@if($inventaris->tahun=='2013') selected @endif>2013</option>
                                            <option value="2012"@if($inventaris->tahun=='2012') selected @endif>2012</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label name="keterangan" for="exampleFormControlTextarea1" >KETERANGAN</label>
                                        <textarea name="keterangan" class="form-control" id="exampleFormControlTextarea1" rows="3" required>{{$inventaris->keterangan}}</textarea>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="exampleFormControlFile1">Masukkan Foto/Nota Inventaris (bila ada)</label>
                                        <input name="foto_inventaris" type="file" value="{{$inventaris->foto_inventaris}}" class="form-control-file" id="exampleFormControlFile1">
                                    </div>
                                    <button type="submit" class="btn btn-warning">Update Data</button>
                                    </form>
								</div>
							</div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop