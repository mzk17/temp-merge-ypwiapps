import Vue from "vue";
import Buefy from "buefy";

const DATE_FORMATTER = date => {
    let d = new Date(date);
    let month = "" + (d.getMonth() + 1);
    let day = "" + d.getDate();
    let year = "" + d.getFullYear();

    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;

    return [year, month, day].join("-");
};

Vue.use(Buefy, {
    defaultIconPack: "fa",
    defaultDateFormatter: DATE_FORMATTER
});
