import Vue from "vue";
import { library, dom } from "@fortawesome/fontawesome-svg-core";
import {
    faCheck,
    faCalendarDay,
    faAngleRight,
    faAngleLeft,
    faExclamationCircle
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

let icons = [
    faCheck,
    faAngleRight,
    faAngleLeft,
    faCalendarDay,
    faExclamationCircle
];

library.add(icons);
dom.watch();

Vue.component("font-awesome-icon", FontAwesomeIcon);
