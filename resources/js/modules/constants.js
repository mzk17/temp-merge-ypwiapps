// Define URL
const API_URL =
    process.env.MIX_APP_ENV === "local"
        ? process.env.MIX_APP_URL + "/api"
        : "http://ypwi.or.id/api";

export { API_URL };
