import Vue from "vue";
import Vuex from "vuex";

import formHandling from "./modules/formHandling";

Vue.use(Vuex);

const debug = process.env.APP_ENV !== "production";

export default new Vuex.Store({
    modules: {
        formHandling
    },
    strict: debug
});
