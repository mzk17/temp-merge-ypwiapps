function convertObjectToMap(obj) {
    let map = new Map();
    let keys = Object.keys(obj);
    for (let i = 0; i < keys.length; i++) {
        map.set(keys[i], obj[keys[i]]);
    }

    return map;
}

const state = {
    errorMessages: [],
    currentData: {}
};

const getters = {
    currentData: state => {
        return state.currentData;
    },
    errorMessages: state => {
        return state.errorMessages;
    }
};

const mutations = {
    setEntityData(state, data) {
        state.currentData = state.currentData ? Object.assign(state.currentData, data) : data;
    },
    setErrorMessages(state, messages) {
        state.errorMessages = messages;
    }
};

const actions = {
    pushEntityData(context, data) {
        context.commit("setEntityData", data);
    },
    pushErrorMessages(context, messages) {
        context.commit("setErrorMessages", messages);
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
