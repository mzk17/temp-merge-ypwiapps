import { mapGetters } from "vuex";

export default {
    props: {
        entity: {
            type: [Array, Object],
            required: false,
            default: () => {}
        },
        old: {
            type: [Array, Object],
            required: false,
            default: () => {}
        }
    },
    created() {
        if (this.entity) {
            this.$store.dispatch("formHandling/pushEntityData", this.entity);
        } else if (this.old) {
            this.$store.dispatch("formHandling/pushEntityData", this.old);
        }
    },
    computed: {
        ...mapGetters("formHandling", ["currentData"])
    }
};
