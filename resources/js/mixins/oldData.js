import { mapGetters } from "vuex";

export default {
    props: {
        old: {
            type: [Array, Object],
            required: false,
            default: () => {}
        }
    },
    computed: {
        ...mapGetters("formHandling", ["oldData"])
    },
};
