import { mapGetters } from "vuex";

export default {
    props: {
        errors: {
            type: [Array, Object],
            required: false,
            default: () => []
        }
    },
    created() {
        if (this.errors) {
            this.$store.dispatch("formHandling/pushErrorMessages", this.errors);
        }
    },
    computed: {
        ...mapGetters("formHandling", ["errorMessages"])
    },
    methods: {
        showErrorMessages(prop) {
            if (this.errorMessages.hasOwnProperty(prop)) {
                let last_index = this.errorMessages[prop].length - 1;
                return this.errorMessages[prop][last_index];
            }
            return;
        }
    }
};
