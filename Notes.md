### Proses pendaftaran akun pegawai
1. Data dari DAPONDAS diupload terlebih dahulu
2. Link pendaftaran akun untuk YPWI-Apps Suite dikirimkan kepada para pegawai (sekolah maupun yayasan)
3. Pada halaman registrasi terdapat form
    a. Sekolah (Select - Options)
    b. NIP (Text field). Para pegawai diharuskan menuliskan NIP dan terdapat tombol "Cek" untuk mengecek kebenaran.
       Kenapa menggunakan text field?
       Karena kalau memakai (Select - Options), bisa saja NIP pegawai lain disalahgunakan.
    c. Checkbox apakah dia sebagai guru atau tidak

### Informasi Penggabungan
- Tiap modul aplikasi akan ada database nya sendiri
- Struktur folder juga akan di refactor sesuai modul aplikasi masing - masing.
Misal untuk controller dan model.
- Khusus untuk master data, akan dinamakan modul "Zero" pada struktur folder aplikasi
- 