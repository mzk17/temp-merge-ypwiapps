const mix = require("laravel-mix");
const tailwindcss = require("tailwindcss");

mix.js("resources/js/app.js", "public/js");
mix.sass("resources/sass/app.scss", "public/css", {
    includePaths: ["node_modules/buefy/src/scss"]
}).options({
    processCssUrls: false,
    postCss: [tailwindcss("./tailwind.config.js")]
});

mix.browserSync({
    open: false,
    proxy: process.env.MIX_APP_URL
});
