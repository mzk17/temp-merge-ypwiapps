Akselerasi pembangunan Sekolah menuju Visi 2030 Wahdah Islamiyah

PALOPO, ypwi.or⁯.id — Wakil Wali Kota Palopo, Rahmat Masri Bandaso (RMB) mengawali peletakan batu pertama pembangunan ruang kelas baru (RKB) SMPIT Wahdah Islamiyah Palopo 
yang berlokasi di Jalan Andi Bintang, Kelurahan Peta, Kecamatan Sendana,Kota Palopo, Sabtu 1 Pebruari 2020.

Pada Kesempatan itu, RMB memberikan apresiasi dan dorongan kepada Wahdah Islamiyah untuk terus membangun generasi muda 
bukan hanya di sektor pendidikan, dakwah, lingkungan hidup, kesehatan, pengembangan muslimah tetapi juga sektor lain, termasuk bidang ekonomi.

“Pemerintah senantiasa memfasilitasi Wahdah Islamiyah untuk berkiprah dalam membangun bangsa dan negara 
melalui upaya di bidang pendidikan, dakwah, lingkungan hidup, kesehatan, sehingga tetap eksis khususnya di kota Palopo.”

Menurut beliau, kehadiran Wahdah Islamiyah khusus pada sektor pendidikan akan memberikan kontribusi yang positif di Kota Palopo,
terutama bagi para anak didik terutama dalam implementasi pendidikan keagamaan, hafalan Al-Qur'an, serta nilai-nilai kebangsaan dalam diri masing-masing.

Sementara itu, Ketua Yayasan Wahdah Islamiyah Pusat, Nursalam Siradjuddin, dalam sambutannya menyampaikan pembangunan RKB ini merupakan salah satu 
program Nasional yang disebut program inti imbas yang dilakukan Wahdah Islamiyah dalam membangun dan mengembangkan sumber daya manusia yang cerdas dan religius
terutama di daerah-daerah yang berpotensi untuk maju.

Selain itu beliau menyampaikan, untuk Membentuk generasi muda yang cerdas dan religius penting kita lakukan dalam rangka 
membentengi anak-anak kita dari pengaruh negatif terutama pergaulan bebas, khususnya penyalahgunaan narkotika dan obat-obatan terlarang.

Turut hadir pada peletakan batu pertama pembangunan RKB dan Kantor Kepsek, Kepala Dinas Pendidikan Kota Palopo Asnita Darwis, 
Camat Sendana Andi Baso Asnur, pimpinan perbankan (Bank Syariah Mandiri, Bank Muamalat), pimpinan Penerbit Erlangga cabang Palopo, serta tokoh agama dan tokoh masyarakat.(rls)